-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 03, 2019 at 07:12 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `nuikasmanssa`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE IF NOT EXISTS `admins` (
  `admin_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(225) NOT NULL,
  `slug_admin` varchar(225) NOT NULL,
  `password` varchar(225) NOT NULL,
  `email` varchar(225) NOT NULL,
  `role` int(11) NOT NULL DEFAULT '0',
  `id_alumni` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`admin_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`admin_id`, `username`, `slug_admin`, `password`, `email`, `role`, `id_alumni`) VALUES
(1, 'admin', '', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'contact@compose.com', 1, 0),
(3, 'admin123', 'admin123', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'siriusblack@hg.uk', 1, 0),
(4, 'harrypotter', '', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'harrypotter@hg.uk', 2, 1),
(5, 'ronweasley', '', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'ronweasley@hg.uk', 3, 2),
(6, 'hermione', '', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'hermionegranger@hg.uk', 3, 4);

-- --------------------------------------------------------

--
-- Table structure for table `alumni`
--

CREATE TABLE IF NOT EXISTS `alumni` (
  `idalumni` int(11) NOT NULL AUTO_INCREMENT,
  `nama_lengkap` varchar(254) DEFAULT NULL,
  `nama_panggilan` varchar(254) DEFAULT NULL,
  `angkatan` int(11) DEFAULT NULL,
  `jurusan` varchar(128) DEFAULT NULL,
  `alamat` longtext,
  `telepon` varchar(50) DEFAULT NULL,
  `idprofesi` int(11) NOT NULL,
  `kodepos` int(11) DEFAULT NULL,
  `idkota` int(11) DEFAULT NULL,
  `verifikasi` int(11) NOT NULL DEFAULT '0' COMMENT '1=terverifikasi',
  `foto` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idalumni`),
  KEY `profesi_fk_idx` (`idprofesi`),
  KEY `kota_fk_idx` (`idkota`),
  KEY `role_fk_idx` (`verifikasi`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `alumni`
--

INSERT INTO `alumni` (`idalumni`, `nama_lengkap`, `nama_panggilan`, `angkatan`, `jurusan`, `alamat`, `telepon`, `idprofesi`, `kodepos`, `idkota`, `verifikasi`, `foto`) VALUES
(1, 'Harry Potter', 'Harry', 2012, 'MIPA', 'RT 002 RW 010', '085123123123', 42, 50722, 3374, 1, 'hp1.jpg'),
(2, 'Ronald Weasly', 'Ron', 2012, 'MIPA', 'RT 004 RW 008', '085123123123', 11, 50999, 3471, 1, 'Ron_Weasley_(I).jpg'),
(4, 'Hermione Granger', 'Hermione', 2012, 'MIPA', 'RT 004 RW 010', '085123123123', 14, 58556, 5104, 0, '110910-Hermione-2-400_01.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `berita`
--

CREATE TABLE IF NOT EXISTS `berita` (
  `idberita` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(256) DEFAULT NULL,
  `tgl_post` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `slug` varchar(256) DEFAULT NULL,
  `isi` longtext,
  `idkategori` int(11) DEFAULT NULL,
  `aktif` smallint(1) DEFAULT '1',
  `foto` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`idberita`),
  KEY `idkategori_idx` (`idkategori`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `berita`
--

INSERT INTO `berita` (`idberita`, `judul`, `tgl_post`, `slug`, `isi`, `idkategori`, `aktif`, `foto`) VALUES
(1, 'Breaking News', '2019-11-13 07:47:58', '1-breaking-news', '<p><em>Lorem ipsum</em>, or&nbsp;<em>lipsum</em>&nbsp;as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed to an unknown typesetter in the 15th century who is thought to have scrambled parts of Cicero''s De Finibus Bonorum et Malorum for use in a type specimen book.</p>', 1, 1, 'Pomona_Sprout.jpg'),
(2, 'New York Story', '2019-11-13 02:07:22', 'new-york-story', '<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.</p>\r\n<p>The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p>', 1, 1, 'bZp3Sx.jpg'),
(3, 'Tentang Kami', '2019-12-03 05:48:28', '4-tentang-kami', '<p class="text-light">Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius.</p>\r\n<p class="text-light">Investigationes demonstraverunt lectores. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius.</p>\r\n<p class="text-light">&nbsp;</p>\r\n<h2 class="text-regular" style="text-align: center;">Guyub, Gayeng, Migunani</h2>\r\n<p class="text-light" style="text-align: center;">uis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan.</p>', 2, 1, 'slider21.jpg'),
(4, 'Kontak Kami', '2019-12-03 06:44:08', '4-kontak-kami', '<div class="g">\r\n<div data-hveid="CAQQAA" data-ved="2ahUKEwish_fz4pjmAhUK7nMBHd1AA1oQFSgAMAp6BAgEEAA">\r\n<div class="rc">\r\n<div class="s">\r\n<div><span class="st"><em>Lorem ipsum</em>, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed to an unknown typesetter in the 15th century who is thought to have scrambled parts of Cicero''s De Finibus Bonorum et Malorum for use in a type specimen book.</span></div>\r\n</div>\r\n<div id="ed_7" data-base-uri="/search?safe=strict&amp;sxsrf=ACYBGNSJC9CwFmlmzRmBPBKxprYeOyOZWA:1575351834500" data-ved="2ahUKEwish_fz4pjmAhUK7nMBHd1AA1oQ2Z0BMAp6BAgEEAU">&nbsp;</div>\r\n</div>\r\n</div>\r\n</div>\r\n<div class="g">&nbsp;</div>', 2, 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

CREATE TABLE IF NOT EXISTS `blogs` (
  `blog_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `title` varchar(225) NOT NULL,
  `slug_blog` varchar(225) NOT NULL,
  `content` text NOT NULL,
  `image` varchar(225) NOT NULL,
  `date_post` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` varchar(100) NOT NULL,
  `keywords` text NOT NULL,
  PRIMARY KEY (`blog_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `category_name` varchar(225) NOT NULL,
  `slug_category` varchar(225) NOT NULL,
  `order_category` int(11) NOT NULL,
  `category_description` text NOT NULL,
  `date_category` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`category_id`, `user_id`, `category_name`, `slug_category`, `order_category`, `category_description`, `date_category`) VALUES
(2, 1, 'News', '2-news', 1, 'News Update', '2017-02-15 14:31:39'),
(3, 1, 'Politik', 'politik', 2, 'Seputar Politik', '2017-02-23 05:47:26');

-- --------------------------------------------------------

--
-- Table structure for table `config`
--

CREATE TABLE IF NOT EXISTS `config` (
  `config_id` int(11) NOT NULL AUTO_INCREMENT,
  `nameweb` varchar(225) NOT NULL,
  `email` varchar(225) NOT NULL,
  `keywords` text NOT NULL,
  `google_maps` text NOT NULL,
  `logo` varchar(225) NOT NULL,
  `icon` varchar(225) NOT NULL,
  `about` text NOT NULL,
  `address` text NOT NULL,
  `city` varchar(100) NOT NULL,
  `zip_code` varchar(100) NOT NULL,
  `phone_number` varchar(225) NOT NULL,
  `metatext` text NOT NULL,
  `fax` text NOT NULL,
  `facebook` varchar(225) NOT NULL,
  `twitter` varchar(225) NOT NULL,
  `instagram` varchar(225) NOT NULL,
  `google_plus` varchar(225) NOT NULL,
  `pinterest` varchar(225) NOT NULL,
  PRIMARY KEY (`config_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `config`
--

INSERT INTO `config` (`config_id`, `nameweb`, `email`, `keywords`, `google_maps`, `logo`, `icon`, `about`, `address`, `city`, `zip_code`, `phone_number`, `metatext`, `fax`, `facebook`, `twitter`, `instagram`, `google_plus`, `pinterest`) VALUES
(1, 'Ikasmanssa', 'alumni@ikasmanssa.org', 'Pickup your items', 'https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15829.281090614078!2d110.4971345!3d-7.3178741!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x8c0238b053a6c04c!2sSMAN%201%20Salatiga!5e0!3m2!1sen!2sid!4v1572937542924!5m2!1sen!2sid"', 'logo.png', 'i3qUAvP.jpg', 'Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare.', 'Kemiri', 'Salatiga', '18923', '324234', 'lorem ipsum idoor lorem ipsum idoor lorem ipsum idoorlorem ipsum idoorlorem ipsum idoorlorem ipsum idoorlorem ipsum idoorlorem ipsum idoor lorem ipsum idoor lorem ipsum idoor lorem ipsum idoorlorem ipsum idoorlorem ipsum idoorlorem ipsum idoorlorem ipsum idoorlorem ipsum idoor', '23424', 'https://www.facebook.com/', '', 'https://www.instagram.com/', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `event`
--

CREATE TABLE IF NOT EXISTS `event` (
  `idevent` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `isi` text,
  `tgl_event` date DEFAULT NULL,
  `alamat` text,
  `longitude` varchar(100) DEFAULT NULL,
  `latitude` varchar(100) DEFAULT NULL,
  `foto` varchar(255) DEFAULT NULL,
  `google_map` text,
  PRIMARY KEY (`idevent`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `event`
--

INSERT INTO `event` (`idevent`, `judul`, `slug`, `isi`, `tgl_event`, `alamat`, `longitude`, `latitude`, `foto`, `google_map`) VALUES
(1, 'NYC', 'nyc', 't is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ''Content here, content here'', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for ''lorem ipsum'' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).', '2019-11-27', '420 E 87th St, New York, NY 10128, United States', '-73.9479135', '40.7770607', 'nyc.jpg', '<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3021.3458877043704!2d-73.94701902883605!3d40.77640938811429!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x88ecd279447548db!2sSt.%20Joseph&#39;s%20School%20-%20Yorkville!5e0!3m2!1sen!2sid!4v1574750967383!5m2!1sen!2sid" width="400" height="300" frameborder="0" style="border:0;" allowfullscreen=""></iframe>'),
(2, 'Blenheim', '2-blenheim', '<blockquote class="page-section__blockquote">&ldquo;Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.&rdquo;</blockquote>\r\n<p class="f4 cl-white mv16">The purpose of&nbsp;<em>lorem ipsum</em>&nbsp;is to create a natural looking block of text (sentence, paragraph, page, etc.) that doesn''t distract from the layout. A practice not without&nbsp;<a title="Controversy in the Design World" href="https://loremipsum.io/#controversy">controversy</a>, laying out pages with meaningless filler text can be very useful when the focus is meant to be on design, not content.</p>\r\n<p class="f4 cl-white mv16">The passage experienced a surge in popularity during the 1960s when Letraset used it on their dry-transfer sheets, and again during the 90s as desktop publishers bundled the text with their software. Today it''s seen all around the web; on templates, websites, and stock designs. Use our&nbsp;<a title="Lorem Ipsum Generator" href="https://loremipsum.io/#generator">generator</a>&nbsp;to get your own, or read on for the authoritative history of&nbsp;<em>lorem ipsum</em>.</p>\r\n<p style="text-align: center;"><img src="../../../../assets/upload/image/11484.jpg" alt="" width="229" height="237" /></p>\r\n<p>&nbsp;</p>', '2020-12-14', 'Woodstock OX20 1PP, United Kingdom', NULL, NULL, 'Blenheim-Palace-Park-and-Estate_Fotor-1024x574.jpg', '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2465.072865508819!2d-1.3631606848130684!3d51.84136497969137!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4876cfe323947857%3A0x553e59b8e1ef0e18!2sBlenheim%20Palace!5e0!3m2!1sen!2sid!4v1574752168110!5m2!1sen!2sid" width="400" height="300" frameborder="0" style="border:0;" allowfullscreen=""></iframe>');

-- --------------------------------------------------------

--
-- Table structure for table `galeri`
--

CREATE TABLE IF NOT EXISTS `galeri` (
  `idgaleri` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(256) DEFAULT NULL,
  `deskripsi` text,
  `tgl_galeri` datetime NOT NULL,
  `slug` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idgaleri`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `galeri`
--

INSERT INTO `galeri` (`idgaleri`, `judul`, `deskripsi`, `tgl_galeri`, `slug`) VALUES
(1, 'USS Enterprise', 'Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.', '2019-11-27 02:23:02', 'uss-enterprise'),
(2, 'Lorem ipsum, or lipsum as it is sometimes known, is dummy text', 'Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed to an unknown typesetter in the 15th century who is thought to have scrambled parts of Cicero''s De Finibus Bonorum et Malorum for use in a type specimen book.', '2019-11-28 10:19:09', 'hogwarts');

-- --------------------------------------------------------

--
-- Table structure for table `galeri_detail`
--

CREATE TABLE IF NOT EXISTS `galeri_detail` (
  `idgaleri_detail` int(11) NOT NULL AUTO_INCREMENT,
  `tgl_post` datetime DEFAULT NULL,
  `keterangan` longtext,
  `idgaleri` int(11) DEFAULT NULL,
  `foto` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`idgaleri_detail`),
  KEY `galeri_fk_idx` (`idgaleri`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `galeri_detail`
--

INSERT INTO `galeri_detail` (`idgaleri_detail`, `tgl_post`, `keterangan`, `idgaleri`, `foto`) VALUES
(7, '2019-11-27 14:40:00', 'Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed to an unknown typesetter in the 15th century who is thought to have scrambled parts of Cicero''s De Finibus Bonorum et Malorum for use in a type specimen book.', 1, '1.jpg'),
(8, '2019-11-27 14:40:00', 'gfdssa', 1, '2.jpg'),
(9, '2019-11-28 10:19:09', 'Hogwarts', 2, 'hogwarts.jpg'),
(11, '2019-11-28 10:21:00', 'Albus Dumbledore', 2, 'Albus_Dumbledore.jpg'),
(12, '2019-11-28 10:21:00', 'Sirius Black', 2, 'Sirius_Black.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `galleries`
--

CREATE TABLE IF NOT EXISTS `galleries` (
  `gallery_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `gallery_name` varchar(225) NOT NULL,
  `slug_gallery` varchar(225) NOT NULL,
  `image` varchar(225) NOT NULL,
  `gallery_description` text NOT NULL,
  `position` varchar(100) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` varchar(100) NOT NULL,
  PRIMARY KEY (`gallery_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `halaman`
--

CREATE TABLE IF NOT EXISTS `halaman` (
  `idhalaman` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(45) DEFAULT NULL,
  `tgl_post` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `slug` varchar(256) DEFAULT NULL,
  `isi` longtext,
  `aktif` int(1) NOT NULL DEFAULT '1',
  `foto` varchar(256) DEFAULT NULL,
  `iduser` int(11) DEFAULT NULL,
  PRIMARY KEY (`idhalaman`),
  KEY `iduser_idx` (`iduser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE IF NOT EXISTS `kategori` (
  `idkategori` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(256) NOT NULL,
  `statis` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idkategori`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`idkategori`, `nama`, `statis`) VALUES
(1, 'Sosial', 0),
(2, 'Halaman', 1),
(3, 'Featured', 0);

-- --------------------------------------------------------

--
-- Table structure for table `komentar`
--

CREATE TABLE IF NOT EXISTS `komentar` (
  `idkomentar` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(256) DEFAULT NULL,
  `email` varchar(256) DEFAULT NULL,
  `tgl_komentar` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `komentar` longtext,
  `idkonten` int(11) NOT NULL DEFAULT '0' COMMENT 'idkonten tersambung ke idberita atau idwawancara',
  `tipe` int(11) DEFAULT NULL COMMENT '1=wawancara;',
  PRIMARY KEY (`idkomentar`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `komentar`
--

INSERT INTO `komentar` (`idkomentar`, `nama`, `email`, `tgl_komentar`, `komentar`, `idkonten`, `tipe`) VALUES
(1, 'Winchester', 'winchester@email.com', '2019-11-06 13:46:29', 'Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis.', 1, 1),
(2, 'Henstridge', 'henstridge@email.com', '2019-11-06 13:47:15', 'Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis.', 1, 2),
(3, 'Wayne', 'wayne@email.com', '2019-11-07 02:20:30', 'Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed to an unknown typesetter in the 15th century who is thought to have scrambled parts of Cicero''s De Finibus Bonorum e', 1, 1),
(4, 'NYC', 'siriusblack@hg.uk', '2019-11-08 10:11:52', 'Reference site about Lorem Ipsum, giving information on its origins, as well as a random Lipsum generator.', 3, 1),
(5, 'NYC', 'siriusblack@hg.uk', '2019-11-08 10:11:52', 'Reference site about Lorem Ipsum, giving information on its origins, as well as a random Lipsum generator.', 1, 2),
(6, 'Sirius', 'siriusblack@hg.uk', '2019-11-13 02:40:43', 'Nice', 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `kota`
--

CREATE TABLE IF NOT EXISTS `kota` (
  `idkota` int(11) NOT NULL AUTO_INCREMENT,
  `kota` varchar(45) DEFAULT NULL,
  `idprovinsi` int(11) DEFAULT NULL,
  PRIMARY KEY (`idkota`),
  KEY `idprovinsi_idx` (`idprovinsi`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9272 ;

--
-- Dumping data for table `kota`
--

INSERT INTO `kota` (`idkota`, `kota`, `idprovinsi`) VALUES
(1101, 'KAB. ACEH SELATAN', 11),
(1102, 'KAB. ACEH TENGGARA', 11),
(1103, 'KAB. ACEH TIMUR', 11),
(1104, 'KAB. ACEH TENGAH', 11),
(1105, 'KAB. ACEH BARAT', 11),
(1106, 'KAB. ACEH BESAR', 11),
(1107, 'KAB. PIDIE', 11),
(1108, 'KAB. ACEH UTARA', 11),
(1109, 'KAB. SIMEULUE', 11),
(1110, 'KAB. ACEH SINGKIL', 11),
(1111, 'KAB. BIREUEN', 11),
(1112, 'KAB. ACEH BARAT DAYA', 11),
(1113, 'KAB. GAYO LUES', 11),
(1114, 'KAB. ACEH JAYA', 11),
(1115, 'KAB. NAGAN RAYA', 11),
(1116, 'KAB. ACEH TAMIANG', 11),
(1117, 'KAB. BENER MERIAH', 11),
(1118, 'KAB. PIDIE JAYA', 11),
(1171, 'KOTA BANDA ACEH', 11),
(1172, 'KOTA SABANG', 11),
(1173, 'KOTA LHOKSEUMAWE', 11),
(1174, 'KOTA LANGSA', 11),
(1175, 'KOTA SUBULUSSALAM', 11),
(1201, 'KAB. TAPANULI TENGAH', 12),
(1202, 'KAB. TAPANULI UTARA', 12),
(1203, 'KAB. TAPANULI SELATAN', 12),
(1204, 'KAB. NIAS', 12),
(1205, 'KAB. LANGKAT', 12),
(1206, 'KAB. KARO', 12),
(1207, 'KAB. DELI SERDANG', 12),
(1208, 'KAB. SIMALUNGUN', 12),
(1209, 'KAB. ASAHAN', 12),
(1210, 'KAB. LABUHANBATU', 12),
(1211, 'KAB. DAIRI', 12),
(1212, 'KAB. TOBA SAMOSIR', 12),
(1213, 'KAB. MANDAILING NATAL', 12),
(1214, 'KAB. NIAS SELATAN', 12),
(1215, 'KAB. PAKPAK BHARAT', 12),
(1216, 'KAB. HUMBANG HASUNDUTAN', 12),
(1217, 'KAB. SAMOSIR', 12),
(1218, 'KAB. SERDANG BEDAGAI', 12),
(1219, 'KAB. BATU BARA', 12),
(1220, 'KAB. PADANG LAWAS UTARA', 12),
(1221, 'KAB. PADANG LAWAS', 12),
(1222, 'KAB. LABUHANBATU SELATAN', 12),
(1223, 'KAB. LABUHANBATU UTARA', 12),
(1224, 'KAB. NIAS UTARA', 12),
(1225, 'KAB. NIAS BARAT', 12),
(1271, 'KOTA MEDAN', 12),
(1272, 'KOTA PEMATANG SIANTAR', 12),
(1273, 'KOTA SIBOLGA', 12),
(1274, 'KOTA TANJUNG BALAI', 12),
(1275, 'KOTA BINJAI', 12),
(1276, 'KOTA TEBING TINGGI', 12),
(1277, 'KOTA PADANGSIDIMPUAN', 12),
(1278, 'KOTA GUNUNGSITOLI', 12),
(1301, 'KAB. PESISIR SELATAN', 13),
(1302, 'KAB. SOLOK', 13),
(1303, 'KAB. SIJUNJUNG', 13),
(1304, 'KAB. TANAH DATAR', 13),
(1305, 'KAB. PADANG PARIAMAN', 13),
(1306, 'KAB. AGAM', 13),
(1307, 'KAB. LIMA PULUH KOTA', 13),
(1308, 'KAB. PASAMAN', 13),
(1309, 'KAB. KEPULAUAN MENTAWAI', 13),
(1310, 'KAB. DHARMASRAYA', 13),
(1311, 'KAB. SOLOK SELATAN', 13),
(1312, 'KAB. PASAMAN BARAT', 13),
(1371, 'KOTA PADANG', 13),
(1372, 'KOTA SOLOK', 13),
(1373, 'KOTA SAWAHLUNTO', 13),
(1374, 'KOTA PADANG PANJANG', 13),
(1375, 'KOTA BUKITTINGGI', 13),
(1376, 'KOTA PAYAKUMBUH', 13),
(1377, 'KOTA PARIAMAN', 13),
(1401, 'KAB. KAMPAR', 14),
(1402, 'KAB. INDRAGIRI HULU', 14),
(1403, 'KAB. BENGKALIS', 14),
(1404, 'KAB. INDRAGIRI HILIR', 14),
(1405, 'KAB. PELALAWAN', 14),
(1406, 'KAB. ROKAN HULU', 14),
(1407, 'KAB. ROKAN HILIR', 14),
(1408, 'KAB. SIAK', 14),
(1409, 'KAB. KUANTAN SINGINGI', 14),
(1410, 'KAB. KEPULAUAN MERANTI', 14),
(1471, 'KOTA PEKANBARU', 14),
(1472, 'KOTA DUMAI', 14),
(1501, 'KAB. KERINCI', 15),
(1502, 'KAB. MERANGIN', 15),
(1503, 'KAB. SAROLANGUN', 15),
(1504, 'KAB. BATANGHARI', 15),
(1505, 'KAB. MUARO JAMBI', 15),
(1506, 'KAB. TANJUNG JABUNG BARAT', 15),
(1507, 'KAB. TANJUNG JABUNG TIMUR', 15),
(1508, 'KAB. BUNGO', 15),
(1509, 'KAB. TEBO', 15),
(1571, 'KOTA JAMBI', 15),
(1572, 'KOTA SUNGAI PENUH', 15),
(1601, 'KAB. OGAN KOMERING ULU', 16),
(1602, 'KAB. OGAN KOMERING ILIR', 16),
(1603, 'KAB. MUARA ENIM', 16),
(1604, 'KAB. LAHAT', 16),
(1605, 'KAB. MUSI RAWAS', 16),
(1606, 'KAB. MUSI BANYUASIN', 16),
(1607, 'KAB. BANYUASIN', 16),
(1608, 'KAB. OGAN KOMERING ULU TIMUR', 16),
(1609, 'KAB. OGAN KOMERING ULU SELATAN', 16),
(1610, 'KAB. OGAN ILIR', 16),
(1611, 'KAB. EMPAT LAWANG', 16),
(1612, 'KAB. PENUKAL ABAB LEMATANG ILIR', 16),
(1613, 'KAB. MUSI RAWAS UTARA', 16),
(1671, 'KOTA PALEMBANG', 16),
(1672, 'KOTA PAGAR ALAM', 16),
(1673, 'KOTA LUBUK LINGGAU', 16),
(1674, 'KOTA PRABUMULIH', 16),
(1701, 'KAB. BENGKULU SELATAN', 17),
(1702, 'KAB. REJANG LEBONG', 17),
(1703, 'KAB. BENGKULU UTARA', 17),
(1704, 'KAB. KAUR', 17),
(1705, 'KAB. SELUMA', 17),
(1706, 'KAB. MUKO MUKO', 17),
(1707, 'KAB. LEBONG', 17),
(1708, 'KAB. KEPAHIANG', 17),
(1709, 'KAB. BENGKULU TENGAH', 17),
(1771, 'KOTA BENGKULU', 17),
(1801, 'KAB. LAMPUNG SELATAN', 18),
(1802, 'KAB. LAMPUNG TENGAH', 18),
(1803, 'KAB. LAMPUNG UTARA', 18),
(1804, 'KAB. LAMPUNG BARAT', 18),
(1805, 'KAB. TULANG BAWANG', 18),
(1806, 'KAB. TANGGAMUS', 18),
(1807, 'KAB. LAMPUNG TIMUR', 18),
(1808, 'KAB. WAY KANAN', 18),
(1809, 'KAB. PESAWARAN', 18),
(1810, 'KAB. PRINGSEWU', 18),
(1811, 'KAB. MESUJI', 18),
(1812, 'KAB. TULANG BAWANG BARAT', 18),
(1813, 'KAB. PESISIR BARAT', 18),
(1871, 'KOTA BANDAR LAMPUNG', 18),
(1872, 'KOTA METRO', 18),
(1901, 'KAB. BANGKA', 19),
(1902, 'KAB. BELITUNG', 19),
(1903, 'KAB. BANGKA SELATAN', 19),
(1904, 'KAB. BANGKA TENGAH', 19),
(1905, 'KAB. BANGKA BARAT', 19),
(1906, 'KAB. BELITUNG TIMUR', 19),
(1971, 'KOTA PANGKAL PINANG', 19),
(2101, 'KAB. BINTAN', 21),
(2102, 'KAB. KARIMUN', 21),
(2103, 'KAB. NATUNA', 21),
(2104, 'KAB. LINGGA', 21),
(2105, 'KAB. KEPULAUAN ANAMBAS', 21),
(2171, 'KOTA BATAM', 21),
(2172, 'KOTA TANJUNG PINANG', 21),
(3101, 'KAB. ADM. KEP. SERIBU', 31),
(3171, 'KOTA ADM. JAKARTA PUSAT', 31),
(3172, 'KOTA ADM. JAKARTA UTARA', 31),
(3173, 'KOTA ADM. JAKARTA BARAT', 31),
(3174, 'KOTA ADM. JAKARTA SELATAN', 31),
(3175, 'KOTA ADM. JAKARTA TIMUR', 31),
(3201, 'KAB. BOGOR', 32),
(3202, 'KAB. SUKABUMI', 32),
(3203, 'KAB. CIANJUR', 32),
(3204, 'KAB. BANDUNG', 32),
(3205, 'KAB. GARUT', 32),
(3206, 'KAB. TASIKMALAYA', 32),
(3207, 'KAB. CIAMIS', 32),
(3208, 'KAB. KUNINGAN', 32),
(3209, 'KAB. CIREBON', 32),
(3210, 'KAB. MAJALENGKA', 32),
(3211, 'KAB. SUMEDANG', 32),
(3212, 'KAB. INDRAMAYU', 32),
(3213, 'KAB. SUBANG', 32),
(3214, 'KAB. PURWAKARTA', 32),
(3215, 'KAB. KARAWANG', 32),
(3216, 'KAB. BEKASI', 32),
(3217, 'KAB. BANDUNG BARAT', 32),
(3218, 'KAB. PANGANDARAN', 32),
(3271, 'KOTA BOGOR', 32),
(3272, 'KOTA SUKABUMI', 32),
(3273, 'KOTA BANDUNG', 32),
(3274, 'KOTA CIREBON', 32),
(3275, 'KOTA BEKASI', 32),
(3276, 'KOTA DEPOK', 32),
(3277, 'KOTA CIMAHI', 32),
(3278, 'KOTA TASIKMALAYA', 32),
(3279, 'KOTA BANJAR', 32),
(3301, 'KAB. CILACAP', 33),
(3302, 'KAB. BANYUMAS', 33),
(3303, 'KAB. PURBALINGGA', 33),
(3304, 'KAB. BANJARNEGARA', 33),
(3305, 'KAB. KEBUMEN', 33),
(3306, 'KAB. PURWOREJO', 33),
(3307, 'KAB. WONOSOBO', 33),
(3308, 'KAB. MAGELANG', 33),
(3309, 'KAB. BOYOLALI', 33),
(3310, 'KAB. KLATEN', 33),
(3311, 'KAB. SUKOHARJO', 33),
(3312, 'KAB. WONOGIRI', 33),
(3313, 'KAB. KARANGANYAR', 33),
(3314, 'KAB. SRAGEN', 33),
(3315, 'KAB. GROBOGAN', 33),
(3316, 'KAB. BLORA', 33),
(3317, 'KAB. REMBANG', 33),
(3318, 'KAB. PATI', 33),
(3319, 'KAB. KUDUS', 33),
(3320, 'KAB. JEPARA', 33),
(3321, 'KAB. DEMAK', 33),
(3322, 'KAB. SEMARANG', 33),
(3323, 'KAB. TEMANGGUNG', 33),
(3324, 'KAB. KENDAL', 33),
(3325, 'KAB. BATANG', 33),
(3326, 'KAB. PEKALONGAN', 33),
(3327, 'KAB. PEMALANG', 33),
(3328, 'KAB. TEGAL', 33),
(3329, 'KAB. BREBES', 33),
(3371, 'KOTA MAGELANG', 33),
(3372, 'KOTA SURAKARTA', 33),
(3373, 'KOTA SALATIGA', 33),
(3374, 'KOTA SEMARANG', 33),
(3375, 'KOTA PEKALONGAN', 33),
(3376, 'KOTA TEGAL', 33),
(3401, 'KAB. KULON PROGO', 34),
(3402, 'KAB. BANTUL', 34),
(3403, 'KAB. GUNUNG KIDUL', 34),
(3404, 'KAB. SLEMAN', 34),
(3471, 'KOTA YOGYAKARTA', 34),
(3501, 'KAB. PACITAN', 35),
(3502, 'KAB. PONOROGO', 35),
(3503, 'KAB. TRENGGALEK', 35),
(3504, 'KAB. TULUNGAGUNG', 35),
(3505, 'KAB. BLITAR', 35),
(3506, 'KAB. KEDIRI', 35),
(3507, 'KAB. MALANG', 35),
(3508, 'KAB. LUMAJANG', 35),
(3509, 'KAB. JEMBER', 35),
(3510, 'KAB. BANYUWANGI', 35),
(3511, 'KAB. BONDOWOSO', 35),
(3512, 'KAB. SITUBONDO', 35),
(3513, 'KAB. PROBOLINGGO', 35),
(3514, 'KAB. PASURUAN', 35),
(3515, 'KAB. SIDOARJO', 35),
(3516, 'KAB. MOJOKERTO', 35),
(3517, 'KAB. JOMBANG', 35),
(3518, 'KAB. NGANJUK', 35),
(3519, 'KAB. MADIUN', 35),
(3520, 'KAB. MAGETAN', 35),
(3521, 'KAB. NGAWI', 35),
(3522, 'KAB. BOJONEGORO', 35),
(3523, 'KAB. TUBAN', 35),
(3524, 'KAB. LAMONGAN', 35),
(3525, 'KAB. GRESIK', 35),
(3526, 'KAB. BANGKALAN', 35),
(3527, 'KAB. SAMPANG', 35),
(3528, 'KAB. PAMEKASAN', 35),
(3529, 'KAB. SUMENEP', 35),
(3571, 'KOTA KEDIRI', 35),
(3572, 'KOTA BLITAR', 35),
(3573, 'KOTA MALANG', 35),
(3574, 'KOTA PROBOLINGGO', 35),
(3575, 'KOTA PASURUAN', 35),
(3576, 'KOTA MOJOKERTO', 35),
(3577, 'KOTA MADIUN', 35),
(3578, 'KOTA SURABAYA', 35),
(3579, 'KOTA BATU', 35),
(3601, 'KAB. PANDEGLANG', 36),
(3602, 'KAB. LEBAK', 36),
(3603, 'KAB. TANGERANG', 36),
(3604, 'KAB. SERANG', 36),
(3671, 'KOTA TANGERANG', 36),
(3672, 'KOTA CILEGON', 36),
(3673, 'KOTA SERANG', 36),
(3674, 'KOTA TANGERANG SELATAN', 36),
(5101, 'KAB. JEMBRANA', 51),
(5102, 'KAB. TABANAN', 51),
(5103, 'KAB. BADUNG', 51),
(5104, 'KAB. GIANYAR', 51),
(5105, 'KAB. KLUNGKUNG', 51),
(5106, 'KAB. BANGLI', 51),
(5107, 'KAB. KARANGASEM', 51),
(5108, 'KAB. BULELENG', 51),
(5171, 'KOTA DENPASAR', 51),
(5201, 'KAB. LOMBOK BARAT', 52),
(5202, 'KAB. LOMBOK TENGAH', 52),
(5203, 'KAB. LOMBOK TIMUR', 52),
(5204, 'KAB. SUMBAWA', 52),
(5205, 'KAB. DOMPU', 52),
(5206, 'KAB. BIMA', 52),
(5207, 'KAB. SUMBAWA BARAT', 52),
(5208, 'KAB. LOMBOK UTARA', 52),
(5271, 'KOTA MATARAM', 52),
(5272, 'KOTA BIMA', 52),
(5301, 'KAB. KUPANG', 53),
(5302, 'KAB TIMOR TENGAH SELATAN', 53),
(5303, 'KAB. TIMOR TENGAH UTARA', 53),
(5304, 'KAB. BELU', 53),
(5305, 'KAB. ALOR', 53),
(5306, 'KAB. FLORES TIMUR', 53),
(5307, 'KAB. SIKKA', 53),
(5308, 'KAB. ENDE', 53),
(5309, 'KAB. NGADA', 53),
(5310, 'KAB. MANGGARAI', 53),
(5311, 'KAB. SUMBA TIMUR', 53),
(5312, 'KAB. SUMBA BARAT', 53),
(5313, 'KAB. LEMBATA', 53),
(5314, 'KAB. ROTE NDAO', 53),
(5315, 'KAB. MANGGARAI BARAT', 53),
(5316, 'KAB. NAGEKEO', 53),
(5317, 'KAB. SUMBA TENGAH', 53),
(5318, 'KAB. SUMBA BARAT DAYA', 53),
(5319, 'KAB. MANGGARAI TIMUR', 53),
(5320, 'KAB. SABU RAIJUA', 53),
(5321, 'KAB. MALAKA', 53),
(5371, 'KOTA KUPANG', 53),
(6101, 'KAB. SAMBAS', 61),
(6102, 'KAB. MEMPAWAH', 61),
(6103, 'KAB. SANGGAU', 61),
(6104, 'KAB. KETAPANG', 61),
(6105, 'KAB. SINTANG', 61),
(6106, 'KAB. KAPUAS HULU', 61),
(6107, 'KAB. BENGKAYANG', 61),
(6108, 'KAB. LANDAK', 61),
(6109, 'KAB. SEKADAU', 61),
(6110, 'KAB. MELAWI', 61),
(6111, 'KAB. KAYONG UTARA', 61),
(6112, 'KAB. KUBU RAYA', 61),
(6171, 'KOTA PONTIANAK', 61),
(6172, 'KOTA SINGKAWANG', 61),
(6201, 'KAB. KOTAWARINGIN BARAT', 62),
(6202, 'KAB. KOTAWARINGIN TIMUR', 62),
(6203, 'KAB. KAPUAS', 62),
(6204, 'KAB. BARITO SELATAN', 62),
(6205, 'KAB. BARITO UTARA', 62),
(6206, 'KAB. KATINGAN', 62),
(6207, 'KAB. SERUYAN', 62),
(6208, 'KAB. SUKAMARA', 62),
(6209, 'KAB. LAMANDAU', 62),
(6210, 'KAB. GUNUNG MAS', 62),
(6211, 'KAB. PULANG PISAU', 62),
(6212, 'KAB. MURUNG RAYA', 62),
(6213, 'KAB. BARITO TIMUR', 62),
(6271, 'KOTA PALANGKARAYA', 62),
(6301, 'KAB. TANAH LAUT', 63),
(6302, 'KAB. KOTABARU', 63),
(6303, 'KAB. BANJAR', 63),
(6304, 'KAB. BARITO KUALA', 63),
(6305, 'KAB. TAPIN', 63),
(6306, 'KAB. HULU SUNGAI SELATAN', 63),
(6307, 'KAB. HULU SUNGAI TENGAH', 63),
(6308, 'KAB. HULU SUNGAI UTARA', 63),
(6309, 'KAB. TABALONG', 63),
(6310, 'KAB. TANAH BUMBU', 63),
(6311, 'KAB. BALANGAN', 63),
(6371, 'KOTA BANJARMASIN', 63),
(6372, 'KOTA BANJARBARU', 63),
(6401, 'KAB. PASER', 64),
(6402, 'KAB. KUTAI KARTANEGARA', 64),
(6403, 'KAB. BERAU', 64),
(6407, 'KAB. KUTAI BARAT', 64),
(6408, 'KAB. KUTAI TIMUR', 64),
(6409, 'KAB. PENAJAM PASER UTARA', 64),
(6411, 'KAB. MAHAKAM ULU', 64),
(6471, 'KOTA BALIKPAPAN', 64),
(6472, 'KOTA SAMARINDA', 64),
(6474, 'KOTA BONTANG', 64),
(6501, 'KAB. BULUNGAN', 65),
(6502, 'KAB. MALINAU', 65),
(6503, 'KAB. NUNUKAN', 65),
(6504, 'KAB. TANA TIDUNG', 65),
(6571, 'KOTA TARAKAN', 65),
(7101, 'KAB. BOLAANG MONGONDOW', 71),
(7102, 'KAB. MINAHASA', 71),
(7103, 'KAB. KEPULAUAN SANGIHE', 71),
(7104, 'KAB. KEPULAUAN TALAUD', 71),
(7105, 'KAB. MINAHASA SELATAN', 71),
(7106, 'KAB. MINAHASA UTARA', 71),
(7107, 'KAB. MINAHASA TENGGARA', 71),
(7108, 'KAB. BOLAANG MONGONDOW UTARA', 71),
(7109, 'KAB. KEP. SIAU TAGULANDANG BIARO', 71),
(7110, 'KAB. BOLAANG MONGONDOW TIMUR', 71),
(7111, 'KAB. BOLAANG MONGONDOW SELATAN', 71),
(7171, 'KOTA MANADO', 71),
(7172, 'KOTA BITUNG', 71),
(7173, 'KOTA TOMOHON', 71),
(7174, 'KOTA KOTAMOBAGU', 71),
(7201, 'KAB. BANGGAI', 72),
(7202, 'KAB. POSO', 72),
(7203, 'KAB. DONGGALA', 72),
(7204, 'KAB. TOLI TOLI', 72),
(7205, 'KAB. BUOL', 72),
(7206, 'KAB. MOROWALI', 72),
(7207, 'KAB. BANGGAI KEPULAUAN', 72),
(7208, 'KAB. PARIGI MOUTONG', 72),
(7209, 'KAB. TOJO UNA UNA', 72),
(7210, 'KAB. SIGI', 72),
(7211, 'KAB. BANGGAI LAUT', 72),
(7212, 'KAB. MOROWALI UTARA', 72),
(7271, 'KOTA PALU', 72),
(7301, 'KAB. KEPULAUAN SELAYAR', 73),
(7302, 'KAB. BULUKUMBA', 73),
(7303, 'KAB. BANTAENG', 73),
(7304, 'KAB. JENEPONTO', 73),
(7305, 'KAB. TAKALAR', 73),
(7306, 'KAB. GOWA', 73),
(7307, 'KAB. SINJAI', 73),
(7308, 'KAB. BONE', 73),
(7309, 'KAB. MAROS', 73),
(7310, 'KAB. PANGKAJENE KEPULAUAN', 73),
(7311, 'KAB. BARRU', 73),
(7312, 'KAB. SOPPENG', 73),
(7313, 'KAB. WAJO', 73),
(7314, 'KAB. SIDENRENG RAPPANG', 73),
(7315, 'KAB. PINRANG', 73),
(7316, 'KAB. ENREKANG', 73),
(7317, 'KAB. LUWU', 73),
(7318, 'KAB. TANA TORAJA', 73),
(7322, 'KAB. LUWU UTARA', 73),
(7324, 'KAB. LUWU TIMUR', 73),
(7326, 'KAB. TORAJA UTARA', 73),
(7371, 'KOTA MAKASSAR', 73),
(7372, 'KOTA PARE PARE', 73),
(7373, 'KOTA PALOPO', 73),
(7401, 'KAB. KOLAKA', 74),
(7402, 'KAB. KONAWE', 74),
(7403, 'KAB. MUNA', 74),
(7404, 'KAB. BUTON', 74),
(7405, 'KAB. KONAWE SELATAN', 74),
(7406, 'KAB. BOMBANA', 74),
(7407, 'KAB. WAKATOBI', 74),
(7408, 'KAB. KOLAKA UTARA', 74),
(7409, 'KAB. KONAWE UTARA', 74),
(7410, 'KAB. BUTON UTARA', 74),
(7411, 'KAB. KOLAKA TIMUR', 74),
(7412, 'KAB. KONAWE KEPULAUAN', 74),
(7413, 'KAB. MUNA BARAT', 74),
(7414, 'KAB. BUTON TENGAH', 74),
(7415, 'KAB. BUTON SELATAN', 74),
(7471, 'KOTA KENDARI', 74),
(7472, 'KOTA BAU BAU', 74),
(7501, 'KAB. GORONTALO', 75),
(7502, 'KAB. BOALEMO', 75),
(7503, 'KAB. BONE BOLANGO', 75),
(7504, 'KAB. PAHUWATO', 75),
(7505, 'KAB. GORONTALO UTARA', 75),
(7571, 'KOTA GORONTALO', 75),
(7601, 'KAB. MAMUJU UTARA', 76),
(7602, 'KAB. MAMUJU', 76),
(7603, 'KAB. MAMASA', 76),
(7604, 'KAB. POLEWALI MANDAR', 76),
(7605, 'KAB. MAJENE', 76),
(7606, 'KAB. MAMUJU TENGAH', 76),
(8101, 'KAB. MALUKU TENGAH', 81),
(8102, 'KAB. MALUKU TENGGARA', 81),
(8103, 'KAB MALUKU TENGGARA BARAT', 81),
(8104, 'KAB. BURU', 81),
(8105, 'KAB. SERAM BAGIAN TIMUR', 81),
(8106, 'KAB. SERAM BAGIAN BARAT', 81),
(8107, 'KAB. KEPULAUAN ARU', 81),
(8108, 'KAB. MALUKU BARAT DAYA', 81),
(8109, 'KAB. BURU SELATAN', 81),
(8171, 'KOTA AMBON', 81),
(8172, 'KOTA TUAL', 81),
(8201, 'KAB. HALMAHERA BARAT', 82),
(8202, 'KAB. HALMAHERA TENGAH', 82),
(8203, 'KAB. HALMAHERA UTARA', 82),
(8204, 'KAB. HALMAHERA SELATAN', 82),
(8205, 'KAB. KEPULAUAN SULA', 82),
(8206, 'KAB. HALMAHERA TIMUR', 82),
(8207, 'KAB. PULAU MOROTAI', 82),
(8208, 'KAB. PULAU TALIABU', 82),
(8271, 'KOTA TERNATE', 82),
(8272, 'KOTA TIDORE KEPULAUAN', 82),
(9101, 'KAB. MERAUKE', 91),
(9102, 'KAB. JAYAWIJAYA', 91),
(9103, 'KAB. JAYAPURA', 91),
(9104, 'KAB. NABIRE', 91),
(9105, 'KAB. KEPULAUAN YAPEN', 91),
(9106, 'KAB. BIAK NUMFOR', 91),
(9107, 'KAB. PUNCAK JAYA', 91),
(9108, 'KAB. PANIAI', 91),
(9109, 'KAB. MIMIKA', 91),
(9110, 'KAB. SARMI', 91),
(9111, 'KAB. KEEROM', 91),
(9112, 'KAB PEGUNUNGAN BINTANG', 91),
(9113, 'KAB. YAHUKIMO', 91),
(9114, 'KAB. TOLIKARA', 91),
(9115, 'KAB. WAROPEN', 91),
(9116, 'KAB. BOVEN DIGOEL', 91),
(9117, 'KAB. MAPPI', 91),
(9118, 'KAB. ASMAT', 91),
(9119, 'KAB. SUPIORI', 91),
(9120, 'KAB. MAMBERAMO RAYA', 91),
(9121, 'KAB. MAMBERAMO TENGAH', 91),
(9122, 'KAB. YALIMO', 91),
(9123, 'KAB. LANNY JAYA', 91),
(9124, 'KAB. NDUGA', 91),
(9125, 'KAB. PUNCAK', 91),
(9126, 'KAB. DOGIYAI', 91),
(9127, 'KAB. INTAN JAYA', 91),
(9128, 'KAB. DEIYAI', 91),
(9171, 'KOTA JAYAPURA', 91),
(9201, 'KAB. SORONG', 92),
(9202, 'KAB. MANOKWARI', 92),
(9203, 'KAB. FAK FAK', 92),
(9204, 'KAB. SORONG SELATAN', 92),
(9205, 'KAB. RAJA AMPAT', 92),
(9206, 'KAB. TELUK BINTUNI', 92),
(9207, 'KAB. TELUK WONDAMA', 92),
(9208, 'KAB. KAIMANA', 92),
(9209, 'KAB. TAMBRAUW', 92),
(9210, 'KAB. MAYBRAT', 92),
(9211, 'KAB. MANOKWARI SELATAN', 92),
(9212, 'KAB. PEGUNUNGAN ARFAK', 92),
(9271, 'KOTA SORONG', 92);

-- --------------------------------------------------------

--
-- Table structure for table `profesi`
--

CREATE TABLE IF NOT EXISTS `profesi` (
  `idprofesi` int(11) NOT NULL AUTO_INCREMENT,
  `nama_profesi` varchar(254) DEFAULT NULL,
  PRIMARY KEY (`idprofesi`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=89 ;

--
-- Dumping data for table `profesi`
--

INSERT INTO `profesi` (`idprofesi`, `nama_profesi`) VALUES
(1, 'Anggota BPK'),
(2, 'Anggota DPD'),
(5, 'Anggota DPRD Kabupaten / kota'),
(7, 'Anggota DPRD Provinsi'),
(8, 'Anggota DPR-RI'),
(9, 'Anggota Kabinet / Kementerian'),
(10, 'Anggota Mahkamah Konstitusi'),
(11, 'Apoteker'),
(12, 'Arsitek'),
(13, 'belum / tidak bekerja'),
(14, 'Biarawati'),
(15, 'Bidan'),
(16, 'Bupati'),
(17, 'Buruh Nelayan / Perikanan'),
(18, 'Buruh Harian Lepas'),
(19, 'Buruh Peternakan'),
(20, 'Buruh Pertanian / Perkebunan'),
(21, 'Dokter'),
(22, 'Dosen'),
(23, 'Duta Besar'),
(24, 'Gubernur'),
(25, 'Guru-PNS'),
(26, 'Guru-Honorer'),
(27, 'Imam Masjid'),
(28, 'Industri'),
(29, 'Juru Masak'),
(30, 'Pegawai BUMD'),
(31, 'Pegawai BUMN'),
(32, 'Karyawan Swasta'),
(33, 'Kepala Desa'),
(34, 'Polri'),
(35, 'Konstruksi'),
(36, 'Konsultan'),
(37, 'Mekanik'),
(38, 'Mengurus Rumah Tangga'),
(39, 'Nelayan / Perikanan'),
(40, 'Notaris'),
(41, 'Paraji'),
(42, 'Paranormal'),
(43, 'Pastur'),
(44, 'Pedagang'),
(45, 'Pegawai Negeri Sipil'),
(46, 'Pelajar / Mahasiswa'),
(47, 'Pelaut'),
(48, 'Asisten Rumah Tangga'),
(49, 'Penata Busana'),
(50, 'Penata Rambut'),
(51, 'Penata Rias'),
(52, 'Pendeta'),
(53, 'Peneliti'),
(54, 'Pengacara'),
(55, 'Pensiunan'),
(56, 'Penerjemah'),
(57, 'Penyiar Radio'),
(58, 'Penyiar Televisi'),
(59, 'Perancang Busana'),
(60, 'Perangkat Desa'),
(61, 'Perawat'),
(62, 'Petani / Berkebun'),
(63, 'Peternak'),
(64, 'Pialang'),
(65, 'Pilot'),
(66, 'Presiden'),
(67, 'Promotor Acara'),
(68, 'Psikiater / Psikolog'),
(69, 'Seniman'),
(70, 'Sopir'),
(71, 'Tabib'),
(72, 'Tentara Nasional Indonesia / TNI'),
(73, 'Tukang Batu'),
(74, 'Tukang Cukur'),
(75, 'Tukang Gigi'),
(76, 'Tukang Jahit'),
(77, 'Tukang Kayu'),
(78, 'Tukang Las'),
(79, 'Tukang Listrik'),
(80, 'Tukang Sol Sepatu'),
(81, 'Ustadz / Mubaliqh'),
(82, 'Wakil Bupati'),
(83, 'Wakil Gubernur'),
(84, 'Wakil Presiden'),
(85, 'Wakil Walikota'),
(86, 'Walikota'),
(87, 'Wartawan'),
(88, 'Wiraswasta');

-- --------------------------------------------------------

--
-- Table structure for table `provinsi`
--

CREATE TABLE IF NOT EXISTS `provinsi` (
  `idprovinsi` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`idprovinsi`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=93 ;

--
-- Dumping data for table `provinsi`
--

INSERT INTO `provinsi` (`idprovinsi`, `nama`) VALUES
(11, 'Aceh'),
(12, 'Sumatera Utara'),
(13, 'Sumatera Barat'),
(14, 'Riau'),
(15, 'Jambi'),
(16, 'Sumatera Selatan'),
(17, 'Bengkulu'),
(18, 'Lampung'),
(19, 'Kepulauan Bangka Belitung'),
(21, 'Kepulauan Riau'),
(31, 'DKI Jakarta'),
(32, 'Jawa Barat'),
(33, 'Jawa Tengah'),
(34, 'DI Yogyakarta'),
(35, 'Jawa Timur'),
(36, 'Banten'),
(51, 'Bali'),
(52, 'Nusa Tenggara Barat'),
(53, 'Nusa Tenggara Timur'),
(61, 'Kalimantan Barat'),
(62, 'Kalimantan Tengah'),
(63, 'Kalimantan Selatan'),
(64, 'Kalimantan Timur'),
(65, 'Kalimantan Utara'),
(71, 'Sulawesi Utara'),
(72, 'Sulawesi Tengah'),
(73, 'Sulawesi Selatan'),
(74, 'Sulawesi Tenggara'),
(75, 'Gorontalo'),
(76, 'Sulawesi Barat'),
(81, 'Maluku'),
(82, 'Maluku Utara'),
(91, 'Papua Barat'),
(92, 'Papua');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `idroles` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`idroles`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`idroles`, `nama`) VALUES
(1, 'Super Admin'),
(2, 'Ketua Angkatan'),
(3, 'Anggota');

-- --------------------------------------------------------

--
-- Table structure for table `wawancara`
--

CREATE TABLE IF NOT EXISTS `wawancara` (
  `idwawancara` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(256) NOT NULL,
  `slug` varchar(256) DEFAULT NULL,
  `isi` longtext,
  `tgl_wawancara` datetime DEFAULT NULL,
  `foto` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`idwawancara`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `wawancara`
--

INSERT INTO `wawancara` (`idwawancara`, `judul`, `slug`, `isi`, `tgl_wawancara`, `foto`) VALUES
(1, 'New York Story', '1-new-york-story', '<p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.</p>\r\n<p>Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima. Eodem modo typi, qui nunc nobis videntur parum clari, fiant sollemnes in futurum.</p>', '2019-11-06 10:36:08', 'new-york-city-street-11.jpg'),
(2, 'HOGWARTS Story', '3-hogwarts-story', '<p class="f4 cl-white mt0 mb16"><em>Lorem ipsum</em>, or&nbsp;<em>lipsum</em>&nbsp;as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed to an unknown typesetter in the 15th century who is thought to have scrambled parts of Cicero''s&nbsp;<em>De Finibus Bonorum et Malorum</em>&nbsp;for use in a type specimen book. It usually begins with:</p>\r\n<blockquote class="page-section__blockquote">&ldquo;Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.&rdquo;</blockquote>\r\n<p class="f4 cl-white mv16">The purpose of&nbsp;<em>lorem ipsum</em>&nbsp;is to create a natural looking block of text (sentence, paragraph, page, etc.) that doesn''t distract from the layout. A practice not without&nbsp;<a title="Controversy in the Design World" href="https://loremipsum.io/#controversy">controversy</a>, laying out pages with meaningless filler text can be very useful when the focus is meant to be on design, not content.</p>\r\n<p class="f4 cl-white mv16">The passage experienced a surge in popularity during the 1960s when Letraset used it on their dry-transfer sheets, and again during the 90s as desktop publishers bundled the text with their software. Today it''s seen all around the web; on templates, websites, and stock designs. Use our&nbsp;<a title="Lorem Ipsum Generator" href="https://loremipsum.io/#generator">generator</a>&nbsp;to get your own, or read on for the authoritative history of&nbsp;<em>lorem ipsum</em>.</p>', '2019-11-08 09:01:31', 'hogwarts.jpg'),
(3, 'Star Trek Story', '3-star-trek-story', '<p class="f4 cl-white mt0 mb16"><em>Lorem ipsum</em>, or&nbsp;<em>lipsum</em>&nbsp;as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed to an unknown typesetter in the 15th century who is thought to have scrambled parts of Cicero''s&nbsp;<em>De Finibus Bonorum et Malorum</em>&nbsp;for use in a type specimen book. It usually begins with:</p>\n<blockquote class="page-section__blockquote">&ldquo;Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.&rdquo;</blockquote>\n<p class="f4 cl-white mv16">The purpose of&nbsp;<em>lorem ipsum</em>&nbsp;is to create a natural looking block of text (sentence, paragraph, page, etc.) that doesn''t distract from the layout. A practice not without&nbsp;<a title="Controversy in the Design World" href="https://loremipsum.io/#controversy">controversy</a>, laying out pages with meaningless filler text can be very useful when the focus is meant to be on design, not content.</p>\n<p class="f4 cl-white mv16">The passage experienced a surge in popularity during the 1960s when Letraset used it on their dry-transfer sheets, and again during the 90s as desktop publishers bundled the text with their software. Today it''s seen all around the web; on templates, websites, and stock designs. Use our&nbsp;<a title="Lorem Ipsum Generator" href="https://loremipsum.io/#generator">generator</a>&nbsp;to get your own, or read on for the authoritative history of&nbsp;<em>lorem ipsum</em>.</p>\n<p class="f4 cl-white mv16"><img src="../../../../assets/upload/image/29006-art_alphacoders_com9.jpg" alt="" width="371" height="232" /></p>', '2019-11-08 09:01:31', '2703601.jpg');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `berita`
--
ALTER TABLE `berita`
  ADD CONSTRAINT `idkategori` FOREIGN KEY (`idkategori`) REFERENCES `kategori` (`idkategori`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `halaman`
--
ALTER TABLE `halaman`
  ADD CONSTRAINT `iduser` FOREIGN KEY (`iduser`) REFERENCES `alumni` (`idalumni`) ON DELETE SET NULL ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

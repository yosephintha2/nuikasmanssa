<?php

/*
  @Copyright Indra Rukmana
  @Class Name : Home(Front)
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Career extends CI_Controller {

    // Main Page Home
    public function index() {

       
        
        $data['aktif'] = 'career';
        $data['data'] = '';
        $data['content'] = $this->load->view('front/career-opportunity', $data, true);
        $this->load->view('front/main_template', $data);
    }

}

<?php

/*
  @
  @Class Name : Home(Front)
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    // Main Page Home
    public function index() {



//        $data = array('title' => 'Home - ' . $site['nameweb'],
//            'site' => $site,
//            'galleries' => $galleries,
//            'blogs' => $blogs,
//            'products' => $products,
//            'clients' => $clients,
//            'isi' => 'front/home/list');
//        $this->load->view('front/layout/wrapper', $data);

        $data['new'] = $this->db->query('SELECT * FROM event ORDER BY tgl_event DESC LIMIT 1')->row();
        $data['berita'] = $this->db->select('a.*, b.*')
                        //->from('berita a')
                        ->join('kategori b', 'b.idkategori = a.idkategori')
                        ->where('statis', 0)
                        ->where('aktif', 1)
                        ->order_by('idberita', 'DESC')
                        ->limit('4')->get('berita a')->result();
        $data['wawancara'] = $this->db->order_by('idwawancara', 'DESC')->limit('4')->get('wawancara')->result();
        $data['event'] = $this->db->where('YEAR(tgl_event)', date('Y'))->where('MONTH(tgl_event)', date('m'))
                        ->order_by('tgl_event', 'ASC')->get('event')->result();
        $data['wawancara_random'] = $this->db->query('SELECT * FROM wawancara ORDER BY RAND() LIMIT 1')->row();

        $data['aktif'] = 'home';
        $data['data'] = '';
        $data['content'] = $this->load->view('front/homepage', $data, true);
        $this->load->view('front/main_template', $data);
    }

}

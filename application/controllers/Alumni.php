<?php

/*
  @Copyright Indra Rukmana
  @Class Name : Home(Front)
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Alumni extends CI_Controller {

    function __construct() {
        parent::__construct();
        //load libary pagination
        $this->load->library('pagination');
    }

    // Main Page Home
    public function index() {
        $random = $this->db->query('SELECT * FROM wawancara ORDER BY RAND() LIMIT 1')->row();

        //konfigurasi pagination
        $config['base_url'] = site_url('alumni/index'); //site url
        $config['total_rows'] = $this->db->count_all('wawancara'); //total row
        $config['per_page'] = 6;  //show record per halaman
        $config["uri_segment"] = 3;  // uri parameter
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = floor($choice);

        // Membuat Style pagination untuk BootStrap v4
        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Previous';
        $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
        $config['full_tag_close'] = '</ul></nav></div>';
        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="page-item current"><span class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] = '</span>Next</li>';
        $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] = '</span></li>';

        $this->pagination->initialize($config);
        $data['page'] = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

        //panggil function get_mahasiswa_list yang ada pada mmodel mahasiswa_model. 
        $data['data'] = $this->db->get('wawancara', $config["per_page"], $data['page'])->result();

        $data['pagination'] = $this->pagination->create_links();

        $data['random'] = $random;
        $data['aktif'] = 'alumni';
//        $data['data'] = $this->db->get('wawancara')->result();
        $data['content'] = $this->load->view('front/alumni-story', $data, true);
        $this->load->view('front/main_template', $data);
    }

    public function detail($slug) {
        $wawancara = $this->mWawancara->readWawancara($slug);

        if (empty($wawancara))
            redirect(site_url());

        $comments = $this->mWawancara->listKomentarByWawancara($slug);
        $id = $wawancara['idwawancara'];
        $count = $this->mWawancara->countKomentarByWawancara($id);

        $data = array(
            'data' => $wawancara,
            'count' => $count,
            'comments' => $comments);

        $data['aktif'] = 'alumni';
        $data['content'] = $this->load->view('front/alumni-story-detail', $data, true);
        $this->load->view('front/main_template', $data);
    }

    // Reply
    public function reply() {

        if ($this->input->post('message')) {
            $this->mWawancara->replyWawancara();
        }

        redirect('alumni/detail/' . $this->input->post('slug'));
    }

}

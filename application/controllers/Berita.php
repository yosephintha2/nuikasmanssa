<?php

/*
  @Copyright Indra Rukmana
  @Class Name : Home(Front)
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Berita extends CI_Controller {

    function __construct() {
        parent::__construct();
        //load libary pagination
        $this->load->library('pagination');
    }

    // Main Page Home
    public function index() {

        //konfigurasi pagination
        $config['base_url'] = site_url('berita/index'); //site url
        $config['total_rows'] = $this->db->select('a.*, b.*')
                //->from('berita a')
                ->join('kategori b', 'b.idkategori = a.idkategori')
                ->where('statis', 0)
                ->where('aktif', 1)
                ->count_all('berita a'); //total row
        $config['per_page'] = 5;  //show record per halaman
        $config["uri_segment"] = 3;  // uri parameter
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = floor($choice);

        // Membuat Style pagination untuk BootStrap v4
        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Previous';
        $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
        $config['full_tag_close'] = '</ul></nav></div>';
        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="page-item current"><span class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] = '</span>Next</li>';
        $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] = '</span></li>';

        $this->pagination->initialize($config);
        $data['page'] = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

        //panggil function get_mahasiswa_list yang ada pada mmodel mahasiswa_model. 
        $data['data'] = $this->db->select('a.*, b.*')
                        //->from('berita a')
                        ->join('kategori b', 'b.idkategori = a.idkategori')
                        ->where('statis', 0)
                        ->where('aktif', 1)
                        ->get('berita a', $config["per_page"], $data['page'])->result();
        

        $data['pagination'] = $this->pagination->create_links();

        $data['aktif'] = 'berita';
//        $data['data'] = $this->db->get('Berita')->result();
        $data['content'] = $this->load->view('front/berita', $data, true);
        $this->load->view('front/main_template', $data);
    }

    public function detail($slug) {
        $berita = $this->mBerita->readBerita($slug);

        if (empty($berita))
            redirect(site_url());

        $comments = $this->mBerita->listKomentarByBerita($slug);
        $id = $berita['idberita'];
        $count = $this->mBerita->countKomentarByBerita($id);

        $data = array(
            'data' => $berita,
            'count' => $count,
            'comments' => $comments);

        $data['aktif'] = 'berita';
        $data['content'] = $this->load->view('front/berita-single', $data, true);
        $this->load->view('front/main_template', $data);
    }

    // Reply
    public function reply() {

        if ($this->input->post('message')) {
            $this->mBerita->replyBerita();
        }

        redirect('berita/detail/' . $this->input->post('slug'));
    }

}

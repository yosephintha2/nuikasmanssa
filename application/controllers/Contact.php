<?php

/*
  @Copyright Indra Rukmana
  @Class Name : Home(Front)
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends CI_Controller {

    // Main Page Home
    public function index() {

        $data['aktif'] = 'contact';
        $data['data'] = $this->db->where('idberita',4)->get('berita')->row();
        $data['content'] = $this->load->view('front/contact', $data, true);
        $this->load->view('front/main_template', $data);
    }

}

<?php

/*
  @Copyright Indra Rukmana
  @Class Name : Home(Front)
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Event extends CI_Controller {

    // Main Page Home
    public function index() {

        $data['new'] = $this->db->query('SELECT * FROM event ORDER BY tgl_event DESC LIMIT 1')->row();

            //konfigurasi pagination
        $config['base_url'] = site_url('event/index'); //site url
        $config['total_rows'] = $this->db->count_all('event'); //total row
        $config['per_page'] = 6;  //show record per halaman
        $config["uri_segment"] = 3;  // uri parameter
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = floor($choice);

        // Membuat Style pagination untuk BootStrap v4
        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Previous';
        $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
        $config['full_tag_close'] = '</ul></nav></div>';
        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="page-item current"><span class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] = '</span>Next</li>';
        $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] = '</span></li>';

        $this->pagination->initialize($config);
        $data['page'] = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

        //panggil function get_mahasiswa_list yang ada pada mmodel mahasiswa_model. 
        $data['data'] = $this->db->get('event', $config["per_page"], $data['page'])->result();

        $data['pagination'] = $this->pagination->create_links();
        
        $data['aktif'] = 'event';
//        $data['data'] = $this->mEvent->listEvent();
        $data['content'] = $this->load->view('front/programs-events', $data, true);
        $this->load->view('front/main_template', $data);
    }

    public function detail($slug) {
        $event = $this->mEvent->readEvent($slug);

        if (empty($event))
            redirect(site_url());


        $id = $event['idevent'];

        $data = array(
            'data' => $event);

        $data['aktif'] = 'event';
        $data['content'] = $this->load->view('front/event-single', $data, true);
        $this->load->view('front/main_template', $data);
    }

}

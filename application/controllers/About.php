<?php

/*
  @Copyright Indra Rukmana
  @Class Name : Home(Front)
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class About extends CI_Controller {

    // Main Page Home
    public function index() {

        $data['aktif'] = 'about';
        $data['data'] = $this->db->where('idberita',3)->get('berita')->row();
        $data['content'] = $this->load->view('front/about-us', $data, true);
        $this->load->view('front/main_template', $data);
    }

}

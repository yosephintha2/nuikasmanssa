<?php

/*
  @
  @Class Name : Home(Front)
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {

    // Main Page Home
    public function index() {
        $data = array('title' => 'Register Alumni');
        $this->load->view('front/register', $data);
    }

    // Create Alumni
    public function create() {


        $site = $this->mConfig->list_config();

        $v = $this->form_validation;
        $v->set_rules('nama_lengkap', 'Nama Lengkap', 'required');
        $v->set_rules('alamat', 'Alamat', 'required');
        $v->set_rules('idprofesi', 'Profesi', 'required');
        $v->set_rules('provinsi', 'Provinsi', '');
        $v->set_rules('idkota', 'Kabupaten/Kota', '');
        $v->set_rules('role', 'Role', '');
        $v->set_rules('telepon', 'Telepon', 'required');
        $v->set_rules('angkatan', 'Angkatan', 'required|exact_length[4]|numeric');
        $v->set_rules('username', 'Username', 'required|min_length[5]|is_unique[admins.username]', array(
            'is_unique' => 'This %s already exists.')
        );
        $v->set_rules('email', 'Email', 'required|is_unique[admins.email]', array(
            'is_unique' => 'This %s already exists.'
                )
        );
        $v->set_rules('password', 'Password', 'required|min_length[6]');
        $v->set_rules('passconf', 'Password Confirmation', 'required|matches[password]');

        if ($v->run()) {

            date_default_timezone_set("Asia/Jakarta");
            $i = $this->input;
            $data = array(
                'nama_lengkap' => $i->post('nama_lengkap'),
                'nama_panggilan' => $i->post('nama_panggilan'),
                'angkatan' => $i->post('angkatan'),
                'jurusan' => $i->post('jurusan'),
                'alamat' => $i->post('alamat'),
                'telepon' => $i->post('telepon'),
                'idprofesi' => $i->post('idprofesi'),
                'kodepos' => $i->post('kodepos'),
                'idkota' => $i->post('idkota'),
            );


            if (!empty($_FILES['image']['name'])) {
                $config['upload_path'] = './assets/upload/alumni/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg|GIF|JPG|PNG|JPEG';
                $config['max_size'] = '1000'; // KB			
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('image')) {

                    $data = array('title' => 'Create Alumni - ' . $site['nameweb'],
                        'site' => $site,
//                    'categories' => $categories,
                        'error' => $this->upload->display_errors(),
                        'isi' => 'admin/alumni/create');
                    $this->load->view('admin/layout/wrapper', $data);
                } else {
                    $upload_data = array('uploads' => $this->upload->data());

                    $data['foto'] = $upload_data['uploads']['file_name'];
                    // Image Editor
                    $config['image_library'] = 'gd2';
                    $config['source_image'] = './assets/upload/alumni/' . $upload_data['uploads']['file_name'];
                    $config['new_image'] = './assets/upload/alumni/thumbs/';
                    $config['create_thumb'] = TRUE;
                    $config['maintain_ratio'] = TRUE;
                    $config['width'] = 150; // Pixel
                    $config['height'] = 150; // Pixel
                    $config['thumb_marker'] = '';
                    $this->load->library('image_lib', $config);
                    $this->image_lib->resize();
                }
            }

            $id_alumni = $this->mAlumni->save('alumni', $data);

            if ($id_alumni > 0) {
                $data_user = array(
                    'username' => $i->post('username'),
                    'password' => sha1($i->post('password')),
                    'email' => $i->post('email'),
                    'id_alumni' => $id_alumni,
                    'role' => 3
                );
                $this->mAlumni->save('admins', $data_user);
                $this->session->set_flashdata('sukses', 'Register berhasil.. Tunggu verifikasi akun oleh admin agar bisa login..');
                redirect(site_url('admin/login'));
            } else {
                $this->session->set_flashdata('sukses', 'Register gagal');
                redirect(site_url('register'));
            }

//            $this->session->set_flashdata('sukses', 'Success');
//            redirect(site_url('admin/login'));
        }

        // Default page
        $data = array('title' => 'Register Alumni - ' . $site['nameweb'],
            'site' => $site,
            'isi' => '');
        $this->load->view('front/register', $data);
    }

    public function list_kota($id, $dipilih = '/0') {
        echo form_dropdown('idkota', $this->form_option->kota_opsi($id), set_value('idkota'), "id='kota' class='form-control'");
    }

}

<?php

/*
  @
  @Class Name : Event
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Event extends CI_Controller {

    public function __construct() {
        parent::__construct();

        if ($this->session->userdata('roles') != 1) {
            echo "<script>location.href = '" . site_url('admin/dashboard') . "';
		</script>";
        }
    }

    // Main Page Event
    public function index() {

        $site = $this->mConfig->list_config();
        $event = $this->mEvent->listEvent();

        $data = array('title' => 'List Event - ' . $site['nameweb'],
            'event' => $event,
            'isi' => 'admin/event/list');
        $this->load->view('admin/layout/wrapper', $data);
    }

    public function map($idevent) {

        $site = $this->mConfig->list_config();
        $event = $this->mEvent->detailEvent($idevent);

        $data = array('title' => 'Map Event - ' . $event['judul'],
            'event' => $event,
            'isi' => 'admin/event/map');

        $lat = $event['latitude'];
        $long = $event['longitude'];

        $this->load->library('googlemaps');
        $config['center'] = "$lat, $long";
        $config['zoom'] = '15';
        $config['apiKey'] = "AIzaSyDDkyiOeYrYiEWZoGEPCKSzcS7klheX6xY";
        $this->googlemaps->initialize($config);

        $marker = array();
        $marker['position'] = "$lat, $long";
        $marker['infowindow_content'] = '<b>' . $event['alamat'] . '</b>';
        $marker['animation'] = 'DROP';
        $this->googlemaps->add_marker($marker);

        $data['map'] = $this->googlemaps->create_map();

        $this->load->view('admin/layout/wrapper', $data);
    }

    /*
      Function Create
     */

    // Create Event
    public function create() {

        $site = $this->mConfig->list_config();
//        $categories = $this->mCategories->listCategories();
        $endEvent = $this->mEvent->endEvent();

        $v = $this->form_validation;
        $v->set_rules('title', 'Title', 'required');
        $v->set_rules('desc', 'Description', 'required');
        $v->set_rules('date', 'Date', 'required');
        $v->set_rules('alamat', 'Address', 'required');

        if ($v->run()) {
            chmod('assets/upload/event', 0777);
            date_default_timezone_set("Asia/Jakarta");
            $i = $this->input;
            $slug_event = url_title($this->input->post('title'), 'dash', TRUE);

            $data = array(
                'slug' => $slug_event,
                'judul' => $i->post('title'),
                'isi' => $i->post('desc'),
                'alamat' => $i->post('alamat'),
//                    'latitude' => $i->post('latitude'),
//                    'longitude' => $i->post('longitude'),
                'google_map' => $i->post('gmap'),
                'tgl_event' => $i->post('date'),
//                'foto' => $upload_data['uploads']['file_name']
            );

            if (!empty($_FILES['image']['name'])) {
                $config['upload_path'] = './assets/upload/event/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg|GIF|JPG|PNG|JPEG';
                $config['max_size'] = '1000'; // KB			
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('image')) {

                    $data = array('title' => 'Create Event - ' . $site['nameweb'],
                        'site' => $site,
//                    'categories' => $categories,
                        'error' => $this->upload->display_errors(),
                        'isi' => 'admin/event/create');
                    $this->load->view('admin/layout/wrapper', $data);
                } else {
                    $upload_data = array('uploads' => $this->upload->data());
                    // Image Editor
                    $config['image_library'] = 'gd2';
                    $config['source_image'] = './assets/upload/event/' . $upload_data['uploads']['file_name'];
                    $config['new_image'] = './assets/upload/event/thumbs/';
                    $config['create_thumb'] = TRUE;
                    $config['maintain_ratio'] = TRUE;
                    $config['width'] = 150; // Pixel
                    $config['height'] = 150; // Pixel
                    $config['thumb_marker'] = '';
                    $this->load->library('image_lib', $config);
                    $this->image_lib->resize();

                    $data['foto'] = $upload_data['uploads']['file_name'];
                }
            }

            $this->mEvent->createEvent($data);
            $this->session->set_flashdata('sukses', 'Success');
            redirect(site_url('admin/event'));
        }
        // Default page
        $data = array('title' => 'Create Event - ' . $site['nameweb'],
            'site' => $site,
//            'categories' => $categories,
            'isi' => 'admin/event/create');
        $this->load->view('admin/layout/wrapper', $data);
    }

    /*
      Function Edit
     */

    // Edit Event
    public function edit($idevent) {

        $event = $this->mEvent->detailEvent($idevent);
        $endEvent = $this->mEvent->endEvent();
//        $category = $this->mCategories->listCategories();

        // Validation
        $v = $this->form_validation;
        $v->set_rules('title', 'Title', 'required');
        $v->set_rules('desc', 'Description', 'required');
        $v->set_rules('date', 'Date', 'required');
        $v->set_rules('alamat', 'Address', 'required');

        if ($v->run()) {
            chmod('assets/upload/event', 0777);
            if (!empty($_FILES['image']['name'])) {
                $config['upload_path'] = './assets/upload/event/';
                $config['allowed_types'] = 'gif|jpg|png|svg';
                $config['max_size'] = '1000'; // KB			
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('image')) {

                    $data = array('title' => 'Edit Event - ' . $event['judul'],
//                        'category' => $category,
                        'event' => $event,
                        'error' => $this->upload->display_errors(),
                        'isi' => 'admin/event/edit');
                    $this->load->view('admin/layout/wrapper', $data);
                } else {
                    $upload_data = array('uploads' => $this->upload->data());
                    $config['image_library'] = 'gd2';
                    $config['source_image'] = './assets/upload/event/' . $upload_data['uploads']['file_name'];
                    $config['new_image'] = './assets/upload/event/thumb/';
                    $config['create_thumb'] = TRUE;
                    $config['quality'] = "100%";
                    $config['maintain_ratio'] = FALSE;
                    $config['width'] = 360; // Pixel
                    $config['height'] = 200; // Pixel
                    $config['x_axis'] = 0;
                    $config['y_axis'] = 0;
                    $config['thumb_marker'] = '';
                    $this->load->library('image_lib', $config);
                    $this->image_lib->resize();

                    $i = $this->input;

                    unlink('./assets/upload/event/' . $event['foto']);
                    unlink('./assets/upload/event/thumbs/' . $event['foto']);

                    $slugEvent = $endEvent['idevent'] . '-' . url_title($i->post('title'), 'dash', TRUE);
                    $data = array('idevent' => $event['idevent'],
                        'slug' => $slugEvent,
                        'judul' => $i->post('title'),
                        'isi' => $i->post('desc'),
                        'alamat' => $i->post('alamat'),
//                        'latitude' => $i->post('latitude'),
//                        'longitude' => $i->post('longitude'),
                        'google_map' => $i->post('gmap'),
                        'tgl_event' => $i->post('date'),
                        'foto' => $upload_data['uploads']['file_name']
                    );
                    $this->mEvent->editEvent($data);
//                    $this->edit_model->edit_biz($data);
                    $this->session->set_flashdata('sukses', 'Event telah diedit dan gambar telah diganti');
                    redirect(site_url('admin/event'));
                }
            } else {
                $i = $this->input;
                $slugEvent = $endEvent['idevent'] . '-' . url_title($i->post('title'), 'dash', TRUE);
                $data = array('idevent' => $event['idevent'],
                    'slug' => $slugEvent,
                    'judul' => $i->post('title'),
                    'isi' => $i->post('desc'),
                    'alamat' => $i->post('alamat'),
//                    'latitude' => $i->post('latitude'),
//                    'longitude' => $i->post('longitude'),
                    'google_map' => $i->post('gmap'),
                    'tgl_event' => $i->post('date'),
                );
                $this->mEvent->editEvent($data);
                $this->session->set_flashdata('sukses', 'Success');
                redirect(site_url('admin/event'));
            }
        }

        $data = array('title' => 'Edit Event - ' . $event['judul'],
//            'category' => $category,
            'event' => $event,
            'isi' => 'admin/event/edit');
        $this->load->view('admin/layout/wrapper', $data);
    }

    /*
      Function Delete
     */

    // Delete Event
    public function delete_event($idevent) {
        $data = array('idevent' => $idevent);
        $this->mEvent->deleteEvent($data);
        $this->session->set_flashdata('sukses', 'Success');
        redirect(site_url('admin/event'));
    }

}

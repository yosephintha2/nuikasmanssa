<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    // Main Page
    public function index() {
        //$site = $this->mConfig->list_config();
        $data = array('title' => 'Dashboard',
            'admins' => $this->mStats->admins(),
            'blogs' => $this->mStats->blogs(),
            'berita' => $this->mStats->berita(),
            'wawancara' => $this->mStats->wawancara(),
            'alumni' => $this->mStats->alumni(),
            'event' => $this->mStats->event(),
//            'clients' => $this->mStats->clients(),
            'galeri' => $this->mStats->galeri(),
//            'downloads' => $this->mStats->downloads(),
//            'contacts' => $this->mStats->contacts(),
            'isi' => 'admin/dashboard/list');
        $this->load->view('admin/layout/wrapper', $data);
    }

    // General Config
    public function config() {
        $site = $this->mConfig->list_config();

        // Validasi 
        $this->form_validation->set_rules('nameweb', 'Website Name', 'required');
        $this->form_validation->set_rules('email', 'Email', 'valid_email');
        $this->form_validation->set_rules('phone_number', 'Phone', 'required');
        $this->form_validation->set_rules('about', 'Abut', 'required');

        if ($this->form_validation->run() === FALSE) {

            $data = array('title' => 'General Settings - ' . $site['nameweb'],
                'site' => $site,
                'isi' => 'admin/dashboard/config');
            $this->load->view('admin/layout/wrapper', $data);
        } else {
            $i = $this->input;
            $data = array(''
                . 'config_id' => $i->post('config_id'),
                'nameweb' => $i->post('nameweb'),
                'email' => $i->post('email'),
                'address' => $i->post('address'),
                'about' => $i->post('about'),
                'city' => $i->post('city'),
                'zip_code' => $i->post('zip_code'),
                'phone_number' => $i->post('phone_number'),
                'facebook' => $i->post('facebook'),
                'twitter' => $i->post('twitter'),
                'instagram' => $i->post('instagram'),
            );
            $this->mConfig->edit_config($data);
            $this->session->set_flashdata('sukses', 'Konfigurasi telah diupdate');
            redirect(site_url('admin/dashboard/config'));
        }
    }

}

<?php

/*
  @
  @Class Name : Alumni
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Alumni extends CI_Controller {

    // Main Page Alumni
    public function index() {

        $site = $this->mConfig->list_config();
        //$alumni = $this->mAlumni->listAlumni();

        $data = array('title' => 'List Alumni - ' . $site['nameweb'],
            //'alumni' => $alumni,
            'isi' => 'admin/alumni/list');
        $this->load->view('admin/layout/wrapper', $data);
    }

    /*
      Function Create
     */

    // Create Alumni
    public function create() {

        if ($this->session->userdata('roles') == 3) {
            echo "<script>location.href = '" . site_url('admin/dashboard') . "';
		</script>";
        }

        $site = $this->mConfig->list_config();

        $v = $this->form_validation;
        $v->set_rules('nama_lengkap', 'Nama Lengkap', 'required');
        $v->set_rules('alamat', 'Alamat', 'required');
        $v->set_rules('idprofesi', 'Profesi', 'required');
        $v->set_rules('provinsi', 'Provinsi', '');
        $v->set_rules('idkota', 'Kabupaten/Kota', '');
        $v->set_rules('role', 'Role', '');
        $v->set_rules('telepon', 'Telepon', 'required');
        $v->set_rules('angkatan', 'Angkatan', 'required|exact_length[4]|numeric');
        $v->set_rules('username', 'Username', 'required|min_length[5]|is_unique[admins.username]', array(
            'is_unique' => 'This %s already exists.')
        );
        $v->set_rules('email', 'Email', 'required|is_unique[admins.email]', array(
            'is_unique' => 'This %s already exists.'
                )
        );
        $v->set_rules('password', 'Password', 'required|min_length[6]');
        $v->set_rules('passconf', 'Password Confirmation', 'required|matches[password]');

        if ($v->run()) {
            chmod('assets/upload/alumni', 0777);
            date_default_timezone_set("Asia/Jakarta");
            $i = $this->input;
            $data = array(
                'nama_lengkap' => $i->post('nama_lengkap'),
                'nama_panggilan' => $i->post('nama_panggilan'),
                'angkatan' => $i->post('angkatan'),
                'jurusan' => $i->post('jurusan'),
                'alamat' => $i->post('alamat'),
                'telepon' => $i->post('telepon'),
                'idprofesi' => $i->post('idprofesi'),
                'kodepos' => $i->post('kodepos'),
                'idkota' => $i->post('idkota'),
            );


            if (!empty($_FILES['image']['name'])) {
                $config['upload_path'] = './assets/upload/alumni/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg|GIF|JPG|PNG|JPEG';
                $config['max_size'] = '1000'; // KB			
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('image')) {

                    $data = array('title' => 'Create Alumni - ' . $site['nameweb'],
                        'site' => $site,
//                    'categories' => $categories,
                        'error' => $this->upload->display_errors(),
                        'isi' => 'admin/alumni/create');
                    $this->load->view('admin/layout/wrapper', $data);
                } else {
                    $upload_data = array('uploads' => $this->upload->data());

                    $data['foto'] = $upload_data['uploads']['file_name'];
                    // Image Editor
                    $config['image_library'] = 'gd2';
                    $config['source_image'] = './assets/upload/alumni/' . $upload_data['uploads']['file_name'];
                    $config['new_image'] = './assets/upload/alumni/thumbs/';
                    $config['create_thumb'] = TRUE;
                    $config['maintain_ratio'] = TRUE;
                    $config['width'] = 150; // Pixel
                    $config['height'] = 150; // Pixel
                    $config['thumb_marker'] = '';
                    $this->load->library('image_lib', $config);
                    $this->image_lib->resize();
                }
            }


            $id_alumni = $this->mAlumni->save('alumni', $data);

            if ($id_alumni > 0) {
                $data_user = array(
                    'username' => $i->post('username'),
                    'password' => sha1($i->post('password')),
                    'email' => $i->post('email'),
                    'id_alumni' => $id_alumni,
                    'role' => $i->post('role')
                );
                $this->mAlumni->save('admins', $data_user);
            }

            $this->session->set_flashdata('sukses', 'Success');
            redirect(site_url('admin/alumni/create'));
        }

        // Default page
        $data = array('title' => 'Create Alumni - ' . $site['nameweb'],
            'site' => $site,
            'isi' => 'admin/alumni/create');
        $this->load->view('admin/layout/wrapper', $data);
    }

    /*
      Function Edit
     */

    // Edit Alumni
    public function edit($idalumni) {

        if ($this->session->userdata('roles') == 3 AND $this->session->userdata('id_alumni') != $idalumni) {
            echo "<script>location.href = '" . site_url('admin/dashboard') . "';
		</script>";
        }

        $alumni = $this->mAlumni->detail($idalumni);
//        $endAlumni = $this->mAlumni->endAlumni();
        // Validation
        $v = $this->form_validation;
        $v->set_rules('nama_lengkap', 'Nama Lengkap', 'required');
        $v->set_rules('alamat', 'Alamat', 'required');
        $v->set_rules('idprofesi', 'Profesi', 'required');
        $v->set_rules('telepon', 'Telepon', 'required');
        $v->set_rules('angkatan', 'Angkatan', 'required|exact_length[4]|numeric');
        $v->set_rules('username', 'Username', 'required|min_length[5]|edit_unique[admins.username.id_alumni.' . $idalumni . ']', array(
            'edit_unique' => 'This %s already exists.')
        );
        $v->set_rules('email', 'Email', 'required|edit_unique[admins.email.id_alumni.' . $idalumni . ']', array(
            'edit_unique' => 'This %s already exists.'
                )
        );
        $v->set_rules('password', 'Password', 'min_length[6]');
        $v->set_rules('passconf', 'Password Confirmation', 'matches[password]');

        if ($v->run()) {
            chmod('assets/upload/alumni', 0777);
            date_default_timezone_set("Asia/Jakarta");
            $i = $this->input;
            $data = array(
                'nama_lengkap' => $i->post('nama_lengkap'),
                'nama_panggilan' => $i->post('nama_panggilan'),
                'angkatan' => $i->post('angkatan'),
                'jurusan' => $i->post('jurusan'),
                'alamat' => $i->post('alamat'),
                'telepon' => $i->post('telepon'),
                'idprofesi' => $i->post('idprofesi'),
                'kodepos' => $i->post('kodepos'),
                'idkota' => $i->post('idkota'),
            );

            if (!empty($_FILES['image']['name'])) {
                $config['upload_path'] = './assets/upload/alumni/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg|GIF|JPG|PNG|JPEG';
                $config['max_size'] = '1000'; // KB			
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('image')) {

                    $data = array('title' => 'Edit Alumni - ' . $alumni['nama_lengkap'],
                        'alumni' => $alumni,
                        'error' => $this->upload->display_errors(),
                        'isi' => 'admin/alumni/edit');
                    $this->load->view('admin/layout/wrapper', $data);
                } else {
                    $upload_data = array('uploads' => $this->upload->data());
                    $data['foto'] = $upload_data['uploads']['file_name'];
                    $config['image_library'] = 'gd2';
                    $config['source_image'] = './assets/upload/alumni/' . $upload_data['uploads']['file_name'];
                    $config['new_image'] = './assets/upload/alumni/thumb/';
                    $config['create_thumb'] = TRUE;
                    $config['quality'] = "100%";
                    $config['maintain_ratio'] = FALSE;
                    $config['width'] = 360; // Pixel
                    $config['height'] = 200; // Pixel
                    $config['x_axis'] = 0;
                    $config['y_axis'] = 0;
                    $config['thumb_marker'] = '';
                    $this->load->library('image_lib', $config);
                    $this->image_lib->resize();

                    unlink('./assets/upload/alumni/' . $alumni['foto']);
                    unlink('./assets/upload/alumni/thumbs/' . $alumni['foto']);
                }
            }

            $this->mAlumni->update('alumni', array('idalumni' => $idalumni), $data);
            if ($i->post('password') == true) {
                $data_user['password'] = sha1($i->post('password'));
            }

            $data_user = array(
                'username' => $i->post('username'),
//                'password' => sha1($i->post('password')),
                'email' => $i->post('email'),
//                'id_alumni' => $idalumni,
                'role' => $i->post('role')
            );
            $this->mAlumni->update('admins', array('id_alumni' => $idalumni), $data_user);

            $this->session->set_flashdata('sukses', 'Success');
            redirect(site_url('admin/alumni/edit/' . $idalumni));
        }

        $data = array('title' => 'Edit Alumni - ' . $alumni['nama_lengkap'],
            'alumni' => $alumni,
            'isi' => 'admin/alumni/edit');
        $this->load->view('admin/layout/wrapper', $data);
    }

    /*
      Function Delete
     */

    // Delete Alumni
    public function delete($idalumni) {

        if ($this->session->userdata('roles') == 3) {
            echo "<script>location.href = '" . site_url('admin/dashboard') . "';
		</script>";
        }

        //$data = array('idalumni' => $idalumni);
        $this->mAlumni->delete('alumni', 'idalumni', $idalumni);
        $this->mAlumni->delete('admins', 'id_alumni', $idalumni);
        $this->session->set_flashdata('sukses', 'Success');
        redirect(site_url('admin/alumni'));
    }

    public function list_kota($id, $dipilih = '/0') {
        echo form_dropdown('idkota', $this->form_option->kota_opsi($id), set_value('idkota'), "id='kota' class='form-control'");
    }

    public function ajax_list() {
        $this->load->helper('url');

        $list = $this->mAlumni->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $person) {
            $no++;
            $row = array();

            if ($person->foto)
                $row[] = '<center><a href="' . base_url('assets/upload/alumni/' . $person->foto) . '" '
                        . 'target="_blank"><img src="' . base_url('assets/upload/alumni/' . $person->foto) . '" '
                        . 'class="img-responsive" class="img-thumbnail" style="width:50px;height:35px"/></a></center>';
            else
                $row[] = '<center><img src="' . base_url('assets/images/no-image.png') . '" '
                        . 'class="img-responsive" class="img-thumbnail" style="width:50px;height:35px"/></center>';

            $row[] = $person->nama_lengkap;
            $row[] = $person->angkatan;
            $row[] = $person->alamat;
            $row[] = $person->nama_profesi;
            $verifikasi = '';
            if ($person->verifikasi) {
                $row[] = '<center><i class="fa fa-check" style="color:#5cb85c;font-size:24px;"></i></center>';
                $verifikasi = ' <a class="btn btn-danger" href="javascript:void(0)" title="Batalkan Verifikasi" '
                        . 'onclick="verifikasi(' . "'" . $person->idalumni . "', '0'" . ')"><i class="fa fa-close"></i></a>';
            } else {
                $row[] = '<center><i class="fa fa-close" style="color:#d9534f;font-size:24px;"></i></center>';
                $verifikasi = ' <a class="btn btn-success" href="javascript:void(0)" title="Verifikasi" '
                        . 'onclick="verifikasi(' . "'" . $person->idalumni . "', '1'" . ')"><i class="fa fa-check"></i></a>';
            }


            if ($this->session->userdata('roles') == 1 OR $this->session->userdata('roles') == 2) {
                $row[] = '<center><button class="btn" title="Detail"'
                        . 'onclick="detail(' . "'" . $person->idalumni . "'" . ')"><i class="fa fa-eye"></i></button>'
                        . ' <a href="' . site_url('admin/alumni/edit/' . $person->idalumni) . '" title="Edit" '
                        . 'class="btn btn-primary"><i class="fa fa-pencil"></i></a>'
                        . ' <a href="' . site_url('admin/alumni/delete/' . $person->idalumni) . '" class="btn btn-warning" title="Hapus"'
                        . 'onClick="return confirm(' . "'" . 'Are you sure?' . "'" . ')"><i class="fa fa-trash"></i></a>' .
                        $verifikasi . '</center>';
            } else if ($this->session->userdata('roles') == 3 AND $this->session->userdata('id_alumni') == $person->idalumni) {
                $row[] = '<center><button class="btn" title="Detail"'
                        . 'onclick="detail(' . "'" . $person->idalumni . "'" . ')"><i class="fa fa-eye"></i></button>'
                        . ' <a href="' . site_url('admin/alumni/edit/' . $person->idalumni) . '" title="Edit" '
                        . 'class="btn btn-primary"><i class="fa fa-pencil"></i></a></center>';
            } else {
                $row[] = '<center><button class="btn" title="Detail"'
                        . 'onclick="detail(' . "'" . $person->idalumni . "'" . ')"><i class="fa fa-eye"></i></button></center>';
            }

            //add html for action
//            $row[] = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_person(' . "'" . $person->id . "'" . ')"><i class="glyphicon glyphicon-pencil"></i> Edit</a>
//				  <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_person(' . "'" . $person->id . "'" . ')"><i class="glyphicon glyphicon-trash"></i> Delete</a>';

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mAlumni->count_all(),
            "recordsFiltered" => $this->mAlumni->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_detail($id) {

        $data = $this->mAlumni->detail($id);

        echo json_encode($data);
    }

    public function ajax_verifikasi($id, $con) {
        $this->db->set('verifikasi', $con)->where('idalumni', $id)->update('alumni');
        echo json_encode(array("status" => TRUE));
    }

}

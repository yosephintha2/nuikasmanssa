<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

    public function __construct() {
        parent::__construct();

        if ($this->session->userdata('roles') != 1) {
            echo "<script>location.href = '" . site_url('admin/dashboard') . "';
		</script>";
        }
    }

    // Main Page Users
    public function index() {

        $user = $this->list_model->list_users();
        $site = $this->list_model->list_config();

        // Validasi
        $valid = $this->form_validation;
        $valid->set_rules('username', 'Username', 'required');
        $valid->set_rules('email', 'Email', 'required');
        $valid->set_rules('password', 'Password', 'required');

        if ($valid->run() === FALSE) {

            $data = array('title' => 'Manajemen Users - ' . $site['nameweb'],
                'user' => $user,
                'isi' => 'admin/user/list');
            $this->load->view('admin/layout/wrapper', $data);
        } else {

            $i = $this->input;
            if ($i->post('password') == true) {

                $slug = url_title($this->input->post('username'), 'dash', TRUE);
                $data = array('slug' => $slug,
                    'f_name' => $i->post('f_name'),
                    'l_name' => $i->post('l_name'),
                    'username' => $i->post('username'),
                    'email' => $i->post('email'),
                    'zip_code' => $i->post('zip_code'),
                    'birthdate_m' => $i->post('birthdate_m'),
                    'birthdate_d' => $i->post('birthdate_d'),
                    'birthdate_y' => $i->post('birthdate_y'),
                );
            }

            $slug = url_title($this->input->post('username'), 'dash', TRUE);
            $data = array('slug' => $slug,
                'f_name' => $i->post('f_name'),
                'l_name' => $i->post('l_name'),
                'username' => $i->post('username'),
                'password' => sha1($i->post('password')),
                'email' => $i->post('email'),
                'zip_code' => $i->post('zip_code'),
                'birthdate_m' => $i->post('birthdate_m'),
                'birthdate_d' => $i->post('birthdate_d'),
                'birthdate_y' => $i->post('birthdate_y'),
            );
            $this->create_model->create_users($data);
            $this->session->set_flashdata('sukses', 'User telah ditambah');
            redirect(site_url('admin/user'));
        }
    }

    // Is User Admins
    public function admin() {

        $admin = $this->mAdmins->listAdmins();
        $site = $this->mConfig->list_config();

        // Validasi
        $valid = $this->form_validation;
//        $valid->set_rules('username', 'Username', 'required');
        $valid->set_rules(
                'username', 'Username', 'required|min_length[5]|is_unique[admins.username]', array(
            'is_unique' => 'This %s already exists.'
                )
        );
        $valid->set_rules('email', 'Email', 'required|is_unique[admins.email]', array(
            'is_unique' => 'This %s already exists.'
                )
        );
        $valid->set_rules('password', 'Password', 'required|min_length[6]');
        $valid->set_rules('passconf', 'Password Confirmation', 'required|matches[password]');

        if ($valid->run() === FALSE) {

            $data = array('title' => 'Management Admins - ' . $site['nameweb'],
                'admin' => $admin,
                'isi' => 'admin/user/admin');
            $this->load->view('admin/layout/wrapper', $data);
        } else {

            $i = $this->input;

            $slug = url_title($this->input->post('username'), 'dash', TRUE);
            $data = array('slug_admin' => $slug,
                'username' => $i->post('username'),
                'password' => sha1($i->post('password')),
                'email' => $i->post('email'),
                'role' => 1
            );
            $this->mAdmins->createAdmin($data);
            $this->session->set_flashdata('sukses', 'Admin telah ditambah');
            redirect(site_url('admin/users/admin'));
        }
    }

    // Edit User Admin
    public function edit_admin($admin_id) {

        $admin = $this->mAdmins->detailAdmin($admin_id);
        $site = $this->mConfig->list_config();

        // Validasi
        $valid = $this->form_validation;
//        $valid->set_rules('username', 'Username', 'required|min_length[5]|edit_unique[admins.username.admin_id.'.$admin_id.']', array(
//            'is_unique' => 'This %s already exists.'
//                )
//        );
        $valid->set_rules('email', 'Email', 'required|edit_unique[admins.email.admin_id.' . $admin_id . ']');
        $valid->set_rules('password', 'Password', 'min_length[6]');
        $valid->set_rules('passconf', 'Password Confirmation', 'matches[password]');

        if ($valid->run() === FALSE) {

            $data = array('title' => 'Edit Admin - ' . $admin['username'],
                'admin' => $admin,
                'isi' => 'admin/user/edit_admin');
            $this->load->view('admin/layout/wrapper', $data);
        } else {

            $i = $this->input;
            if ($i->post('password') == false) {

                //$slug = url_title($this->input->post('username'), 'dash', TRUE);
                $data = array('admin_id' => $admin['admin_id'],
                    //'slug_admin'=> $slug,			
                    //'username'	=> $i->post('username'),
                    'email' => $i->post('email'),
                );
            } else {

                //$slug = url_title($this->input->post('username'), 'dash', TRUE);
                $data = array('admin_id' => $admin['admin_id'],
                    //'slug_admin'=> $slug,		
                    //'username'	=> $i->post('username'),
                    'password' => sha1($i->post('password')),
                    'email' => $i->post('email'),
                );
            }
            $this->mAdmins->editAdmin($data);
            $this->session->set_flashdata('sukses', 'Success');
            redirect(site_url('admin/users/admin'));
        }
    }

    // Delete Admin
    public function delete_admin($admin_id) {
        $data = array('admin_id' => $admin_id);
        $this->mAdmins->deleteAdmin($data);
        $this->session->set_flashdata('sukses', 'Success');
        redirect(site_url('admin/users/admin'));
    }

}

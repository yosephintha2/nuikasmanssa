<?php

/*
  @
  @Class Name : Wawancara
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Wawancara extends CI_Controller {

    public function __construct() {
        parent::__construct();

        if ($this->session->userdata('roles') != 1) {
            echo "<script>location.href = '" . site_url('admin/dashboard') . "';
		</script>";
        }
    }

    // Main Page Wawancara
    public function index() {

        $site = $this->mConfig->list_config();
        $wawancara = $this->mWawancara->listWawancara();

        $data = array('title' => 'List Wawancara - ' . $site['nameweb'],
            'wawancara' => $wawancara,
            'isi' => 'admin/wawancara/list');
        $this->load->view('admin/layout/wrapper', $data);
    }

    /*
      Function Create
     */

    // Create Wawancara
    public function create() {

        $site = $this->mConfig->list_config();
//        $categories = $this->mCategories->listCategories();
        $endWawancara = $this->mWawancara->endWawancara();

        $v = $this->form_validation;
        $v->set_rules('title', 'Title Wawancara', 'required');
        $v->set_rules('content', 'Content Wawancara', 'required');

        if ($v->run()) {
            chmod('assets/upload/wawancara', 0777);
            $config['upload_path'] = './assets/upload/wawancara/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg|GIF|JPG|PNG|JPEG';
            $config['max_size'] = '1000'; // KB			
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('image')) {

                $data = array('title' => 'Create Wawancara - ' . $site['nameweb'],
                    'site' => $site,
//                    'categories' => $categories,
                    'error' => $this->upload->display_errors(),
                    'isi' => 'admin/wawancara/create');
                $this->load->view('admin/layout/wrapper', $data);
            } else {
                $upload_data = array('uploads' => $this->upload->data());
                // Image Editor
                $config['image_library'] = 'gd2';
                $config['source_image'] = './assets/upload/wawancara/' . $upload_data['uploads']['file_name'];
                $config['new_image'] = './assets/upload/wawancara/thumbs/';
                $config['create_thumb'] = TRUE;
                $config['maintain_ratio'] = TRUE;
                $config['width'] = 150; // Pixel
                $config['height'] = 150; // Pixel
                $config['thumb_marker'] = '';
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();

                date_default_timezone_set("Asia/Jakarta");
                $i = $this->input;
                $slug_wawancara = url_title($this->input->post('title'), 'dash', TRUE);
                $data = array(
//                    'category_id' => $i->post('category_id'),
                    'slug' => $slug_wawancara,
//                    'user_id' => $this->session->userdata('id'),
                    'judul' => $i->post('title'),
                    'isi' => $i->post('content'),
                    'tgl_wawancara' => date('Y-m-d h:i:s'),
//                    'status' => $i->post('status'),
//                    'keywords' => $i->post('keywords'),
                    'foto' => $upload_data['uploads']['file_name']
                );

                $this->mWawancara->createWawancara($data);
                $this->session->set_flashdata('sukses', 'Success');
                redirect(site_url('admin/wawancara'));
            }
        }
        // Default page
        $data = array('title' => 'Create Wawancara - ' . $site['nameweb'],
            'site' => $site,
//            'categories' => $categories,
            'isi' => 'admin/wawancara/create');
        $this->load->view('admin/layout/wrapper', $data);
    }

    /*
      Function Edit
     */

    // Edit Wawancara
    public function edit($idwawancara) {

        $wawancara = $this->mWawancara->detailWawancara($idwawancara);
        $endWawancara = $this->mWawancara->endWawancara();
//        $category = $this->mCategories->listCategories();
        // Validation
        $v = $this->form_validation;
//        $v->set_rules('category_id', 'Category Wawancara', 'required');
        $v->set_rules('title', 'Title Wawancara', 'required');
        $v->set_rules('content', 'Content Wawancara', 'required');

        if ($v->run()) {
            chmod('assets/upload/wawancara', 0777);
            if (!empty($_FILES['image']['name'])) {
                $config['upload_path'] = './assets/upload/wawancara/';
                $config['allowed_types'] = 'gif|jpg|png|svg';
                $config['max_size'] = '1000'; // KB			
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('image')) {

                    $data = array('title' => 'Edit Wawancara - ' . $wawancara['judul'],
//                        'category' => $category,
                        'wawancara' => $wawancara,
                        'error' => $this->upload->display_errors(),
                        'isi' => 'admin/wawancara/edit');
                    $this->load->view('admin/layout/wrapper', $data);
                } else {
                    $upload_data = array('uploads' => $this->upload->data());
                    $config['image_library'] = 'gd2';
                    $config['source_image'] = './assets/upload/wawancara/' . $upload_data['uploads']['file_name'];
                    $config['new_image'] = './assets/upload/wawancara/thumb/';
                    $config['create_thumb'] = TRUE;
                    $config['quality'] = "100%";
                    $config['maintain_ratio'] = FALSE;
                    $config['width'] = 360; // Pixel
                    $config['height'] = 200; // Pixel
                    $config['x_axis'] = 0;
                    $config['y_axis'] = 0;
                    $config['thumb_marker'] = '';
                    $this->load->library('image_lib', $config);
                    $this->image_lib->resize();

                    $i = $this->input;

                    unlink('./assets/upload/wawancara/' . $wawancara['foto']);
                    unlink('./assets/upload/wawancara/thumbs/' . $wawancara['foto']);
//../../../assets/upload/image/
                    $slugWawancara = $endWawancara['idwawancara'] . '-' . url_title($i->post('title'), 'dash', TRUE);
                    $data = array('idwawancara' => $wawancara['idwawancara'],
                        'slug' => $slugWawancara,
//                        'user_id' => $this->session->userdata('id'),
                        'judul' => $i->post('title'),
                        'isi' => $i->post('content'),
//                        'date_post' => $i->post('date_post'),
//                        'status' => $i->post('status'),
//                        'keywords' => $i->post('keywords'),
                        'foto' => $upload_data['uploads']['file_name']
                    );
                    $this->mWawancara->editWawancara($data);
//                    $this->edit_model->edit_biz($data);
                    $this->session->set_flashdata('sukses', 'Wawancara telah diedit dan gambar telah diganti');
                    redirect(site_url('admin/wawancara'));
                }
            } else {
                $i = $this->input;
                $slugWawancara = $endWawancara['idwawancara'] . '-' . url_title($i->post('title'), 'dash', TRUE);
                $data = array('idwawancara' => $wawancara['idwawancara'],
                    'slug' => $slugWawancara,
//                    'user_id' => $this->session->userdata('id'),
                    'judul' => $i->post('title'),
                    'isi' => $i->post('content'),
//                    'date_post' => $i->post('date_post'),
//                    'status' => $i->post('status'),
//                    'keywords' => $i->post('keywords'),
                );
                $this->mWawancara->editWawancara($data);
                $this->session->set_flashdata('sukses', 'Success');
                redirect(site_url('admin/wawancara'));
            }
        }

        $data = array('title' => 'Edit Wawancara - ' . $wawancara['judul'],
//            'category' => $category,
            'wawancara' => $wawancara,
            'isi' => 'admin/wawancara/edit');
        $this->load->view('admin/layout/wrapper', $data);
    }

    /*
      Function Delete
     */

    // Delete Wawancara
    public function delete_wawancara($idwawancara) {
        $data = array('idwawancara' => $idwawancara);
        $this->mWawancara->deleteWawancara($data);
        $this->session->set_flashdata('sukses', 'Success');
        redirect(site_url('admin/wawancara'));
    }

}

<?php

/*
  @
  @Class Name : Berita
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Berita extends CI_Controller {

    public function __construct() {
        parent::__construct();

        if ($this->session->userdata('roles') != 1) {
            echo "<script>location.href = '" . site_url('admin/dashboard') . "';
		</script>";
        }
    }

    // Main Page Berita
    public function index() {

        $site = $this->mConfig->list_config();
        $berita = $this->mBerita->listBerita();

        $data = array('title' => 'List Blog - ' . $site['nameweb'],
            'berita' => $berita,
            'isi' => 'admin/berita/list');
        $this->load->view('admin/layout/wrapper', $data);
    }

    /*
      Function Create
     */

    // Main Page Kategori Berita
    public function kategori() {

        $site = $this->mConfig->list_config();
        $kategori = $this->mKategori->listKategori();

        // Validasi
        $valid = $this->form_validation;
        $valid->set_rules('kategori_name', 'Kategori Name', 'required');
//        $valid->set_rules('order_kategori', 'Order Kategori', 'required');

        if ($valid->run() === FALSE) {

            $data = array('title' => 'Management Kategori - ' . $site['nameweb'],
                'kategori' => $kategori,
                'isi' => 'admin/kategori/list');
            $this->load->view('admin/layout/wrapper', $data);
        } else {
            $i = $this->input;
            $slug = url_title($this->input->post('kategori_name'), 'dash', TRUE);
            $data = array(
//                'slug_kategori' => $slug,
//                'user_id' => $this->session->userdata('id'),
//                'kategori_name' => $i->post('kategori_name'),
//                'order_kategori' => $i->post('order_kategori'),
//                'kategori_description' => $i->post('kategori_description'),
                'nama' => $i->post('kategori_name'),
                'statis' => $i->post('statis'),
            );
            $this->mKategori->createKategori($data);
            $this->session->set_flashdata('sukses', 'Success');
            redirect(site_url('admin/berita/kategori'));
        }
    }

    // Create Berita
    public function create() {

        $site = $this->mConfig->list_config();
        $kategori = $this->mKategori->listKategori();
        $endBerita = $this->mBerita->endBerita();

        $v = $this->form_validation;
        $v->set_rules('idkategori', 'Kategory Blog', 'required');
//        $v->set_rules('title', 'Title Blog', 'required');
        $v->set_rules(
                'title', 'Title Blog', 'required|is_unique[berita.judul]', array(
            'is_unique' => 'This %s already exists.'
                )
        );
        $v->set_rules('content', 'Content Blog', 'required');

        if ($v->run()) {
            chmod('assets/upload/berita', 0777);
            date_default_timezone_set("Asia/Jakarta");
            $i = $this->input;
            $slug = url_title($this->input->post('title'), 'dash', TRUE);
            $data = array(
                'idkategori' => $i->post('idkategori'),
                'slug' => $slug,
//                    'user_id' => $this->session->userdata('id'),
                'judul' => $i->post('title'),
                'isi' => $i->post('content'),
                'tgl_post' => date('Y-m-d h:i:s'),
//                    'status' => $i->post('status'),
                'aktif' => 1,
//                    'keywords' => $i->post('keywords'),
//                'foto' => $upload_data['uploads']['file_name']
            );

            if (!empty($_FILES['image']['name'])) {
                $config['upload_path'] = './assets/upload/berita/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg|GIF|JPG|PNG|JPEG';
                $config['max_size'] = '1000'; // KB			
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('image')) {

                    $data = array('title' => 'Create Blog - ' . $site['nameweb'],
                        'site' => $site,
                        'kategori' => $kategori,
                        'error' => $this->upload->display_errors(),
                        'isi' => 'admin/berita/create');
                    $this->load->view('admin/layout/wrapper', $data);
                } else {
                    $upload_data = array('uploads' => $this->upload->data());
                    // Image Editor
                    $config['image_library'] = 'gd2';
                    $config['source_image'] = './assets/upload/berita/' . $upload_data['uploads']['file_name'];
                    $config['new_image'] = './assets/upload/berita/thumbs/';
                    $config['create_thumb'] = TRUE;
                    $config['maintain_ratio'] = TRUE;
                    $config['width'] = 150; // Pixel
                    $config['height'] = 150; // Pixel
                    $config['thumb_marker'] = '';
                    $this->load->library('image_lib', $config);
                    $this->image_lib->resize();

                    $data['foto'] = $upload_data['uploads']['file_name'];
                }
            }


            $this->mBerita->createBerita($data);
            $this->session->set_flashdata('sukses', 'Success');
            redirect(site_url('admin/berita/'));
        }
        // Default page
        $data = array('title' => 'Create Blog - ' . $site['nameweb'],
            'site' => $site,
            'kategori' => $kategori,
            'isi' => 'admin/berita/create');
        $this->load->view('admin/layout/wrapper', $data);
    }

    /*
      Function Edit
     */

    // Edit Kategori Berita
    public function edit_kategori($idkategori) {

        $site = $this->mConfig->list_config();
        $kategori = $this->mKategori->detailKategori($idkategori);
        $endKategori = $this->mKategori->endKategori();

        // Validation
        $valid = $this->form_validation;
        $valid->set_rules('kategori_name', 'Kategori Name', 'required');
//        $valid->set_rules('order_kategori', 'Order Kategori', 'required');

        if ($valid->run() === FALSE) {

            $data = array('title' => 'Edit Kategori - ' . $kategori['nama'],
                'kategori' => $kategori,
                'isi' => 'admin/kategori/edit');
            $this->load->view('admin/layout/wrapper', $data);
        } else {
            $i = $this->input;
//            $slug_kategori = $endKategori['idkategori'] . '-' . url_title($i->post('kategori_name'), 'dash', TRUE);
            $data = array(
                'idkategori' => $kategori['idkategori'],
//                'slug_kategori' => $slug_kategori,
//                'kategori_name' => $i->post('kategori_name'),
//                'order_kategori' => $i->post('order_kategori'),
//                'date_kategori' => $i->post('date_kategori'),
                'nama' => $i->post('kategori_name'),
                'statis' => $i->post('statis'),
            );
            $this->mKategori->editKategori($data);
            $this->session->set_flashdata('sukses', 'Success');
            redirect(site_url('admin/berita/kategori'));
        }
    }

    // Edit Berita
    public function edit($idberita) {

        $berita = $this->mBerita->detailBerita($idberita);
        $endBerita = $this->mBerita->endBerita();
        $kategori = $this->mKategori->listKategori();

        // Validation
        $v = $this->form_validation;
        $v->set_rules('idkategori', 'Kategori Blog', 'required');
//        $v->set_rules('title', 'Title Blog', 'required');
        $v->set_rules('title', 'Title Blog', 'required|edit_unique[berita.judul.idberita.' . $idberita . ']');
        $v->set_rules('content', 'Content Blog', 'required');

        if ($v->run()) {
            chmod('assets/upload/berita', 0777);
            if (!empty($_FILES['image']['name'])) {
                $config['upload_path'] = './assets/upload/berita/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg|GIF|JPG|PNG|JPEG';
                $config['max_size'] = '1000'; // KB			
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('image')) {

                    $data = array('title' => 'Edit Blog - ' . $berita['judul'],
                        'kategori' => $kategori,
                        'berita' => $berita,
                        'error' => $this->upload->display_errors(),
                        'isi' => 'admin/berita/edit');
                    $this->load->view('admin/layout/wrapper', $data);
                } else {
                    $upload_data = array('uploads' => $this->upload->data());
                    $config['image_library'] = 'gd2';
                    $config['source_image'] = './assets/upload/berita/' . $upload_data['uploads']['file_name'];
                    $config['new_image'] = './assets/upload/berita/thumbs/';
                    $config['create_thumb'] = TRUE;
                    $config['quality'] = "100%";
                    $config['maintain_ratio'] = FALSE;
                    $config['width'] = 360; // Pixel
                    $config['height'] = 200; // Pixel
                    $config['x_axis'] = 0;
                    $config['y_axis'] = 0;
                    $config['thumb_marker'] = '';
                    $this->load->library('image_lib', $config);
                    $this->image_lib->resize();

                    $i = $this->input;

                    unlink('./assets/upload/berita/' . $berita['foto']);
                    unlink('./assets/upload/berita/thumbs/' . $berita['foto']);

                    $slug = $endBerita['idberita'] . '-' . url_title($i->post('title'), 'dash', TRUE);
//                    $data = array('idberita' => $berita['idberita'],
//                        'slug' => $slugBerita,
//                        'user_id' => $this->session->userdata('id'),
//                        'title' => $i->post('title'),
//                        'content' => $i->post('content'),
//                        'date_post' => $i->post('date_post'),
//                        'status' => $i->post('status'),
//                        'keywords' => $i->post('keywords'),
//                        'image' => $upload_data['uploads']['file_name']
//                    );

                    $data = array('idberita' => $berita['idberita'],
                        'idkategori' => $i->post('idkategori'),
                        'slug' => $slug,
//                    'user_id' => $this->session->userdata('id'),
                        'judul' => $i->post('title'),
                        'isi' => $i->post('content'),
                        'tgl_post' => date('Y-m-d h:i:s'),
//                    'status' => $i->post('status'),
                        'aktif' => $i->post('status'),
//                    'keywords' => $i->post('keywords'),
                        'foto' => $upload_data['uploads']['file_name']
                    );

                    $this->mBerita->editBerita($data);
                    $this->session->set_flashdata('sukses', 'Biz telah diedit dan gambar telah diganti');
                    redirect(site_url('admin/berita'));
                }
            } else {
                $i = $this->input;
                $slug = $endBerita['idberita'] . '-' . url_title($i->post('title'), 'dash', TRUE);

//                $data = array('idberita' => $berita['idberita'],
//                    'slug' => $slugBerita,
//                    'user_id' => $this->session->userdata('id'),
//                    'title' => $i->post('title'),
//                    'content' => $i->post('content'),
//                    'date_post' => $i->post('date_post'),
//                    'status' => $i->post('status'),
//                    'keywords' => $i->post('keywords'),
//                );

                $data = array('idberita' => $berita['idberita'],
                    'idkategori' => $i->post('idkategori'),
                    'slug' => $slug,
//                    'user_id' => $this->session->userdata('id'),
                    'judul' => $i->post('title'),
                    'isi' => $i->post('content'),
                    'tgl_post' => date('Y-m-d h:i:s'),
//                    'status' => $i->post('status'),
                    'aktif' => $i->post('status'),
//                    'keywords' => $i->post('keywords'),
                );

                $this->mBerita->editBerita($data);
                $this->session->set_flashdata('sukses', 'Success');
                redirect(site_url('admin/berita'));
            }
        }

        $data = array('title' => 'Edit Blog - ' . $berita['judul'],
            'kategori' => $kategori,
            'berita' => $berita,
            'isi' => 'admin/berita/edit');
        $this->load->view('admin/layout/wrapper', $data);
    }

    /*
      Function Delete
     */

    // Delete Berita
    public function delete_berita($idberita) {
        $data = array('idberita' => $idberita);
        $this->mBerita->deleteBerita($data);
        $this->session->set_flashdata('sukses', 'Success');
        redirect(site_url('admin/berita'));
    }

    // Delete Kategori Berita
    public function delete_kategori($idkategori) {
        $data = array('idkategori' => $idkategori);
        $this->mKategori->deleteKategori($data);
        $this->session->set_flashdata('sukses', 'Success');
        redirect(site_url('admin/berita/kategori'));
    }

}

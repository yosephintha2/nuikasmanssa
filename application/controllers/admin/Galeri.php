<?php

/*
  @
  @Class Name : Galeri
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Galeri extends CI_Controller {

    public function __construct() {
        parent::__construct();

        if ($this->session->userdata('roles') != 1) {
            echo "<script>location.href = '" . site_url('admin/dashboard') . "';
		</script>";
        }
    }

    // Main Page Galeri
    public function index() {

        $site = $this->mConfig->list_config();
        $galeri = $this->mGaleri->listGaleri();

        $data = array('title' => 'List Galeri - ' . $site['nameweb'],
            'galeri' => $galeri,
            'isi' => 'admin/galeri/list');
        $this->load->view('admin/layout/wrapper', $data);
    }

    public function detail($idgaleri) {

        $site = $this->mConfig->list_config();
        $galeri = $this->mGaleri->detailGaleri($idgaleri);
        $galeri_detail = $this->db->where('idgaleri', $galeri['idgaleri'])->get('galeri_detail')->result();

        $data = array('title' => 'Detail Galeri - ' . $galeri['judul'],
            'galeri' => $galeri,
            'galeri_detail' => $galeri_detail,
            'isi' => 'admin/galeri/detail');
        $this->load->view('admin/layout/wrapper', $data);
    }

    // Create Galeri
    public function create() {

        $site = $this->mConfig->list_config();
        $endGaleri = $this->mGaleri->endGaleri();

        $v = $this->form_validation;
        $v->set_rules('title', 'Title', 'required');
        $v->set_rules('desc', 'Description', 'required');

        if ($v->run()) {
            chmod('assets/upload/galeri', 0777);
            date_default_timezone_set("Asia/Jakarta");
            $i = $this->input;
            $slug_galeri = url_title($this->input->post('title'), 'dash', TRUE);

            $data = array(
                'slug' => $slug_galeri,
                'judul' => $i->post('title'),
                'deskripsi' => $i->post('desc'),
                'tgl_galeri' => date('Y-m-d h:i:s'),
            );

            $id_galeri = $this->mGaleri->createGaleri($data);
            if ($id_galeri > 0) {
                $path = 'assets/upload/galeri/' . $id_galeri;
                if (!file_exists($path)) {
                    $create = mkdir($path, 0777, TRUE);
                }

                $files = $_FILES;
                $cpt = count($_FILES['image']['name']);

                if ($cpt == 0) {
                    $data = array('title' => 'Create Galeri - ' . $site['nameweb'],
                        'site' => $site,
                        'error' => 'Please select image to upload',
                        'isi' => 'admin/galeri/create');
                    $this->load->view('admin/layout/wrapper', $data);
                }

                for ($i = 0; $i < $cpt; $i++) {

                    $_FILES['image']['name'] = $files['image']['name'][$i];
                    $_FILES['image']['type'] = $files['image']['type'][$i];
                    $_FILES['image']['tmp_name'] = $files['image']['tmp_name'][$i];
                    $_FILES['image']['error'] = $files['image']['error'][$i];
                    $_FILES['image']['size'] = $files['image']['size'][$i];

                    $keterangan = $this->input->post('keterangan');

                    $detail_galeri = array(
                        'idgaleri' => $id_galeri,
                        'keterangan' => $keterangan[$i],
                        'tgl_post' => date('Y-m-d h:i:s'),
                    );

                    if (!empty($_FILES['image']['name'])) {
                        $config['upload_path'] = "./$path/";
                        $config['allowed_types'] = 'gif|jpg|png|jpeg|GIF|JPG|PNG|JPEG';
                        $config['max_size'] = '1000'; // KB			
                        $this->load->library('upload', $config);
                        if (!$this->upload->do_upload('image')) {
                            $data = array('title' => 'Create Galeri - ' . $site['nameweb'],
                                'site' => $site,
                                'error' => $this->upload->display_errors(),
                                'isi' => 'admin/galeri/create');
                            $this->load->view('admin/layout/wrapper', $data);
                        } else {
                            $upload_data = array('uploads' => $this->upload->data());
                            $detail_galeri['foto'] = $upload_data['uploads']['file_name'];

                            $this->mGaleri->createDetailGaleri($detail_galeri);
                        }
                    }
                }
                $this->session->set_flashdata('sukses', 'Success');
            } else {
                $this->session->set_flashdata('sukses', 'Failed');
            }

            redirect(site_url('admin/galeri'));
        }
        // Default page
        $data = array('title' => 'Create Galeri - ' . $site['nameweb'],
            'site' => $site,
//            'categories' => $categories,
            'isi' => 'admin/galeri/create');
        $this->load->view('admin/layout/wrapper', $data);
    }

    /*
      Function Edit
     */

    // Edit Galeri
    public function edit($idgaleri) {
        $site = $this->mConfig->list_config();
        $galeri = $this->mGaleri->detailGaleri($idgaleri);
        $galeri_detail = $this->db->where('idgaleri', $galeri['idgaleri'])->get('galeri_detail')->result();

        // Validation
        $v = $this->form_validation;
        $v->set_rules('title', 'Title', 'required');
        $v->set_rules('desc', 'Description', 'required');

        if ($v->run()) {
            chmod('assets/upload/galeri', 0777);
            date_default_timezone_set("Asia/Jakarta");
            $i = $this->input;
            $slug_galeri = url_title($this->input->post('title'), 'dash', TRUE);

            $data = array(
                'idgaleri' => $idgaleri,
                'slug' => $slug_galeri,
                'judul' => $i->post('title'),
                'deskripsi' => $i->post('desc'),
//                'tgl_galeri' => date('Y-m-d h:i:s'),
            );

            $this->mGaleri->editGaleri($data);

            $path = 'assets/upload/galeri/' . $idgaleri;
            if (!file_exists($path)) {
                $create = mkdir($path, 0777, TRUE);
            }

            $files = $_FILES;
            $cpt = count($_FILES['image']['name']);

            if ($cpt == 0) {
                $data = array('title' => 'Create Galeri - ' . $site['nameweb'],
                    'site' => $site,
                    'error' => 'Please select image to upload',
                    'isi' => 'admin/galeri/edit');
                $this->load->view('admin/layout/wrapper', $data);
            }

            for ($i = 0; $i < $cpt; $i++) {
                
                $_FILES['image']['name'] = $files['image']['name'][$i];
                $_FILES['image']['type'] = $files['image']['type'][$i];
                $_FILES['image']['tmp_name'] = $files['image']['tmp_name'][$i];
                $_FILES['image']['error'] = $files['image']['error'][$i];
                $_FILES['image']['size'] = $files['image']['size'][$i];

                $keterangan = $this->input->post('keterangan');

                $detail_galeri = array(
                    'idgaleri' => $idgaleri,
                    'keterangan' => $keterangan[$i],
                    'tgl_post' => date('Y-m-d h:i:s'),
                );

                if (!empty($_FILES['image']['name'])) {
                    $config['upload_path'] = "./$path/";
                    $config['allowed_types'] = 'gif|jpg|png|jpeg|GIF|JPG|PNG|JPEG';
                    $config['max_size'] = '1000'; // KB			
                    $this->load->library('upload', $config);
                    if (!$this->upload->do_upload('image')) {
                        $data = array('title' => 'Create Galeri - ' . $site['nameweb'],
                            'site' => $site,
                            'error' => $this->upload->display_errors(),
                            'isi' => 'admin/galeri/edit');
                        $this->load->view('admin/layout/wrapper', $data);
                    } else {
                        $upload_data = array('uploads' => $this->upload->data());
                        $detail_galeri['foto'] = $upload_data['uploads']['file_name'];

                        $this->mGaleri->createDetailGaleri($detail_galeri);
                    }
                }
            }

            $delete = $this->input->post('ck_lampiran');
            for ($i = 0; $i < count($delete); $i++) {
                $row = $this->db->where('idgaleri_detail', $delete[$i])->get('galeri_detail')->row();
                if (!empty($row)) {
                    if (file_exists("./$path/" . $row->foto))
                        unlink("./$path/" . $row->foto);
                }
                $this->db->where('idgaleri_detail', $delete[$i])->delete('galeri_detail');
            }

            $this->session->set_flashdata('sukses', 'Success');
            redirect(site_url('admin/galeri'));
        }

        $data = array('title' => 'Edit Galeri - ' . $galeri['judul'],
            'galeri' => $galeri,
            'galeri_detail' => $galeri_detail,
            'isi' => 'admin/galeri/edit');
        $this->load->view('admin/layout/wrapper', $data);
    }

    /*
      Function Delete
     */

    // Delete Galeri
    public function delete_galeri($idgaleri) {
        $row = $this->db->where('idgaleri', $idgaleri)->get('galeri_detail')->result();
        if (!empty($row)) {
            foreach ($row as $r) {
                $path = 'assets/upload/galeri/' . $idgaleri;
                if (file_exists("./$path/" . $r->foto))
                    unlink("./$path/" . $r->foto);
            }
        }

        $data = array('idgaleri' => $idgaleri);
        $this->mGaleri->deleteGaleri($data);

        $this->session->set_flashdata('sukses', 'Success');
        redirect(site_url('admin/galeri'));
    }

}

<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_login {

    // SET SUPER GLOBAL
    var $CI = NULL;

    public function __construct() {
        $this->CI = & get_instance();
    }

    // Login
    public function login($username, $password) {
        // Query untuk pencocokan data
        $query = $this->CI->db->get_where('admins', array(
            'username' => $username,
            'password' => sha1($password)
        ));

        $query2 = $this->CI->db->get_where('admins', array(
            'email' => $username,
            'password' => sha1($password)
        ));

        // Jika ada hasilnya
        if ($query->num_rows() == 1) {
            $row = $this->CI->db->query('SELECT * FROM admins WHERE username = "' . $username . '"');
            $user = $row->row();
            $this->set_data($user);
            redirect(site_url() . '/admin/dashboard');
        } else if ($query2->num_rows() == 1) {
            $row = $this->CI->db->query('SELECT * FROM admins WHERE email = "' . $username . '"');
            $user = $row->row();
            $this->set_data($user);
            redirect(site_url() . '/admin/dashboard');
        } else {
            $this->CI->session->set_flashdata('sukses', 'Oopss.. Username/password salah');
            redirect(site_url() . '/admin/login');
        }
        return false;
    }

    // Cek login
    public function cek_login() {
        if ($this->CI->session->userdata('username') == '') {
            $this->CI->session->set_flashdata('sukses', 'Oops...silakan login dulu');
            redirect(site_url('admin/login'));
        }
    }

    public function set_data($user) {
        $id = $user->admin_id;
        $name = $user->username;
        $email = $user->email;
        $role = $user->role;
        $id_alumni = $user->id_alumni;

        if ($user->role != 1 AND $this->CI->convertion->is_verify($user->id_alumni) == false) {
            $this->CI->session->set_flashdata('sukses', 'Oopss.. Akun belum diverifikasi oleh admin');
            redirect(site_url() . '/admin/login');
        }

        // $_SESSION['username'] = $username;
        $this->CI->session->set_userdata('username', $name);
        $this->CI->session->set_userdata('name', $name);
        $this->CI->session->set_userdata('id_login', uniqid(rand()));
        $this->CI->session->set_userdata('id', $id);
        $this->CI->session->set_userdata('id_alumni', $id_alumni);
        $this->CI->session->set_userdata('email', $email);
        $this->CI->session->set_userdata('roles', $role);
        $this->CI->session->set_userdata('angkatan', $this->CI->convertion->get_angkatan($id_alumni));
    }

    // Logout
    public function logout() {
        $this->CI->session->unset_userdata('username');
        $this->CI->session->unset_userdata('akses_level');
        $this->CI->session->unset_userdata('name');
        $this->CI->session->unset_userdata('id_login');
        $this->CI->session->unset_userdata('id');
        session_destroy();
        $this->CI->session->set_flashdata('sukses', 'Terimakasih, Anda berhasil logout');
        redirect(site_url() . '/admin/login');
    }

}

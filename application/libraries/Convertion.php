<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Convertion {

    public function get_angkatan($id) {
        $CI = & get_instance();

//        $query = $CI->db_model->get('unsur', 'unsur', array('id_unsur' => $option));
        $query = $CI->db->where('idalumni', $id)->get('alumni');
        $row = $query->row();
        $hasil = isset($row->angkatan) ? $row->angkatan : '';
        return $hasil;
    }

    public function is_verify($id) {
        $CI = & get_instance();

        $query = $CI->db->where('idalumni', $id)->get('alumni');
        $row = $query->row();
        $hasil = ($row->verifikasi == 1) ? true : false;
        return $hasil;
    }

    public function cover($id) {
        $CI = & get_instance();

        $query = $CI->db->query("SELECT * FROM galeri_detail WHERE idgaleri = $id ORDER BY tgl_post ASC LIMIT 1");
        $row = $query->row();
        $hasil = isset($row->foto) ? $row->foto : '';
        return $hasil;
    }

}

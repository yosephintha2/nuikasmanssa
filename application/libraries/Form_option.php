<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Form_option {

    public function role() {
        $CI = & get_instance();
        //$option[''] = 'Pilih Jenis Dimensi';
        $query = $CI->db->where('idroles >', 1)->order_by('idroles', 'DESC')->get('roles');
        foreach ($query->result() as $data) {
            $option[$data->idroles] = $data->nama;
        }

        return $option;
    }

    public function provinsi() {
        $CI = & get_instance();
        $option[''] = '-- Pilih Provinsi --';
        $query = $CI->db->order_by('nama', 'ASC')->get('provinsi');
        foreach ($query->result() as $data) {
            $option[$data->idprovinsi] = $data->nama;
        }

        return $option;
    }
    
    public function kota_opsi($id) {
        $CI = & get_instance();
        $option[''] = '-- Pilih Kabupaten/Kota --';
        $query = $CI->db->where('idprovinsi', $id)->order_by('kota', 'ASC')->get('kota');
        foreach ($query->result() as $data) {
            $option[$data->idkota] = $data->kota;
        }

        return $option;
    }
    
    public function kota() {
        $CI = & get_instance();
        $option[''] = '-- Pilih Kabupaten/Kota --';
//        $query = $CI->db->order_by('nama', 'ASC')->get('provinsi');
//        foreach ($query->result() as $data) {
//            $option[$data->idprovinsi] = $data->nama;
//        }

        return $option;
    }

    public function profesi() {
        $CI = & get_instance();
        $option[''] = '-- Pilih Profesi --';
        $query = $CI->db->order_by('nama_profesi', 'ASC')->get('profesi');
        foreach ($query->result() as $data) {
            $option[$data->idprofesi] = $data->nama_profesi;
        }

        return $option;
    }

}

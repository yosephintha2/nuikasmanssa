<?php

/*
  @
  @Class Name : Event Model
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Event_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }

    // Listing Event
    public function listEvent() {
        $this->db->select('*');
        $this->db->from('event');
        $this->db->order_by('idevent', 'DESC');
        $query = $this->db->get();
        return $query->result_array();
    }

    // Listing Event Publish
    public function listEventPub() {
        $this->db->select('*');
        $this->db->from('event');
        $this->db->where(array('status' => 'publish'));
        $this->db->join('admins', 'admins.admin_id = event.user_id', 'LEFT');
        $this->db->join('categories', 'categories.category_id = event.category_id', 'LEFT');
        $this->db->order_by('idevent', 'ASC');
        $query = $this->db->get();
        return $query->result_array();
    }

    // Last Event Publish
    public function listLastEventPub() {
        $this->db->select('*');
        $this->db->from('event');
        $this->db->where(array('status' => 'publish'));
        $this->db->join('admins', 'admins.admin_id = event.user_id', 'LEFT');
        $this->db->join('categories', 'categories.category_id = event.category_id', 'LEFT');
        $this->db->order_by('idevent', 'DESC');
        $query = $this->db->get();
        return $query->result_array();
    }

    // Create Event
    public function createEvent($data) {
        $this->db->insert('event', $data);
    }

    // Detail Event
    public function detailEvent($idevent) {
        $this->db->select('*');
        $this->db->from('event');
        $this->db->where('idevent', $idevent);
        $this->db->order_by('idevent', 'DESC');
        $query = $this->db->get();
        return $query->row_array();
    }

    // Read Event
    public function readEvent($slugEvent) {
        $this->db->select('*');
        $this->db->from('event');
        //$this->db->where(array('status' => 'publish'));            
        //$this->db->join('categories','categories.category_id = event.category_id','LEFT');
        $this->db->where('slug', $slugEvent);
        $query = $this->db->get();
        return $query->row_array();
    }

    // Edit Event
    public function editEvent($data) {
        $this->db->where('idevent', $data['idevent']);
        $this->db->update('event', $data);
    }

    // Delete Event
    public function deleteEvent($data) {
        $this->db->where('idevent', $data['idevent']);
        $this->db->delete('event', $data);
    }

    // End Event
    public function endEvent() {
        $this->db->select('*');
        $this->db->from('event');
        $this->db->order_by('idevent', 'DESC');
        $query = $this->db->get();
        return $query->row_array();
    }

    // Per Page Event
    public function perPageEvent($limit, $start) {
        $this->db->select('*');
        $this->db->from('event');
        $this->db->where(array('status' => 'publish'));
        $this->db->join('categories', 'categories.category_id = event.category_id', 'LEFT');
        $this->db->order_by('idevent', 'ASC');
        $this->db->limit($limit, $start);
        $query = $this->db->get();
        return $query->result_array();
    }

    // Total Event
    public function totalEvent() {
        $this->db->select('*');
        $this->db->from('event');
        $this->db->where(array('status' => 'publish'));
        $this->db->join('categories', 'categories.category_id = event.category_id', 'LEFT');
        $this->db->order_by('idevent', 'ASC');
        $query = $this->db->get();
        return $query->result_array();
    }

    // Reply Event
    public function replyEvent() {
        date_default_timezone_set("Asia/Jakarta");
        $data = array(
            'komentar' => strip_tags(substr($this->input->post('message'), 0, 255)),
            'tipe' => 1,
            'idkonten' => $this->input->post('id'),
            'nama' => $this->input->post('name'),
            'email' => $this->input->post('email'),
            'tgl_komentar' => date('Y-m-d h:i:s'),
        );

        $this->db->insert('komentar', $data);
    }

    // Comment by Event
    public function listKomentarByEvent($slugEvent) {
        $data = array();
        $this->db->select('*');
        $this->db->join('event', 'event.idevent = komentar.idkonten', 'LEFT');
        $this->db->where('event.slug', $slugEvent);
        $this->db->where('komentar.tipe', 1);
        $this->db->order_by('komentar.idkomentar', 'ASC');
        $Q = $this->db->get('komentar');
        if ($Q->num_rows() > 0) {
            foreach ($Q->result_array() as $row) {
                $data[] = $row;
            }
        }
        $Q->free_result();
        return $data;
    }

    // Count Comment By Event
    public function countKomentarByEvent($eventId) {
        $this->db->select('*');
        $this->db->join('event', 'event.idevent = komentar.idkonten', 'LEFT');
        $query = $this->db->get_where('komentar', array('komentar.idkonten' => $eventId,
            'komentar.tipe' => 1));
        return $query->num_rows();
    }

    // get Event by Category
    public function getAllEventByCategory($slugEvent) {
        $this->db->select(' event.idevent,
                                event.slug,
                                event.image,
                                event.title,
                                event.date_post,
                                event.content,
                                categories.category_id,
                                categories.category_name,
                                categories.slug_category,
                             ');
        $this->db->join('categories', 'categories.category_id = event.category_id', 'LEFT');
        $data = array();
        $this->db->where('categories.slug_category', $slugEvent);
        $this->db->order_by('event.idevent', 'ASC');
        $Q = $this->db->get('event');
        if ($Q->num_rows() > 0) {
            foreach ($Q->result_array() as $row) {
                $data[] = $row;
            }
        }
        $Q->free_result();
        return $data;
    }

    public function totalEventByCategory($slugEvent) {
        $this->db->select('*');
        $this->db->from('event');
        $this->db->where('event.category_id', $slugEvent);
        $this->db->order_by('idevent', 'DESC');
        $query = $this->db->get();
        return $query->num_rows();
    }

    // Pencarian Event
    public function cariEvent($perPage, $uri, $ringkasan) {
        $this->db->select('*');
        $this->db->from('event');
        $this->db->join('categories', 'categories.category_id = event.category_id', 'LEFT');
        if (!empty($ringkasan)) {
            $this->db->like('title', $ringkasan);
        }
        $this->db->order_by('idevent', 'asc');
        $getData = $this->db->get('', $perPage, $uri);

        if ($getData->num_rows() > 0)
            return $getData->result_array();
        else
            return null;
    }

    // Event Terkait
    public function eventTerkait($category_id) {
        $this->db->select('*');
        $this->db->join('categories', 'categories.category_id = event.category_id', 'LEFT');
        $data = array();
        $this->db->where('event.category_id', $category_id);
        $this->db->order_by('event.idevent', 'ASC');
        $Q = $this->db->get('event');
        if ($Q->num_rows() > 0) {
            foreach ($Q->result_array() as $row) {
                $data[] = $row;
            }
        }
        $Q->free_result();
        return $data;
    }

}

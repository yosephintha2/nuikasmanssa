<?php

/*
  @
  @Class Name : Galeri Model
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Galeri_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }

    // Listing Galeri
    public function listGaleri() {
        $this->db->select('*');
        $this->db->from('galeri');
        $this->db->order_by('idgaleri', 'DESC');
        $query = $this->db->get();
        return $query->result_array();
    }

    // Listing Galeri Publish
    public function listGaleriPub() {
        $this->db->select('*');
        $this->db->from('galeri');
        $this->db->where(array('status' => 'publish'));
        $this->db->join('admins', 'admins.admin_id = galeri.user_id', 'LEFT');
        $this->db->join('categories', 'categories.category_id = galeri.category_id', 'LEFT');
        $this->db->order_by('idgaleri', 'ASC');
        $query = $this->db->get();
        return $query->result_array();
    }

    // Last Galeri Publish
    public function listLastGaleriPub() {
        $this->db->select('*');
        $this->db->from('galeri');
        $this->db->where(array('status' => 'publish'));
        $this->db->join('admins', 'admins.admin_id = galeri.user_id', 'LEFT');
        $this->db->join('categories', 'categories.category_id = galeri.category_id', 'LEFT');
        $this->db->order_by('idgaleri', 'DESC');
        $query = $this->db->get();
        return $query->result_array();
    }

    // Create Galeri
    public function createGaleri($data) {
        $this->db->insert('galeri', $data);
        return $this->db->insert_id();
    }

    // Create Galeri
    public function createDetailGaleri($data) {
        $this->db->insert('galeri_detail', $data);
        return $this->db->insert_id();
    }

    // Detail Galeri
    public function detailGaleri($idgaleri) {
        $this->db->select('*');
        $this->db->from('galeri');
        $this->db->where('idgaleri', $idgaleri);
        $this->db->order_by('idgaleri', 'DESC');
        $query = $this->db->get();
        return $query->row_array();
    }

    // Read Galeri
    public function readGaleri($slugGaleri) {
        $this->db->select('*');
        $this->db->from('galeri');
        //$this->db->where(array('status' => 'publish'));            
        //$this->db->join('categories','categories.category_id = galeri.category_id','LEFT');
        $this->db->where('slug', $slugGaleri);
        $query = $this->db->get();
        return $query->row_array();
    }

    // Edit Galeri
    public function editGaleri($data) {
        $this->db->where('idgaleri', $data['idgaleri']);
        $this->db->update('galeri', $data);
    }

    // Delete Galeri
    public function deleteGaleri($data) {
        $this->db->where('idgaleri', $data['idgaleri']);
        $this->db->delete('galeri', $data);

        $this->db->where('idgaleri', $data['idgaleri']);
        $this->db->delete('galeri_detail');
    }

    // End Galeri
    public function endGaleri() {
        $this->db->select('*');
        $this->db->from('galeri');
        $this->db->order_by('idgaleri', 'DESC');
        $query = $this->db->get();
        return $query->row_array();
    }

    // Per Page Galeri
    public function perPageGaleri($limit, $start) {
        $this->db->select('*');
        $this->db->from('galeri');
        $this->db->where(array('status' => 'publish'));
        $this->db->join('categories', 'categories.category_id = galeri.category_id', 'LEFT');
        $this->db->order_by('idgaleri', 'ASC');
        $this->db->limit($limit, $start);
        $query = $this->db->get();
        return $query->result_array();
    }

    // Total Galeri
    public function totalGaleri() {
        $this->db->select('*');
        $this->db->from('galeri');
        $this->db->where(array('status' => 'publish'));
        $this->db->join('categories', 'categories.category_id = galeri.category_id', 'LEFT');
        $this->db->order_by('idgaleri', 'ASC');
        $query = $this->db->get();
        return $query->result_array();
    }

    // Reply Galeri
    public function replyGaleri() {
        date_default_timezone_set("Asia/Jakarta");
        $data = array(
            'komentar' => strip_tags(substr($this->input->post('message'), 0, 255)),
            'tipe' => 1,
            'idkonten' => $this->input->post('id'),
            'nama' => $this->input->post('name'),
            'email' => $this->input->post('email'),
            'tgl_komentar' => date('Y-m-d h:i:s'),
        );

        $this->db->insert('komentar', $data);
    }

    // Comment by Galeri
    public function listKomentarByGaleri($slugGaleri) {
        $data = array();
        $this->db->select('*');
        $this->db->join('galeri', 'galeri.idgaleri = komentar.idkonten', 'LEFT');
        $this->db->where('galeri.slug', $slugGaleri);
        $this->db->where('komentar.tipe', 1);
        $this->db->order_by('komentar.idkomentar', 'ASC');
        $Q = $this->db->get('komentar');
        if ($Q->num_rows() > 0) {
            foreach ($Q->result_array() as $row) {
                $data[] = $row;
            }
        }
        $Q->free_result();
        return $data;
    }

    // Count Comment By Galeri
    public function countKomentarByGaleri($galeriId) {
        $this->db->select('*');
        $this->db->join('galeri', 'galeri.idgaleri = komentar.idkonten', 'LEFT');
        $query = $this->db->get_where('komentar', array('komentar.idkonten' => $galeriId,
            'komentar.tipe' => 1));
        return $query->num_rows();
    }

    // get Galeri by Category
    public function getAllGaleriByCategory($slugGaleri) {
        $this->db->select(' galeri.idgaleri,
                                galeri.slug,
                                galeri.image,
                                galeri.title,
                                galeri.date_post,
                                galeri.content,
                                categories.category_id,
                                categories.category_name,
                                categories.slug_category,
                             ');
        $this->db->join('categories', 'categories.category_id = galeri.category_id', 'LEFT');
        $data = array();
        $this->db->where('categories.slug_category', $slugGaleri);
        $this->db->order_by('galeri.idgaleri', 'ASC');
        $Q = $this->db->get('galeri');
        if ($Q->num_rows() > 0) {
            foreach ($Q->result_array() as $row) {
                $data[] = $row;
            }
        }
        $Q->free_result();
        return $data;
    }

    public function totalGaleriByCategory($slugGaleri) {
        $this->db->select('*');
        $this->db->from('galeri');
        $this->db->where('galeri.category_id', $slugGaleri);
        $this->db->order_by('idgaleri', 'DESC');
        $query = $this->db->get();
        return $query->num_rows();
    }

    // Pencarian Galeri
    public function cariGaleri($perPage, $uri, $ringkasan) {
        $this->db->select('*');
        $this->db->from('galeri');
        $this->db->join('categories', 'categories.category_id = galeri.category_id', 'LEFT');
        if (!empty($ringkasan)) {
            $this->db->like('title', $ringkasan);
        }
        $this->db->order_by('idgaleri', 'asc');
        $getData = $this->db->get('', $perPage, $uri);

        if ($getData->num_rows() > 0)
            return $getData->result_array();
        else
            return null;
    }

    // Galeri Terkait
    public function galeriTerkait($category_id) {
        $this->db->select('*');
        $this->db->join('categories', 'categories.category_id = galeri.category_id', 'LEFT');
        $data = array();
        $this->db->where('galeri.category_id', $category_id);
        $this->db->order_by('galeri.idgaleri', 'ASC');
        $Q = $this->db->get('galeri');
        if ($Q->num_rows() > 0) {
            foreach ($Q->result_array() as $row) {
                $data[] = $row;
            }
        }
        $Q->free_result();
        return $data;
    }

}

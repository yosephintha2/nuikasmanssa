<?php

/*
  @
  @Class Name : Berita Model
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Berita_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }

    // Listing Berita
    public function listBerita() {
        $this->db->select('*');
        $this->db->from('berita');
        //$this->db->join('admins','admins.admin_id = berita.user_id','LEFT');            
        $this->db->join('kategori', 'kategori.idkategori = berita.idkategori', 'LEFT');
        $this->db->order_by('idberita', 'ASC');
        $query = $this->db->get();
        return $query->result_array();
    }

    // Listing Berita Publish
    public function listBeritaPub() {
        $this->db->select('*');
        $this->db->from('berita');
        //$this->db->where(array('status' => 'publish'));
        $this->db->where(array('aktif' => '1'));
        $//this->db->join('admins','admins.admin_id = berita.user_id','LEFT');            
        $this->db->join('kategori', 'kategori.idkategori = berita.idkategori', 'LEFT');
        $this->db->order_by('idberita', 'ASC');
        $query = $this->db->get();
        return $query->result_array();
    }

    // Last Berita Publish
    public function listLastBeritaPub() {
        $this->db->select('*');
        $this->db->from('berita');
//        $this->db->where(array('status' => 'publish'));
        $this->db->where(array('aktif' => '1'));
//        $this->db->join('admins', 'admins.admin_id = berita.user_id', 'LEFT');
        $this->db->join('kategori', 'kategori.idkategori = berita.idkategori', 'LEFT');
        $this->db->order_by('idberita', 'DESC');
        $query = $this->db->get();
        return $query->result_array();
    }

    // Create Berita
    public function createBerita($data) {
        $this->db->insert('berita', $data);
    }

    // Detail Berita
    public function detailBerita($idberita) {
        $this->db->select('*');
        $this->db->from('berita');
        $this->db->where('idberita', $idberita);
        $this->db->order_by('idberita', 'DESC');
        $query = $this->db->get();
        return $query->row_array();
    }

    // Read Berita
    public function readBerita($slugBerita) {
        $this->db->select('*');
        $this->db->from('berita');
//        $this->db->where(array('status' => 'publish'));
        $this->db->where(array('aktif' => '1'));
        $this->db->join('kategori', 'kategori.idkategori = berita.idkategori', 'LEFT');
        $this->db->where('slug', $slugBerita);
        $query = $this->db->get();
        return $query->row_array();
    }

    // Edit Berita
    public function editBerita($data) {
        $this->db->where('idberita', $data['idberita']);
        $this->db->update('berita', $data);
    }

    // Delete Berita
    public function deleteBerita($data) {
        $this->db->where('idberita', $data['idberita']);
        $this->db->delete('berita', $data);
    }

    // End Berita
    public function endBerita() {
        $this->db->select('*');
        $this->db->from('berita');
        $this->db->order_by('idberita', 'DESC');
        $query = $this->db->get();
        return $query->row_array();
    }

    // Per Page Berita
    public function perPageBerita($limit, $start) {
        $this->db->select('*');
        $this->db->from('berita');
//            $this->db->where(array('status' => 'publish'));            
        $this->db->where(array('aktif' => '1'));
        $this->db->join('kategori', 'kategori.idkategori = berita.idkategori', 'LEFT');
        $this->db->order_by('idberita', 'ASC');
        $this->db->limit($limit, $start);
        $query = $this->db->get();
        return $query->result_array();
    }

    // Total Berita
    public function totalBerita() {
        $this->db->select('*');
        $this->db->from('berita');
//        $this->db->where(array('status' => 'publish'));
        $this->db->where(array('aktif' => '1'));
        $this->db->join('kategori', 'kategori.idkategori = berita.idkategori', 'LEFT');
        $this->db->order_by('idberita', 'ASC');
        $query = $this->db->get();
        return $query->result_array();
    }

    // Reply Berita
    public function replyBerita() {
        date_default_timezone_set("Asia/Jakarta");
        $data = array(
            'komentar' => strip_tags(substr($this->input->post('message'), 0, 255)),
            'tipe' => 2,
            'idkonten' => $this->input->post('id'),
            'nama' => $this->input->post('name'),
            'email' => $this->input->post('email'),
            'tgl_komentar' => date('Y-m-d h:i:s'),
        );

        $this->db->insert('komentar', $data);
    }

    // Komentar by Berita
    public function listKomentarByBerita($slugBerita) {
        $this->db->select('*');
        $this->db->join('berita', 'berita.idberita = komentar.idkonten', 'LEFT');
        $data = array();
        $this->db->where('berita.slug', $slugBerita);
        $this->db->where('komentar.tipe', 2);
        $this->db->order_by('komentar.idkomentar', 'ASC');
        $Q = $this->db->get('komentar');
        if ($Q->num_rows() > 0) {
            foreach ($Q->result_array() as $row) {
                $data[] = $row;
            }
        }
        $Q->free_result();
        return $data;
    }

    // Count Komentar By Berita
    public function countKomentarByBerita($beritaId) {
        $this->db->select('*');
        $this->db->join('berita', 'berita.idberita = komentar.idkonten', 'LEFT');
        $query = $this->db->get_where('komentar', array('komentar.idkonten' => $beritaId, 'komentar.tipe' => 2));
        return $query->num_rows();
    }

    // get Berita by Category
    public function getAllBeritaByCategory($slugBerita) {
        $this->db->select(' berita.idberita,
                                berita.slug,
                                berita.image,
                                berita.title,
                                berita.date_post,
                                berita.content,
                                kategori.idkategori,
                                kategori.category_name,
                                kategori.slug_category,
                             ');
        $this->db->join('kategori', 'kategori.idkategori = berita.idkategori', 'LEFT');
        $data = array();
        $this->db->where('kategori.slug_category', $slugBerita);
        $this->db->order_by('berita.idberita', 'ASC');
        $Q = $this->db->get('berita');
        if ($Q->num_rows() > 0) {
            foreach ($Q->result_array() as $row) {
                $data[] = $row;
            }
        }
        $Q->free_result();
        return $data;
    }

    public function totalBeritaByCategory($slugBerita) {
        $this->db->select('*');
        $this->db->from('berita');
        $this->db->where('berita.idkategori', $slugBerita);
        $this->db->order_by('idberita', 'DESC');
        $query = $this->db->get();
        return $query->num_rows();
    }

    // Pencarian Berita
    public function cariBerita($perPage, $uri, $ringkasan) {
        $this->db->select('*');
        $this->db->from('berita');
        $this->db->join('kategori', 'kategori.idkategori = berita.idkategori', 'LEFT');
        if (!empty($ringkasan)) {
            $this->db->like('title', $ringkasan);
        }
        $this->db->order_by('idberita', 'asc');
        $getData = $this->db->get('', $perPage, $uri);

        if ($getData->num_rows() > 0)
            return $getData->result_array();
        else
            return null;
    }

    // Berita Terkait
    public function beritaTerkait($idkategori) {
        $this->db->select('*');
        $this->db->join('kategori', 'kategori.idkategori = berita.idkategori', 'LEFT');
        $data = array();
        $this->db->where('berita.idkategori', $idkategori);
        $this->db->order_by('berita.idberita', 'ASC');
        $Q = $this->db->get('berita');
        if ($Q->num_rows() > 0) {
            foreach ($Q->result_array() as $row) {
                $data[] = $row;
            }
        }
        $Q->free_result();
        return $data;
    }

}

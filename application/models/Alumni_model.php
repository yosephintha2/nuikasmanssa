<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Alumni_model extends CI_Model {

    var $table = 'alumni';
    var $column_order = array(null, 'nama_lengkap', 'angkatan', 'nama_profesi', 'alamat', null); //set column field database for datatable orderable
    var $column_search = array('nama_lengkap', 'angkatan', 'nama_profesi', 'alamat'); //set column field database for datatable searchable just firstname , lastname , address are searchable
    var $order = array('nama_lengkap' => 'asc'); // default order 

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    private function _get_datatables_query() {

//        $this->db->from($this->table);

        if ($this->session->userdata('roles') == 2 OR $this->session->userdata('roles') == 3) {
            $this->db->where('angkatan', $this->session->userdata('angkatan'));
        }
        $this->db->select('a.*, b.*')
                ->from('alumni a')
                ->join('profesi b', 'a.idprofesi = b.idprofesi', 'left');

        $i = 0;

        foreach ($this->column_search as $item) { // loop column 
            if ($_POST['search']['value']) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables() {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered() {
//        $this->_get_datatables_query();
        if ($this->session->userdata('roles') == 2 OR $this->session->userdata('roles') == 3) {
            $this->db->where('angkatan', $this->session->userdata('angkatan'));
        }
        $this->db->select('a.*, b.*')
                ->from('alumni a')
                ->join('profesi b', 'a.idprofesi = b.idprofesi', 'left');
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all() {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    public function detail($id_alumni) {
        $this->db->select('a.*, b.*, c.*, d.*, e.*,e.nama AS provinsi, f.nama AS roles')
                ->from('alumni a')
                ->join('admins b', 'a.idalumni = b.id_alumni', 'left')
                ->join('kota c', 'c.idkota = a.idkota', 'left')
                ->join('profesi d', 'a.idprofesi = d.idprofesi', 'left')
                ->join('provinsi e', 'c.idprovinsi = e.idprovinsi', 'left')
                ->join('roles f', 'f.idroles = b.role', 'left')
                ->where('a.idalumni', $id_alumni);

        $query = $this->db->get();

        return $query->row_array();
    }

    public function save($table, $data) {
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }

    public function update($table, $where, $data) {
        $this->db->update($table, $data, $where);
        return $this->db->affected_rows();
    }

    public function delete($table, $where, $id) {
        $this->db->where($where, $id);
        $this->db->delete($table);
    }

}

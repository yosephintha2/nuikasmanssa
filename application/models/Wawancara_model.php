<?php

/*
  @
  @Class Name : Wawancara Model
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Wawancara_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }

    // Listing Wawancara
    public function listWawancara() {
        $this->db->select('*');
        $this->db->from('wawancara');
//            $this->db->join('admins','admins.admin_id = wawancara.user_id','LEFT');            
//            $this->db->join('categories','categories.category_id = wawancara.category_id','LEFT');
        $this->db->order_by('idwawancara', 'DESC');
        $query = $this->db->get();
        return $query->result_array();
    }

    // Listing Wawancara Publish
    public function listWawancaraPub() {
        $this->db->select('*');
        $this->db->from('wawancara');
        $this->db->where(array('status' => 'publish'));
        $this->db->join('admins', 'admins.admin_id = wawancara.user_id', 'LEFT');
        $this->db->join('categories', 'categories.category_id = wawancara.category_id', 'LEFT');
        $this->db->order_by('idwawancara', 'ASC');
        $query = $this->db->get();
        return $query->result_array();
    }

    // Last Wawancara Publish
    public function listLastWawancaraPub() {
        $this->db->select('*');
        $this->db->from('wawancara');
        $this->db->where(array('status' => 'publish'));
        $this->db->join('admins', 'admins.admin_id = wawancara.user_id', 'LEFT');
        $this->db->join('categories', 'categories.category_id = wawancara.category_id', 'LEFT');
        $this->db->order_by('idwawancara', 'DESC');
        $query = $this->db->get();
        return $query->result_array();
    }

    // Create Wawancara
    public function createWawancara($data) {
        $this->db->insert('wawancara', $data);
    }

    // Detail Wawancara
    public function detailWawancara($idwawancara) {
        $this->db->select('*');
        $this->db->from('wawancara');
        $this->db->where('idwawancara', $idwawancara);
        $this->db->order_by('idwawancara', 'DESC');
        $query = $this->db->get();
        return $query->row_array();
    }

    // Read Wawancara
    public function readWawancara($slugWawancara) {
        $this->db->select('*');
        $this->db->from('wawancara');
        //$this->db->where(array('status' => 'publish'));            
        //$this->db->join('categories','categories.category_id = wawancara.category_id','LEFT');
        $this->db->where('slug', $slugWawancara);
        $query = $this->db->get();
        return $query->row_array();
    }

    // Edit Wawancara
    public function editWawancara($data) {
        $this->db->where('idwawancara', $data['idwawancara']);
        $this->db->update('wawancara', $data);
    }

    // Delete Wawancara
    public function deleteWawancara($data) {
        $this->db->where('idwawancara', $data['idwawancara']);
        $this->db->delete('wawancara', $data);
    }

    // End Wawancara
    public function endWawancara() {
        $this->db->select('*');
        $this->db->from('wawancara');
        $this->db->order_by('idwawancara', 'DESC');
        $query = $this->db->get();
        return $query->row_array();
    }

    // Per Page Wawancara
    public function perPageWawancara($limit, $start) {
        $this->db->select('*');
        $this->db->from('wawancara');
        $this->db->where(array('status' => 'publish'));
        $this->db->join('categories', 'categories.category_id = wawancara.category_id', 'LEFT');
        $this->db->order_by('idwawancara', 'ASC');
        $this->db->limit($limit, $start);
        $query = $this->db->get();
        return $query->result_array();
    }

    // Total Wawancara
    public function totalWawancara() {
        $this->db->select('*');
        $this->db->from('wawancara');
        $this->db->where(array('status' => 'publish'));
        $this->db->join('categories', 'categories.category_id = wawancara.category_id', 'LEFT');
        $this->db->order_by('idwawancara', 'ASC');
        $query = $this->db->get();
        return $query->result_array();
    }

    // Reply Wawancara
    public function replyWawancara() {
        date_default_timezone_set("Asia/Jakarta");
        $data = array(
            'komentar' => strip_tags(substr($this->input->post('message'), 0, 255)),
            'tipe' => 1,
            'idkonten' => $this->input->post('id'),
            'nama' => $this->input->post('name'),
            'email' => $this->input->post('email'),
            'tgl_komentar' => date('Y-m-d h:i:s'),
        );

        $this->db->insert('komentar', $data);
    }

    // Comment by Wawancara
    public function listKomentarByWawancara($slugWawancara) {
        $data = array();
        $this->db->select('*');
        $this->db->join('wawancara', 'wawancara.idwawancara = komentar.idkonten', 'LEFT');
        $this->db->where('wawancara.slug', $slugWawancara);
        $this->db->where('komentar.tipe', 1);
        $this->db->order_by('komentar.idkomentar', 'ASC');
        $Q = $this->db->get('komentar');
        if ($Q->num_rows() > 0) {
            foreach ($Q->result_array() as $row) {
                $data[] = $row;
            }
        }
        $Q->free_result();
        return $data;
    }

    // Count Comment By Wawancara
    public function countKomentarByWawancara($wawancaraId) {
        $this->db->select('*');
        $this->db->join('wawancara', 'wawancara.idwawancara = komentar.idkonten', 'LEFT');
        $query = $this->db->get_where('komentar', array('komentar.idkonten' => $wawancaraId,
            'komentar.tipe' => 1));
        return $query->num_rows();
    }

    // get Wawancara by Category
    public function getAllWawancaraByCategory($slugWawancara) {
        $this->db->select(' wawancara.idwawancara,
                                wawancara.slug,
                                wawancara.image,
                                wawancara.title,
                                wawancara.date_post,
                                wawancara.content,
                                categories.category_id,
                                categories.category_name,
                                categories.slug_category,
                             ');
        $this->db->join('categories', 'categories.category_id = wawancara.category_id', 'LEFT');
        $data = array();
        $this->db->where('categories.slug_category', $slugWawancara);
        $this->db->order_by('wawancara.idwawancara', 'ASC');
        $Q = $this->db->get('wawancara');
        if ($Q->num_rows() > 0) {
            foreach ($Q->result_array() as $row) {
                $data[] = $row;
            }
        }
        $Q->free_result();
        return $data;
    }

    public function totalWawancaraByCategory($slugWawancara) {
        $this->db->select('*');
        $this->db->from('wawancara');
        $this->db->where('wawancara.category_id', $slugWawancara);
        $this->db->order_by('idwawancara', 'DESC');
        $query = $this->db->get();
        return $query->num_rows();
    }

    // Pencarian Wawancara
    public function cariWawancara($perPage, $uri, $ringkasan) {
        $this->db->select('*');
        $this->db->from('wawancara');
        $this->db->join('categories', 'categories.category_id = wawancara.category_id', 'LEFT');
        if (!empty($ringkasan)) {
            $this->db->like('title', $ringkasan);
        }
        $this->db->order_by('idwawancara', 'asc');
        $getData = $this->db->get('', $perPage, $uri);

        if ($getData->num_rows() > 0)
            return $getData->result_array();
        else
            return null;
    }

    // Wawancara Terkait
    public function wawancaraTerkait($category_id) {
        $this->db->select('*');
        $this->db->join('categories', 'categories.category_id = wawancara.category_id', 'LEFT');
        $data = array();
        $this->db->where('wawancara.category_id', $category_id);
        $this->db->order_by('wawancara.idwawancara', 'ASC');
        $Q = $this->db->get('wawancara');
        if ($Q->num_rows() > 0) {
            foreach ($Q->result_array() as $row) {
                $data[] = $row;
            }
        }
        $Q->free_result();
        return $data;
    }

}

<?php
	/*
    @
    @Class Name : Kategori Model
	*/
    defined('BASEPATH') OR exit('No direct script access allowed');

    class Kategori_model extends CI_Model {

        public function __construct() {
            $this->load->database();
        }

        // Listing Kategori
        public function listKategori() {
            $this->db->select('*');
            $this->db->from('kategori');
            //$this->db->join('admins','admins.admin_id = kategori.user_id','LEFT');            
            $this->db->order_by('idkategori','DESC');
            $query = $this->db->get();
            return $query->result_array();
        }

        // Create Kategori
        public function createKategori($data) {
            $this->db->insert('kategori',$data);
        }

        // Detail Kategori
        public function detailKategori($idkategori) {
            $this->db->select('*');
            $this->db->from('kategori');
            $this->db->where('idkategori',$idkategori);
            $this->db->order_by('idkategori','DESC');
            $query = $this->db->get();
            return $query->row_array();
        }

        // Detail Kategori By Slug
        public function detailKategoriSlug($slugKategori) {
            $this->db->select('*');
            $this->db->from('kategori');
            //$this->db->where('slug_category',$slugKategori);
            $this->db->order_by('idkategori','DESC');
            $query = $this->db->get();
            return $query->row_array();
        }                  

        // Edit Kategori
        public function editKategori($data) {
            $this->db->where('idkategori',$data['idkategori']);
            $this->db->update('kategori',$data);
        }           

        // Delete Kategori
        public function deleteKategori($data) {
            $this->db->where('idkategori',$data['idkategori']);
            $this->db->delete('kategori',$data);
        } 

        // End Kategori
        public function endKategori() {
            $this->db->select('*');
            $this->db->from('kategori');
            $this->db->order_by('idkategori','DESC');
            $query = $this->db->get();
            return $query->row_array();
        }                    

    }

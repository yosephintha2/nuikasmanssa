<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Stats_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }

    // Stat Admins
    public function admins() {
        $query = $this->db->where('role', 1)->get('admins');
        return $query->num_rows();
    }

    // Stat Blogs
    public function blogs() {
        $query = $this->db->get('blogs');
        return $query->num_rows();
    }

    // Stat Berita
    public function berita() {
        $query = $this->db->get('berita');
        return $query->num_rows();
    }

    // Stat Wawancara
    public function wawancara() {
        $query = $this->db->get('wawancara');
        return $query->num_rows();
    }

    // Stat Galeri
    public function galeri() {
        $query = $this->db->get('galeri');
        return $query->num_rows();
    }

    // Stat Alumni
    public function alumni() {
        if ($this->session->userdata('roles') == 2 OR $this->session->userdata('roles') == 3) {
            $this->db->where('angkatan', $this->session->userdata('angkatan'));
        }

        $query = $this->db->get('alumni');
        return $query->num_rows();
    }

    // Stat Products
    public function event() {
        $query = $this->db->get('event');
        return $query->num_rows();
    }

    // Stat Clients
    public function clients() {
        $query = $this->db->get('clients');
        return $query->num_rows();
    }

    // Stat Downloads
    public function downloads() {
        $query = $this->db->get('downloads');
        return $query->num_rows();
    }

    // Stat Contacts
    public function contacts() {
        $query = $this->db->get('contacts');
        return $query->num_rows();
    }

    // Stat Galleries
    public function galleries() {
        $query = $this->db->get('galleries');
        return $query->num_rows();
    }

    // Stat Galleries Publish
    public function galleriesPublish() {
        $query = $this->db->get_where('galleries', array('status' => 'publish'));
        return $query->num_rows();
    }

}

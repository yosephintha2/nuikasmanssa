<!--Begin content wrapper-->
<div class="content-wrapper">
    <!--begin event-->
    <?php if (!empty($data->foto)) { ?>
        <div class="event">
            <div class="cover-img">
                <div class="area-img">
                    <img class="img-responsive" style="width:1536px;height:637px"
                         src="<?php echo base_url('assets') . '/upload/berita/' . $data->foto ?>" alt="">
                </div>
            </div>
        </div>
    <?php } else { ?>
        <div class="event" style="margin-top: 150px"></div>
    <?php } ?>

    <div class="about-us">
        <div class="about-us-content">
            <div class="container">
                <div class="content-wrapper">
                    <p class="text-light"><?php echo $data->isi ?></p>
                </div>
            </div>
        </div>
    </div>
</div>
<!--end event-->




<link type="text/css" rel="stylesheet" href="<?php echo base_url('assets') ?>/jssocials/jssocials.css" />
<link type="text/css" rel="stylesheet" href="<?php echo base_url('assets') ?>/jssocials/jssocials-theme-flat.css" />
<!--<link type="text/css" rel="stylesheet" href="https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials-theme-classic.css" />
<link type="text/css" rel="stylesheet" href="https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials-theme-minima.css" />
<link type="text/css" rel="stylesheet" href="https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials-theme-plain.css" />-->

<!--Begin content wrapper-->
<div class="content-wrapper">
    <div class="story-single">
        <div class="cover-img">
            <div class="area-img">
                <img class="img-responsive" style="width:1536px;height:637px"
                     src="<?php echo base_url('assets') ?>/upload/event/<?php echo $data['foto'] ?>" alt="">
            </div>
            <div class="area-title">
                <div class="container">
                    <!--<h2 class="heading-light">Pudjo Suseno</h2>-->
                    <h1 class="heading-regular" style="text-shadow: 0 0 5px #FFFFFF"><?php echo $data['judul'] ?></h1>
                </div>
            </div>
        </div>
        <div class="story-content">
            <div class="container">

                <div class="desc">
                    <p>
                        <?php echo str_replace('../../../../assets/upload/image/', '../../../assets/upload/image/', $data['isi']) ?>
                    </p></div>
                <div class="post-by">
                    <p class="text-gray">Posted By <span class="text-regular">Admin</span>, <?php echo date('l, d/m/Y h:i:s', strtotime($data['tgl_event'])); ?></p>
                </div>
            </div>
        </div>
        <div class="share">
            <div class="container">
                <div class="box-share">
                    <h4>SHARE THIS</h4>
                    <!--                    <ul>
                                            <li class="facebook"><a href="#"><span class="hidden">facebook</span></a></li>
                                            <li class="twitter"><a href="#"><span class="hidden">twitter</span></a></li>
                                            <li class="google"><a href="#"><span class="hidden">google</span></a></li>
                    
                                        </ul>-->
                </div>
            </div>
            <div class="container">
                <div class="box-share">
                    <div id="share"></div>
                </div>
            </div>
        </div>

        <div class="comments">
            <div class="container">
                <div class="box-comments">
                    <span class="note-comments text-regular"><?php echo $count ?> Comments Found</span>
                    <?php if ($count > 0) { ?>
                        <ul class="list-comments">
                            <?php foreach ($comments AS $c) { ?>
                                <li>
                                    <!--<span class="user-avatar"><img src="images/avatar-user1.png" alt=""></span>-->
                                    <div class="user-comments">
                                        <h4 class="heading-regular"><?php echo $c['nama'] ?></h4>
                                        <p><?php echo $c['komentar'] ?></p>
                                        <span class="reply"><?php echo $c['tgl_komentar'] . ' ' . $c['email'] ?></span>
                                        <!-- <a href="#" class="reply">reply</a>-->
                                    </div>
                                </li>
                            <?php } ?>
                        </ul>
                    <?php } ?>
                </div>
            </div>
        </div>
        <!--write comments-->
        <div class="write-comments">
            <div class="container">
                <div class="box-comments">
                    <div class="title-write">
                        <h4 class="heading-regular">Write Comment</h4>
                    </div>
                    <form method="post" action="<?php echo site_url('alumni/reply'); ?>">
                        <input type="hidden" name="id" value="<?php echo $data['idevent'] ?>">
                        <input type="hidden" name="slug" value="<?php echo $data['slug'] ?>">
                        <div class="input-box your-comment" style="border-bottom: 0px;padding-bottom: 0px">
                        </div>
                        <div class="input-box password" style="margin-right: 4%">
                            <input type="text" placeholder="Name" name="name" required="">
                        </div>
                        <div class="input-box email" style="margin-right: 0">
                            <input type="email" placeholder="Email Address" name="email" required="">
                        </div>
                        <div class="input-box your-comment">
                            <textarea placeholder="Your Comment" cols="1" rows="1" name="message" required=""></textarea>
                        </div>
                        <div class="buttons-set">
                            <!--                            <a href="#"  title="Log In" class="bnt bnt-theme text-regular text-uppercase">POST COMMENT</a>-->
                            <button type="submit" class="bnt bnt-theme text-regular text-uppercase"
                                    style="padding: 18px 60px;
                                    border: 0;
                                    font-size: 16px;
                                    color: #ffffff;
                                    display: block;
                                    float: right;">POST COMMENT</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

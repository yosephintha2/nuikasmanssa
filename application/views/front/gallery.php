<!--Begin content wrapper-->
<div class="content-wrapper">


    <!--begin Alumni Story-->
    <div class="alumni-story" style="margin-top: 150px">

        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-xs-12">
                    <div class="galery-title text-center" style="margin-bottom: 50px">
                        <h4 class="heading-regular">ALUMNI GALERI</h4>
                    </div>
                </div>
            </div>
            <div class="row">
                <!--6 cell-->
                <?php
                foreach ($data AS $row) {
                    $path = base_url('assets') . '/upload/galeri/' . $row->idgaleri . '/';
                    ?>
                    <div class="col-md-4 col-xs-12">
                        <div class="alumni-story-wrapper" style="border: 1px solid grey; padding: 10px;height: 370px">
                            <a href="<?php echo site_url('gallery/detail/' . $row->slug) ?>">
                            <div class="alumni-story-img">
                                <img class="img-responsive" 
                                     src="<?php echo $path . $this->convertion->cover($row->idgaleri) ?>" style="height:240px" alt="">
                            </div>
                            <div class="alumni-story-content">
                                <h4 class="heading-regular tex"><?php echo (strlen($row->judul) > 50 ) ? (substr($row->judul, 0, 50) . '...') : $row->judul ?></h4>
                                <!--<p class="text-light"><?php echo (strlen($row->deskripsi) > 100 ) ? (substr($row->deskripsi, 0, 100) . '...') : $row->isi ?></p>-->
                                <span class="dates text-light"><?php echo date('l, d/m/Y h:i:s', strtotime($row->tgl_galeri)); ?></span>
                            </div>
                            </a>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <div class="pagination-wrapper">
                <!--Tampilkan pagination-->
                <?php echo $pagination; ?>
            </div>

        </div>
    </div>
    <!--end Alumni Story-->

</div>
<!--End content wrapper-->
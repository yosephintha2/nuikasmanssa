<style>   
    .object-fit_fill { object-fit: fill }
    .object-fit_contain { object-fit: contain }
    .object-fit_cover { object-fit: cover }
    .object-fit_none { object-fit: none }
    .object-fit_scale-down { object-fit: scale-down }

    .img { height: 288px; background-color: black;}

    .img[class] { 
        width: 100%;
    }

</style>

<!--Begin content wrapper-->
<div class="content-wrapper">
    <!--begin alumni interview-->
    <div class="alumni-interview" 
         style="background: url(<?php echo base_url() . '/assets/upload/wawancara/' . $random->foto ?>);background-repeat: no-repeat;
         background-attachment: fixed;
         background-size: cover;width: 100%; height: 100%;">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-xs-12 pull-right">
                    <div class="interview-wrapper" style="margin-top: 100px">
                        <div class="interview-title animated lightSpeedIn">
                            <h4 class="heading-light text-capitalize">Alumni Interview</h4>
                            <h1 class="heading-light text-capitalize"><?php echo $random->judul ?></h1>
                        </div>
                        <div class="interview-desc text-left animated rollIn">
                            <p class="text-light"><?php echo (strlen($random->isi) > 300 ) ? (substr($random->isi, 0, 300) . '...') : $random->isi ?></p>
                        </div>
                        <div class="interview-see-story animated zoomInLeft">
                            <a class="see-story bnt text-uppercase" href="<?php echo site_url('alumni/detail/' . $random->slug) ?>">BACA CERITA <?php echo $random->judul ?></a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <!--end alumni interview-->

    <!--begin Alumni Story-->
    <div class="alumni-story">
        <div class="container">
            <div class="row">
                <!--6 cell-->
                <?php foreach ($data AS $row) { ?>
                    <div class="col-sm-6 col-xs-12">
                        <div class="alumni-story-wrapper">
                            <div class="alumni-story-img">
<!--                                <img class="img-responsive object-fit_contain img"" style="width:569px;height:331px"
                                     src="<?php echo base_url('assets') . '/upload/wawancara/' . $row->foto ?>" alt="">-->
                                 <img class="img-responsive object-fit_contain img" style="width:569px;height:331px"
                                     src="<?php echo base_url('assets') . '/upload/wawancara/' . $row->foto ?>" alt="">
                            </div>
                            <div class="alumni-story-content">
                                <h3 class="heading-regular"><a href="<?php echo site_url('alumni/detail/' . $row->slug) ?>"><?php echo $row->judul ?></a></h3>
                                <p class="text-light"><?php echo (strlen($row->isi) > 150 ) ? (substr($row->isi, 0, 150) . '...') : $row->isi ?></p>
                                <span class="dates text-light"><?php echo date('l, d/m/Y h:i:s', strtotime($row->tgl_wawancara)); ?></span>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <div class="pagination-wrapper">
                <!--Tampilkan pagination-->
                <?php echo $pagination; ?>
            </div>

        </div>
    </div>
    <!--end Alumni Story-->

</div>
<!--End content wrapper-->
<!--Begin content wrapper-->
<div class="content-wrapper">
    <!--begin about us-->
    <div class="about-us">
        <!--        <div class="about-us-title text-center">
                    <div class="container">
                        <h1 class="heading-bold text-uppercase">Tentang Kami</h1>
                    </div>
                </div>-->
        <div class="about-us-content" style="margin-top: 150px">
            <div class="container">
                <div class="content-wrapper">
                    <p class="text-light">
                        Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius.
                    </p>
                    <p class="text-light">

                        Investigationes demonstraverunt lectores. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius.
                    </p>
                </div>
            </div>
        </div>
    </div>
    <!--end about us-->

    <!--begin programs & services-->
    <div class="programs-services">
        <div class="container">
            <div class="row">
                <div class="services-img col-md-6 col-sm-12 col-xs-12">
                    <img class="img-responsive" src="<?php echo base_url('assets') ?>/images/services-img.jpg" alt="">
                </div>
                <div class="services-content col-md-6 col-sm-12 col-xs-12">
                    <h2 class="heading-regular">Programs &amp; Layanan</h2>
                    <div id="tab_services">
                        <!--Nav tabs-->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active">
                                <a class="text-light" href="#social" aria-controls="social" role="tab" data-toggle="tab">Sosial</a>
                            </li>
                            <li role="presentation">
                                <a class="text-light" href="#professional" aria-controls="professional" role="tab" data-toggle="tab">Professional</a>
                            </li>
                            <li role="presentation" >
                                <a class="text-light" href="#intelectual" aria-controls="intelectual" role="tab" data-toggle="tab">Intelektual</a>
                            </li>
                        </ul>
                        <!--Tab panes-->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active animated zoomIn" id="social">
                                <div class="tab-content-wrapper">
                                    <p class="text-light">
                                        Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan.
                                    </p>
                                    <ul class="list-item text-light">
                                        <li>Feugiat nulla facilisis at vero eros et accumsan et iusto.</li>
                                        <li>Luptatum zzril delenit augue duis dolore.</li>
                                        <li>Vulputate velit esse molestie consequat.</li>
                                        <li>Delenit augue duis dolore vulputate velit esse molestie consequat</li>
                                    </ul>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane animated zoomIn" id="professional">
                                <div class="tab-content-wrapper">
                                    <p class="text-light">
                                        Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan.
                                    </p>
                                    <ul class="list-item">
                                        <li>Feugiat nulla facilisis at vero eros et accumsan et iusto.</li>
                                        <li>Delenit augue duis dolore vulputate velit esse molestie consequat</li>
                                        <li>Luptatum zzril delenit augue duis dolore.</li>
                                        <li>Vulputate velit esse molestie consequat.</li>
                                    </ul>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane animated zoomIn" id="intelectual">
                                <div class="tab-content-wrapper">
                                    <p class="text-light">
                                        Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan.
                                    </p>
                                    <ul class="list-item">
                                        <li>Delenit augue duis dolore vulputate velit esse molestie consequat</li>
                                        <li>Vulputate velit esse molestie consequat.</li>
                                        <li>Luptatum zzril delenit augue duis dolore.</li>
                                        <li>Feugiat nulla facilisis at vero eros et accumsan et iusto.</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--end programs & services-->

    <!--begin our history-->
    <div class="our-history">
        <div class="container">
            <div class="title-page text-center">
                <h2 class="text-regular">Sejarah Kami</h2>
                <p class="text-light">Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan.</p>
            </div>
            <div class="history-content">
                <ul class="list-history text-center">
                    <li>
                        <span class="history-title text-light">Pembentukan Alumni</span>
                        <span class="history-dot"> <span></span></span>
                        <span class="history-year">1892</span>
                    </li>
                    <li>
                        <span class="history-title">Raker Pertama</span>
                        <span class="history-dot"> <span></span></span>
                        <span class="history-year">1894</span>
                    </li>
                    <li>
                        <span class="history-title">Pembentukan Badan Hukum</span>
                        <span class="history-dot"><span></span> </span>
                        <span class="history-year">1977</span>
                    </li>
                    <li>
                        <span class="history-title">Launching Website Alumni</span>
                        <span class="history-dot"><span></span> </span>
                        <span class="history-year">1999</span>
                    </li>
                    <li>
                        <span class="history-title">Alumni Dashboard <br> Launched</span>
                        <span class="history-dot"> <span></span></span>
                        <span class="history-year">2002</span>
                    </li>
                    <li>
                        <span class="history-title">Raker Kedua</span>
                        <span class="history-dot"> <span></span></span>
                        <span class="history-year">2006</span>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!--end our history-->

    <!--begin map-->
    <div id="contacts" class="map">
        <div class="container">
            <div class="title-page text-center">
                <h2 class="text-regular">Guyub, Gayeng, Migunani</h2>
                <p class="text-light"> uis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan.</p>
            </div>
            <div class="map-content">
                <img class="img-responsive" src="<?php echo base_url('assets') ?>/images/map-img.jpg" alt="">
            </div>
        </div>
    </div>
    <!--end map-->



</div>
<!--End content wrapper-->

<link type="text/css" rel="stylesheet" href="<?php echo base_url('assets') ?>/jssocials/jssocials.css" />
<link type="text/css" rel="stylesheet" href="<?php echo base_url('assets') ?>/jssocials/jssocials-theme-flat.css" />

<!--Begin content wrapper-->
<div class="content-wrapper">
    <div class="blog-content" style="margin-top: 110px">
        <div class="container">
            <div class="row">
                <!--Col Main-->
                <!--                <div class="col-main col-lg-8 col-md-7 col-xs-12">-->
                <div class="col-main col-lg-12 col-md-12 col-xs-12">
                    <div class="blog-post-content">
                        <!--Blog Post-->
                        <div class="blog-post">
                            <div class="area-img">
                                <img class="img-responsive" src="<?php echo base_url('assets/upload/berita') . '/' . $data['foto'] ?>" alt="">
                            </div>
                            <div class="area-content">
                                <h3 class="text-regular text-uppercase"><?php echo $data['judul'] ?></h3>
                                <div class="stats">
                                    <span class="clock">
                                        <span class="icon clock-icon"></span>
                                        <span class="text-center text-light"><?php echo date('l, d/m/Y h:i:s', strtotime($data['tgl_post'])); ?></span>
                                    </span>
                                    <span class="comment">
                                        <span class="icon comment-icon"></span>
                                        <span class="text-center text-light"><?php echo $count ?> Comments</span>
                                    </span>
                                    <span class="user">
                                        <span class="icon user-icon"></span>
                                        <span class="text-content text-light">IKASMANSSA Admin</span>
                                    </span>
                                </div>
                                <div class="desc">
                                    <p><?php echo str_replace('../../../../assets/upload/image/', '../../../assets/upload/image/', $data['isi']) ?></p>
                                </div>

                            </div>
                        </div>
                        <!--Share-->
                        <div class="share">
                            <div class="container">
                                <div class="box-share">
                                    <h4>SHARE THIS</h4>
                                    <!--                    <ul>
                                                            <li class="facebook"><a href="#"><span class="hidden">facebook</span></a></li>
                                                            <li class="twitter"><a href="#"><span class="hidden">twitter</span></a></li>
                                                            <li class="google"><a href="#"><span class="hidden">google</span></a></li>
                                    
                                                        </ul>-->
                                </div>
                            </div>
                            <div class="container">
                                <div class="box-share">
                                    <div id="share"></div>
                                </div>
                            </div>
                        </div>
                        <!--<div id="share"></div>-->
                        <div class="comments">
                            <div class="container">
                                <div class="box-comments">
                                    <span class="note-comments text-regular"><?php echo $count ?> Comments Found</span>
                                    <?php if ($count > 0) { ?>
                                        <ul class="list-comments">
                                            <?php foreach ($comments AS $c) { ?>
                                                <li>
                                                    <!--<span class="user-avatar"><img src="images/avatar-user1.png" alt=""></span>-->
                                                    <div class="user-comments">
                                                        <h4 class="heading-regular"><?php echo $c['nama'] ?></h4>
                                                        <p><?php echo $c['komentar'] ?></p>
                                                        <span class="reply"><?php echo $c['tgl_komentar'] . ' ' . $c['email'] ?></span>
                                                        <!-- <a href="#" class="reply">reply</a>-->
                                                    </div>
                                                </li>
                                            <?php } ?>
                                        </ul>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <!--write comments-->
                        <div class="write-comments">
                            <div class="container">
                                <div class="box-comments">
                                    <div class="title-write">
                                        <h4 class="heading-regular">Write Comment</h4>
                                    </div>
                                    <form method="post" action="<?php echo site_url('berita/reply'); ?>">
                                        <input type="hidden" name="id" value="<?php echo $data['idberita'] ?>">
                                        <input type="hidden" name="slug" value="<?php echo $data['slug'] ?>">
                                        <div class="input-box your-comment" style="border-bottom: 0px;padding-bottom: 0px">
                                        </div>
                                        <div class="input-box password" style="margin-right: 4%">
                                            <input type="text" placeholder="Name" name="name" required="">
                                        </div>
                                        <div class="input-box email" style="margin-right: 0">
                                            <input type="email" placeholder="Email Address" name="email" required="">
                                        </div>
                                        <div class="input-box your-comment">
                                            <textarea placeholder="Your Comment" cols="1" rows="1" name="message" required=""></textarea>
                                        </div>
                                        <div class="buttons-set">
                                            <!--                            <a href="#"  title="Log In" class="bnt bnt-theme text-regular text-uppercase">POST COMMENT</a>-->
                                            <button type="submit" class="bnt bnt-theme text-regular text-uppercase"
                                                    style="padding: 18px 60px;
                                                    border: 0;
                                                    font-size: 16px;
                                                    color: #ffffff;
                                                    display: block;
                                                    float: right;">POST COMMENT</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--Sidrbar Right-->
                <!--                <div class="sidebar blog-right col-lg-4 col-md-5 hidden-sm hidden-xs">
                                    <div class="block-sidebar">
                                        <div class="block-item popurlar-port">
                                            <div class="block-title text-center">
                                                <h5 class="text-regular text-uppercase">POPULAR POST</h5>
                                            </div>
                                            <div class="block-content">
                                                <ul>
                                                    <li>
                                                        <div class="area-img">
                                                            <img src="images/post-right-1.jpg" alt="">
                                                        </div>
                                                        <div class="area-content">
                                                            <h6>Project Teach shows youngsters what�s possible</h6>
                                                            <div class="stats">
                                                                <span class="clock">
                                                                    <span class="icon clock-icon"></span>
                                                                    <span class="text-center text-light">16 May 2019</span>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="area-img">
                                                            <img src="images/post-right-2.jpg" alt="">
                                                        </div>
                                                        <div class="area-content">
                                                            <h6>Claritas est etiam processus dynamicus, quisasma</h6>
                                                            <div class="stats">
                                                                <span class="clock">
                                                                    <span class="icon clock-icon"></span>
                                                                    <span class="text-center text-light">16 May 2019</span>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="area-img">
                                                            <img src="images/post-right-3.jpg" alt="">
                                                        </div>
                                                        <div class="area-content">
                                                            <h6>Typi non habent claritatem insitam est usus legentis in</h6>
                                                            <div class="stats">
                                                                <span class="clock">
                                                                    <span class="icon clock-icon"></span>
                                                                    <span class="text-center text-light">16 May 2019</span>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="area-img">
                                                            <img src="images/post-right-4.jpg" alt="">
                                                        </div>
                                                        <div class="area-content">
                                                            <h6>Dolore eu feugiat nulla facilisis at vero eros et accumsan et i</h6>
                                                            <div class="stats">
                                                                <span class="clock">
                                                                    <span class="icon clock-icon"></span>
                                                                    <span class="text-center text-light">16 May 2019</span>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="area-img">
                                                            <img src="images/post-right-5.jpg" alt="">
                                                        </div>
                                                        <div class="area-content">
                                                            <h6>Dignissim qui blandit praesent luptatum zzril delenit augue dui</h6>
                                                            <div class="stats">
                                                                <span class="clock">
                                                                    <span class="icon clock-icon"></span>
                                                                    <span class="text-center text-light">16 May 2019</span>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="block-item twitter">
                                            <div class="block-title text-center">
                                                <h5 class="text-regular text-uppercase">TWITTER</h5>
                                            </div>
                                            <div class="block-content">
                                                <div class="twitter-wrapper text-center">
                                                    <div class="twitter-icon color-theme">
                                                        <i class="fa fa-twitter" aria-hidden="true"></i>
                                                    </div>
                                                    <div class="twitter-content">
                                                        <div class="twitter-desc">
                                                            <p class="text-light text-center">�Dignissim qui blandit praesent luptatum zril delenit augue duis dolore te feugait nulla facilisi. Nam liber tempor cum  <a href="#" class="color-theme">@#ikasmanssa</a>�</p>
                                                            <div class="twitter-user">
                                                                <span class="name">@KathleenLittle</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="block-item tag">
                                            <div class="block-title text-center">
                                                <h5 class="text-regular text-uppercase">POPULAR TAGS</h5>
                                            </div>
                                            <div class="block-content">
                                                <ul class="list-inline">
                                                    <li><span>IKASMANSSA</span></li>
                                                    <li><span>community</span></li>
                                                    <li><span>news</span></li>
                                                    <li><span>alumni</span></li>
                                                    <li><span>profile</span></li>
                                                    <li><span>interview</span></li>
                                                    <li><span>Monashku</span></li>
                                                    <li><span>SMANSSA</span></li>
                                                    <li><span>psd</span></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>-->
            </div>
        </div>
    </div>
</div>
<!--End content wrapper-->
<!--Begin content wrapper-->
<div class="content-wrapper">

    <!--begin upcoming event-->
    <div class="program-upcoming-event" 
         style="background: url(<?php echo base_url() . '/assets/upload/event/' . $new->foto ?>);background-repeat: no-repeat;
         background-attachment: fixed;
         background-size: 100% 100%;">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="area-img">
                        <img class="img-responsive animate zoomIn" src="images/programs-events-img.jpg" alt="">
                        <div id="time-event" class="animated fadeIn"></div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="area-content">
                        <div class="area-top">
                            <div class="top-section animated lightSpeedIn">
                                <h5 class="heading-light">KEGIATAN AKAN DATANG</h5>
                                <span class="dates text-white text-uppercase"><?php echo date('F d, Y', strtotime($new->tgl_event)); ?></span>
                            </div>
                            <h2 class="heading-bold animated rollIn"><?php echo $new->judul ?></h2>
                            <span class="animated fadeIn">
                                <span class="icon map-icon"></span>
                                <span class="text-place text-white"><?php echo $new->alamat ?></span>
                            </span>
                        </div>
                        <div class="area-bottom animated zoomInLeft">
                            <a href="<?php echo site_url('event/detail/' . $new->slug) ?>" class="bnt bnt-theme join-now">Selengkapnya</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--end upcoming event-->

    <!--begin event calendar-->
    <div class="event-calendar">
        <div class="container">
            <div class="top-section text-center">
                <h4>All Alumni Events</h4>
            </div>
            <?php foreach ($data AS $row) { ?>
                <div class="event-list-content">
                    <div class="event-list-item">
                        <div class="date-item">
                            <span class="day text-bold color-theme"><?php echo date('d', strtotime($row->tgl_event)); ?></span>
                            <span class="dates text-gray text-uppercase"><?php echo date('D', strtotime($row->tgl_event)); ?></span>
                            <span class="dates text-gray text-uppercase"><?php echo date('m/Y', strtotime($row->tgl_event)); ?></span>
                        </div>
                        <div class="date-desc-wrapper">
                            <div class="date-desc">
                                <div class="date-title"><h4 class="heading-regular"><a href="<?php echo site_url('event/detail/' . $row->slug) ?>"><?php echo $row->judul ?></a></h4></div>
                                <div class="date-excerpt">
                                    <p><?php echo (strlen($row->isi) > 200 ) ? (substr($row->isi, 0, 200) . '...') : $row->isi ?></p>
                                </div>
                                <div class="place">
                                    <span class="icon map-icon"></span>
                                    <span class="text-place"><?php echo $row->alamat ?></span>
                                    <!--<a href="#"> View Map</a>-->
                                </div>
                            </div>
                        </div>
                        <!--                    <div class="date-links sold-out text-center">
                                                <a href="#" class="text-regular">SOLD OUT</a>
                                            </div>-->
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
    <div class="pagination-wrapper">
        <!--Tampilkan pagination-->
        <?php echo $pagination; ?>
    </div>
    <!--end event calendar-->



</div>
<!--End content wrapper-->
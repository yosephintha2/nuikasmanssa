<?php
// Load konfigurasi
$site = $this->mConfig->list_config();
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title><?php echo $title . ' - ' . $site['nameweb'] ?></title>
        <link href="<?php echo site_url('assets/upload/image/' . $site['icon']) ?>" rel="shortcut icon">
            <!-- BOOTSTRAP STYLES-->
            <link href="<?php echo base_url() ?>assets/admin/assets/css/bootstrap.css" rel="stylesheet" />
            <!-- FONTAWESOME STYLES-->
            <link href="<?php echo base_url() ?>assets/admin/assets/css/font-awesome.css" rel="stylesheet" />
            <!-- MORRIS CHART STYLES-->
            <!-- CUSTOM STYLES-->
            <link href="<?php echo base_url() ?>assets/admin/assets/css/custom.css" rel="stylesheet" />
            <!-- GOOGLE FONTS-->
            <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
            <!-- TABLE STYLES-->
            <link href="<?php echo base_url() ?>assets/admin/assets/js/dataTables/dataTables.bootstrap.css" rel="stylesheet" />            
            <script src="<?php echo base_url() ?>assets/admin/assets/js/jquery-1.10.2.js"></script>
    </head>

    <style>
        #imagePreview {
            margin-top: 7px;
            width: 100%;
            height: 370px;
            background-position: center center;
            background-size: cover;
            -webkit-box-shadow: 0 0 1px 1px rgba(0, 0, 0, .3);
            display: inline-block;
            background-image: url('<?php echo base_url('assets') . '/images/placeholder.png' ?>')
        }
    </style>
    <script type="text/javascript">
        $(function () {
            $("#provinsi").on('change', function () {
                var id = $("#provinsi").val();
                var html = '<?= site_url("register/list_kota") ?>/' + id + '/0';
                $("#kota").load(html);
            });

            $("#file").on("change", function () {
                var files = !!this.files ? this.files : [];
                if (!files.length || !window.FileReader)
                    return; // no file selected, or no FileReader support

                if (/^image/.test(files[0].type)) { // only image file
                    var reader = new FileReader(); // instance of the FileReader
                    reader.readAsDataURL(files[0]); // read the local file

                    reader.onloadend = function () { // set image data as background of div
                        $("#imagePreview").css("background-image", "url(" + this.result + ")");
                    };
                }
            });
        });

    </script>


    <?php
// Session 
    if ($this->session->flashdata('sukses')) {
        echo '<div class="alert alert-success">';
        echo $this->session->flashdata('sukses');
        echo '</div>';
    }

// File upload error
    if (isset($error)) {
        echo '<div class="alert alert-success">';
        echo $error;
        echo '</div>';
    }

// Error
    echo validation_errors('<div class="alert alert-success">', '</div>');
    ?>
    <body>
        <div class="container">
            <div class="row text-center ">
                <div class="col-md-12">
                    <h2><a href="<?php echo site_url('home') ?>"> 
                            <img src="<?php echo base_url('assets/upload/image/' . $site['logo']); ?>" width="150px"></a></h2>
                </div>
            </div>
            <div class="row">
                <!--                <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-10 col-xs-offset-1">-->
                <div class="col-sm-12">
                    <div class="panel panel-default">
                        <div class="panel-heading" align="center">
                            <strong>Alumni <?php echo $site['nameweb'] ?></strong>  
                        </div>
                        <div class="panel-body">

                            <?php
// Session 
                            if ($this->session->flashdata('sukses')) {
                                echo '<div class="alert alert-success">';
                                echo $this->session->flashdata('sukses');
                                echo '</div>';
                            }
// Error
                            echo validation_errors('<div class="alert alert-success">', '</div>');
                            ?>

                            <form action="<?php echo site_url('register/create') ?>" method="post" enctype="multipart/form-data">

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label style="color: red">*) Wajib diisi</label>
                                    </div>
                                    <div class="form-group">
                                        <label>Nama Lengkap <i style="color: red">*</i></label>
                                        <input type="text" name="nama_lengkap" class="form-control" 
                                               value="<?php echo set_value('nama_lengkap') ?>" required placeholder="Nama Lengkap">
                                    </div>
                                    <div class="form-group">
                                        <label>Nama Panggilan</label>
                                        <input type="text" name="nama_panggilan" class="form-control" 
                                               value="<?php echo set_value('nama_panggilan') ?>" placeholder="Nama Panggilan">
                                    </div>
                                    <?php if ($this->session->userdata('roles') == 2) { ?>
                                        <div class="form-group">
                                            <label>Angkatan <i style="color: red">*</i></label>
                                            <input type="number" name="angkatan" class="form-control" maxlength="4" min="1945"
                                                   value="<?php echo $this->session->userdata('angkatan') ?>" readonly="" required placeholder="Angkatan">
                                        </div>
                                    <?php } else { ?>
                                        <div class="form-group">
                                            <label>Angkatan <i style="color: red">*</i></label>
                                            <input type="number" name="angkatan" class="form-control" maxlength="4" min="1945"
                                                   value="<?php echo set_value('angkatan') ?>" required placeholder="Angkatan">
                                        </div>
                                    <?php } ?>
                                    <div class="form-group">
                                        <label>Jurusan</label>
                                        <input type="text" name="jurusan" class="form-control"
                                               value="<?php echo set_value('jurusan') ?>" placeholder="Jurusan">
                                    </div>
                                    <div class="form-group">
                                        <label>Profesi <i style="color: red">*</i></label>
                                        <?= form_dropdown('idprofesi', $this->form_option->profesi(), set_value('idprofesi'), "id='profesi' class='form-control'required")
                                        ?>
                                    </div>
                                    <div class="form-group">
                                        <label>Telepon <i style="color: red">*</i></label>
                                        <input type="text" name="telepon" class="form-control" 
                                               value="<?php echo set_value('telepon') ?>" required placeholder="Telepon">
                                    </div>
                                    <div class="form-group">
                                        <label>Email <i style="color: red">*</i></label>
                                        <input type="email" name="email" class="form-control" 
                                               value="<?php echo set_value('email') ?>" required placeholder="Email">
                                    </div>
                                    <div class="form-group">
                                        <label>Alamat <i style="color: red">*</i></label>
                                        <textarea name="alamat" placeholder="Alamat" class="form-control" 
                                                  rows="5" required><?php echo set_value('alamat') ?></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>Provinsi </label>
                                        <?= form_dropdown('provinsi', $this->form_option->provinsi(), set_value('provinsi'), "id='provinsi' class='form-control'")
                                        ?>
                                    </div>
                                    <div class="form-group">
                                        <label>Kabupaten/Kota </label>
                                        <?= form_dropdown('idkota', $this->form_option->kota(), set_value('idkota'), "id='kota' class='form-control'")
                                        ?>
                                    </div>
                                    <div class="form-group">
                                        <label>Kode Pos</label>
                                        <input type="number" name="kodepos" class="form-control" min="0"
                                               value="<?php echo set_value('kodepos') ?>" placeholder="Kode Pos">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label></label>
                                    </div>
                                    <div class="form-group">
                                        <label>Upload Image</label>
                                        <input type="file" name="image" class="form-control" id="file" accept="image/*">
                                            <div id="imagePreview"></div>
                                    </div>
                                    <div class="form-group">
                                        <label>Username <i style="color: red">*</i></label>
                                        <input type="text" name="username" class="form-control" 
                                               value="<?php echo set_value('username') ?>" required placeholder="Username">
                                    </div>
                                    <div class="form-group">
                                        <label>Password <i style="color: red">*</i></label>
                                        <input type="password" name="password" class="form-control" 
                                               value="<?php echo set_value('password') ?>" required placeholder="Password">
                                    </div>
                                    <div class="form-group">
                                        <label>Password Confirmation <i style="color: red">*</i></label>
                                        <input type="password" name="passconf" class="form-control" 
                                               value="<?php echo set_value('passconf') ?>" required placeholder="Password Confirmation">
                                    </div>
                                    <!--                                    <div class="form-group">
                                                                            <label>Role <i style="color: red">*</i></label>
                                    <?= form_dropdown('role', $this->form_option->role(), '', "id='role' class='form-control' required")
                                    ?>
                                                                        </div>-->
                                </div>
                                <div class="col-md-12">
                                    <!--                                    <div class="form-group">
                                                                            <br/><br/>
                                                                            <input type="submit" name="submit" value="Create" class="btn btn-primary"/>
                                                                            <input type="reset" name="reset" value="Reset" class="btn btn-default"/>
                                                                        </div>-->
                                    <div class="form-group">
                                        <!--<br/><br/>-->
                                        <input type="submit" name="submit" value="Register" class="btn btn-primary"/>
                                        <input type="reset" name="reset" value="Reset" class="btn btn-default"/>
                                        <label> Sudah punya akun? </label> <a href="<?php echo site_url('admin/login') ?>"><strong>Login</strong></a></center>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
        <!-- JQUERY SCRIPTS -->
        <script src="<?php echo base_url() ?>assets/admin/assets/js/jquery-1.10.2.js"></script>
        <!-- BOOTSTRAP SCRIPTS -->
        <script src="<?php echo base_url() ?>assets/admin/assets/js/bootstrap.min.js"></script>
        <!-- METISMENU SCRIPTS -->
        <script src="<?php echo base_url() ?>assets/admin/assets/js/jquery.metisMenu.js"></script>
        <!-- CUSTOM SCRIPTS -->
        <script src="<?php echo base_url() ?>assets/admin/assets/js/custom.js"></script>
    </body>
</html>




<!--Begin content wrapper-->
<div class="content-wrapper">
    <!--begin event-->
    <div class="event">
        <!--            <div class="event-img">
                        <div class="container">
                            <img class="img-responsive" style="width:1539px;height:573px"
                             src="<?php echo base_url('assets') ?>/upload/event/<?php echo $data['foto'] ?>" alt="">
                            <div class="event-time animated fadeIn">
                                <div id="time-event">
        
                                </div>
                            </div>
                        </div>
                    </div>-->
        <div class="cover-img">
            <div class="area-img">
                <img class="img-responsive" style="width:1536px;height:637px"
                     src="<?php echo base_url('assets') ?>/upload/event/<?php echo $data['foto'] ?>" alt="">
            </div>

        </div>
        <div class="event-content">
            <div class="container">
                <div class="event-detail text-center">
                    <div class="dates"> <p class="text-light"><?php echo date('l, d/m/Y', strtotime($data['tgl_event'])); ?></p></div>
                    <div class="event-detail-title">
                        <h1 class="heading-bold"><?php echo $data['judul'] ?></h1>
                    </div>
                    <div class="place">
                        <span>
                        <span class="icon icon-map"></span>
                        <span class="place-text text-light"><?php echo $data['alamat'] ?></span>
                        </span>
                    </div>
                    <div class="view-map text-center">
                        <!--                        <a href="#"> View Map</a>-->
                        <?php echo $data['google_map'] ?>
                    </div>
                </div>
                <div class="event-descriptiion">
                    <p class="text-light"><?php echo str_replace('../../../../assets/upload/image/', '../../../assets/upload/image/', $data['isi']) ?></p>
                </div>
                <!--                    <div class="join-now text-center">
                                        <a href="#" class="text-bold bnt bnt-theme">Join Now</a>
                                    </div>-->
            </div>
        </div>
    </div>
    <!--end event-->



</div>
<!--End content wrapper-->
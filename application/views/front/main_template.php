<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
        <link rel="icon" href="<?php echo base_url('assets') ?>/favicon.ico" type="image/ico" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets') ?>/css/bootstrap.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets') ?>/css/animate.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets') ?>/css/owl.carousel.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets') ?>/css/styles.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets') ?>/css/meanmenu.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets') ?>/css/font-awesome.min.css" />
        <link rel="stylesheet" href="https://cdn.linearicons.com/free/1.0.0/icon-font.min.css">
        <script src="<?php echo base_url('assets') ?>/js/libs/modernizr.custom.js"></script>
        <title>Selamat Datang di IKASMANSSA.ORG</title>
    </head>
    <?php
    $site = $this->mConfig->list_config();
    ?>
    <body>
        <div class="main-wrapper">
            <!--Begin header ?rapper-->
            <div class="header-wrapper header-position">
                <header id="header" class="container-header type1">
                    <div class="top-nav">
                        <div class="container">
                            <div class="row">
                                <div class="top-left col-sm-6 hidden-xs">
                                    <ul class="list-inline">
                                        <li>
                                            <a href="mailto:alumni@ikasmanssa.org">
                                                <span class="icon mail-icon"></span>
                                                <span class="text"><?php echo $site['email']?></span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <span class="icon phone-icon"></span>
                                                <span class="text"><?php echo $site['phone_number']?></span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="top-right col-sm-6 col-xs-12">
                                    <ul class="list-inline">
                                        <!--                                        <li class="top-search">
                                                                                    <form class="navbar-form search no-margin no-padding">
                                                                                        <input type="text" name="q" class="form-control input-search" placeholder="search..." autocomplete="off">
                                                                                        <button type="submit" class="lnr lnr-magnifier"></button>
                                                                                    </form>
                                                                                </li>-->
                                        <li class="login">
                                            <a href="<?php echo site_url('admin/login') ?>">Log In</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="header-middle">
                        <div class="container">
                            <div class="logo hidden-sm hidden-xs">
                                <a href="<?php echo site_url('home') ?>"> <img src="<?php echo base_url('assets') ?>/images/logo.png" alt="logo"></a>
                            </div>
                            <div class="menu">
                                <nav>
                                    <ul class="nav navbar-nav">
                                        <li <?php echo ($aktif == 'about') ? 'class="current"' : ''; ?>>
                                            <a href="<?php echo site_url('about') ?>">TENTANG KAMI</a>
                                        </li>
                                        <li <?php echo ($aktif == 'event') ? 'class="current"' : ''; ?>>
                                            <a href="<?php echo site_url('event') ?>">PROGRAM KEGIATAN</a>
                                        </li>
                                        <li <?php echo ($aktif == 'alumni') ? 'class="current"' : ''; ?>>
                                            <a href="<?php echo site_url('alumni') ?>">KISAH ALUMNI</a>
                                        </li>
<!--                                        <li <?php echo ($aktif == 'career') ? 'class="current"' : ''; ?>>
                                            <a href="<?php echo site_url('career') ?>">PELUANG & LOWONGAN</a>
                                        </li>-->
                                        <li <?php echo ($aktif == 'gallery') ? 'class="current"' : ''; ?>>
                                            <a href="<?php echo site_url('gallery') ?>">GALERI</a>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                            <div class="area-mobile-content visible-sm visible-xs">
                                <div class="logo-mobile">
                                    <a href="#"> <img src="<?php echo base_url('assets') ?>/images/logo-small.png" alt="logo"></a>
                                </div>
                                <div class="mobile-menu ">
                                </div>
                            </div>
                        </div>
                    </div>
                </header>
            </div>
            <!--End header wrapper-->

            <?php echo $content; ?>

            <!--Begin footer wrapper-->
            <div class="footer-wrapper type2">
                <footer class="foooter-container">
                    <div class="container">
                        <div class="footer-middle">
                            <div class="row">
                                <div class="col-md-4 col-sm-12 col-xs-12 animated footer-col">
                                    <div class="contact-footer">
                                        <div class="logo-footer">
                                            <a href="<?php echo site_url('home') ?>"><img src="<?php echo base_url('assets') ?>/images/logo-footer.png" alt=""></a>
                                        </div>
                                        <div class="contact-desc">
                                            <p class="text-light"><?php echo $site['about']?></p>
                                        </div>
                                        <div class="contact-phone-email">
                                            <span class="contact-phone"><a href="#"><?php echo $site['phone_number']?></a> </span>
                                            <span class="contact-email"><a href="#"><?php echo $site['email']?></a></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-5 col-sm-12  col-xs-12 animated footer-col">
                                    <div class="links-footer">
                                        <div class="row">
                                            <div class="col-sm-4 col-xs-12">
                                                <h6 class="heading-bold">DASHBOARD</h6>
                                                <ul class="list-unstyled no-margin">
                                                    <li><a href="<?php echo site_url('register') ?>">DAFTAR</a></li>
                                                    <!--<li><a href="<?php echo site_url('career') ?>">PELUANG</a></li>-->
                                                    <li><a href="<?php echo site_url('alumni') ?>">KISAH</a></li>
                                                    <li><a href="<?php echo site_url('admin/login') ?>">DIREKTORI</a></li>
                                                </ul>
                                            </div>

                                            <div class="col-sm-4 col-xs-12">
                                                <h6 class="heading-bold">TENTANG KAMI</h6>
                                                <ul class="list-unstyled no-margin">
                                                    <li><a href="<?php echo site_url('event') ?>">KEGIATAN</a></li>
                                                    <li><a href="<?php echo site_url('gallery') ?>">GALERI</a></li>
                                                    <li><a href="<?php echo site_url('home') ?>">BERANDA</a></li>
                                                    <li><a href="./homepage-2.html"></a></li>
                                                </ul>
                                            </div>

                                            <div class="col-sm-4 col-xs-12">
                                                <h6 class="heading-bold">SUPPORT</h6>
                                                <ul class="list-unstyled no-margin">
                                                    <!--<li><a href="./job-detail.html">FAQ</a></li>-->
                                                    <li><a href="<?php echo site_url('contact') ?>">KONTAK KAMI</a></li>
                                                    <!--<li><a href="./blog.html">ORGANIZER</a></li>-->
                                                    <!--<li><a href="./blog-single-fullwith.html">SOSIAL</a></li>-->
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12 animated footer-col">
                                    <div class="links-social">
                                        <div class="login-dashboard">
                                            <a href="<?php echo site_url('admin/login') ?>" class="bg-color-theme text-center text-regular">Login Alumni</a>
                                        </div>
                                        <ul class="list-inline text-center">
                                            <li><a href="<?php echo $site['twitter']?>"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                            <li><a href="<?php echo $site['instagram']?>"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                            <li><a href="<?php echo $site['facebook']?>"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="footer-bottom text-center">
                            <p class="copyright text-light">&copy;2019 IKASMANSSA</p>
                        </div>
                    </div>
                </footer>
            </div>
            <!--End footer wrapper-->
        </div>

        <script src="<?php echo base_url('assets') ?>/js/libs/jquery-2.2.4.min.js"></script>
        <script src="<?php echo base_url('assets') ?>/js/libs/bootstrap.min.js"></script>
        <script src="<?php echo base_url('assets') ?>/js/libs/owl.carousel.min.js"></script>
        <script src="<?php echo base_url('assets') ?>/js/libs/jquery.meanmenu.js"></script>
        <script src="<?php echo base_url('assets') ?>/js/libs/jquery.meanmenu.js"></script>
        <script src="<?php echo base_url('assets') ?>/js/libs/jquery.syotimer.js"></script>
        <script src="<?php echo base_url('assets') ?>/js/libs/parallax.min.js"></script>
        <script src="<?php echo base_url('assets') ?>/js/libs/jquery.waypoints.min.js"></script>
        <script src="<?php echo base_url('assets') ?>/js/custom/main.js"></script>
        <script src="<?php echo base_url('assets') ?>/jssocials/jssocials.min.js"></script>
       
        <script>
            jQuery(document).ready(function () {
                $('#time').syotimer({
                    year: 2019,
                    month: 12,
                    day: 7,
                    hour: 7,
                    minute: 7,
                });
            });
        </script>
        <script>
            $("#share").jsSocials({
                shares: [
//                    "email", 
                    "twitter",
                    "facebook",
                    "googleplus",
//                    "linkedin", 
//                    "pinterest", 
//                    "stumbleupon", 
//                    "whatsapp"
                ]
            });
        </script>
    </body>
</html>
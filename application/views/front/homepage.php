<!--Begin content wrapper-->
<div class="content-wrapper">
    <!--begin slider-->
    <div class="slider-hero">
        <div class="sliders-wrap columns1">
            <div class="item">
                <img src="<?php echo base_url('assets') ?>/images/slider2.jpg" alt="">
                <div class="owl-caption">
                    <div class="container">
                        <div class="content-block">
                            <h2 class="text-center">
                                <span class="text-bold">Selamat Datang di IKASMANSSA </span> <br />
                                <span class="text-white">guyub, gayeng, migunani</span>
                            </h2>
                            <a href="<?php echo site_url('about'); ?>" class="bnt bnt-theme read-story">BACA SELENGKAPNYA</a>
                        </div>
                    </div>
                </div>
            </div>
            <?php foreach ($berita AS $b) { ?>
                <div class="item">
                    <img src="<?php echo base_url('assets/upload/berita') . '/' . $b->foto ?>" style="height:688px" alt="">
                    <div class="owl-caption">
                        <div class="container">
                            <div class="content-block">
                                <h2>
                                    <span class="text-bold"><?php echo $b->judul ?></span> <br />
                                    <!--<span class="text-white">kerjasama dengan salah satu alumni di KEMHAN</span>-->
                                </h2>
                                <a href="<?php echo site_url('berita/detail/' . $b->slug) ?>" class="bnt bnt-theme read-story">BACA SELENGKAPNYA</a>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
    <!--end slider-->

    <!--begin upcoming event-->
    <div class="upcoming-event">
        <div class="container">
            <div class="row">
                <div class="area-img col-md-5 col-sm-12 col-xs-12">
                    <img class="img-responsive animated zoomIn" src="<?php echo base_url('assets/upload/event') . '/' . $new->foto ?>" alt="">
                </div>
                <div class="area-content col-md-7 col-sm-12 col-xs-12">
                    <div class="area-top">
                        <div class="row">
                            <div class="col-sm-10 col-xs-9">
                                <h5 class="heading-light no-margin animated fadeInRight">KEGIATAN AKAN DATANG</h5>
                                <h2 class="heading-bold animated fadeInLeft"><?php echo $new->judul ?></h2>
                                <span>
                                    <span class="icon map-icon"></span>
                                    <span class="text-place text-light animated fadeInRight"><?php echo $new->alamat ?></span>
                                </span>
                                    <!--<span><?php echo (strlen($new->isi) > 200 ) ? (substr($new->isi, 0, 200) . '...') : $row->isi ?></span>-->
                            </div>
                            <div class="col-sm-2 col-xs-3">
                                <div class="area-calendar calendar animated slideInRight">
                                    <span class="day text-bold"><?php echo date('d', strtotime($new->tgl_event)); ?></span>
                                    <span class="month text-light"><?php echo date('F', strtotime($new->tgl_event)); ?></span>
                                    <span class="year text-light bg-year"><?php echo date('Y', strtotime($new->tgl_event)); ?></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="area-bottom">
                        <!--<div id="time" class="pull-left animated slideInLeft"></div>-->
                        <a href="<?php echo site_url('event/detail/' . $new->slug) ?>" 
                           class="bnt bnt-theme join-now pull-right animated fadeIn">Selengkapnya</a>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!--end upcoming event-->

    <!--begin alumni dashboard-->
    <div class="alumni-dashboard">
        <div class="container">
            <div class="title title-dashboard type1">
                <h3 class="heading-light no-margin"> Dashboard Alumni IKASMANSSA </h3>
            </div>
            <div class="area-content">
                <div class="row">
                    <!--                    <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="icon mail-icon"></div>
                                            <div class="box-content">
                                                <h4 class="heading-regular"> Cek Pesan </h4>
                                                <p class="text-content text-margin text-light ">
                                                    Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam.
                                                </p>
                                            </div>
                                        </div>-->
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="icon account-icon"></div>
                        <div class="box-content">
                            <h4 class="heading-regular"> Perbarui Informasi </h4>
                            <p class="text-content text-margin text-light">
                                Perbaharui informasi dan lengkapi data diri anda agar dapat diverifikasi oleh admin maupun ketua angkatan.</p>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="icon group-icon"></div>
                        <div class="box-content">
                            <h4 class="heading-regular"> Daftar Alumni </h4>
                            <p class="text-content text-margin text-light">
                                Anda dapat melihat daftar rekan-rekan anda dalam satu angkatan.</p>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="icon search-icon"></div>
                        <div class="box-content">
                            <h4 class="heading-regular"> Cari Direktori Alumni </h4>
                            <p class="text-content text-margin text-light">
                            Anda dapat mencari data rekan yang anda inginkan beserta informasi terbarunya.</p>
                        </div>
                    </div>
                    <div class="login-dashboard text-center col-sm-12 col-xs-12">
                        <a href="<?php echo site_url('admin/login') ?>" class="bnt bnt-theme login-links">LOGIN ALUMNI IKASMANSSA</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--end alumni dashboard-->

    <!--begin block links-->
    <div class="block-links">
        <div class="container">
            <div class="row">
                <div class="block-news col-md-4 col-sm-12 col-xs-12">
                    <div class="column-news" style="height: 434px">
                        <div class="title-links">
                            <h3 class="heading-regular">Berita Terbaru</h3>
                        </div>
                        <div class="post-wrapper" style="height: 480px">
                            <?php foreach ($berita AS $b) {
                                ?>
                                <div class="post-item clearfix ">
                                    <div class="image-frame post-photo-wrapper">
                                        <a href="#"> <img src="<?php echo base_url('assets/upload/berita') . '/' . $b->foto ?>" alt=""
                                                          style="width:131px;height:89px"></a>
                                    </div>
                                    <div class="post-desc-wrapper">
                                        <div class="post-desc">
                                            <div class="post-title"><h6 class="heading-regular"><a href="<?php echo site_url('berita/detail/' . $b->slug) ?>"><?php echo $b->judul ?></a></h6></div>
                                            <div class="post-excerpt">
                                                <p><?php echo (strlen($b->isi) > 120 ) ? (substr($b->isi, 0, 120) . '...') : $b->isi ?></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>
                        </div>
                        <div class="view-all"><a href="<?php echo site_url('berita') ?>">Tampilkan Semua Berita</a></div>
                    </div>
                </div>
                <div class="block-news col-md-4 col-sm-12 col-xs-12">
                    <div class="column-news">
                        <div class="title-links">
                            <h3 class="heading-regular">Kisah Alumni Terbaru</h3>
                        </div>
                        <div class="post-wrapper" style="height: 480px">
                            <?php foreach ($wawancara AS $w) {
                                ?>
                                <div class="post-item clearfix ">
                                    <div class="image-frame post-photo-wrapper">
                                        <a href="#"> <img src="<?php echo base_url('assets/upload/wawancara') . '/' . $w->foto ?>" alt=""
                                                          style="width:131px;height:89px"></a>
                                    </div>
                                    <div class="post-desc-wrapper">
                                        <div class="post-desc">
                                            <div class="post-title"><h6 class="heading-regular"><a href="<?php echo site_url('alumni/detail/' . $w->slug) ?>"><?php echo $w->judul ?></a></h6></div>
                                            <div class="post-excerpt">
                                                <p><?php echo (strlen($w->isi) > 120 ) ? (substr($w->isi, 0, 120) . '...') : $w->isi ?></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>
                        </div>
                        <div class="view-all"><a href="<?php echo site_url('wawancara') ?>">Tampilkan Semua Kisah</a></div>
                    </div>
                </div>
                <!--                <div class="block-career col-md-4 col-sm-12 col-xs-12">
                                    <div class="column-career">
                                        <div class="title-links">
                                            <h3 class="heading-regular">Peluang Karir/Usaha</h3>
                                        </div>
                                        <div class="career-content">
                                            <div class="company-item clearfix">
                                                <div class="company-logo">
                                                    <img src="<?php echo base_url('assets') ?>/images/company-logo1.png" alt="">
                                                </div>
                                                <div class="company-desc-wrapper">
                                                    <div class="company-desc">
                                                        <div class="company-title"><h6 class="heading-regular"><a href="#">Kepala Satpam </a></h6></div>
                                                        <div class="company-excerpt">
                                                            <p>Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="company-item clearfix">
                                                <div class="company-logo">
                                                    <img src="<?php echo base_url('assets') ?>/images/company-logo2.png" alt="">
                                                </div>
                                                <div class="company-desc-wrapper">
                                                    <div class="company-desc">
                                                        <div class="company-title"><h6 class="heading-regular"><a href="#">Ternak Sapi Australi</a></h6></div>
                                                        <div class="company-excerpt">
                                                            <p>Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="company-item clearfix">
                                                <div class="company-logo">
                                                    <img src="<?php echo base_url('assets') ?>/images/company-logo3.png" alt="">
                                                </div>
                                                <div class="company-desc-wrapper">
                                                    <div class="company-desc">
                                                        <div class="company-title"><h6 class="heading-regular"><a href="#">Pengolah Data dan Admin</a></h6></div>
                                                        <div class="company-excerpt">
                                                            <p>Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="company-item clearfix">
                                                <div class="company-logo">
                                                    <img src="<?php echo base_url('assets') ?>/images/company-logo4.png" alt="">
                                                </div>
                                                <div class="company-desc-wrapper">
                                                    <div class="company-desc">
                                                        <div class="company-title"><h6 class="heading-regular"><a href="#">Jual Beli Motor</a></h6></div>
                                                        <div class="company-excerpt">
                                                            <p>Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="view-all"><a href="./career-opportunity.html">Tampilkan Semua Peluang</a></div>
                                    </div>
                
                                </div>-->
                <div class="block-event-calendar col-md-4 col-sm-12 col-xs-12">
                    <div class="column-calendar">
                        <div class="title-links">
                            <h3 class="heading-regular">Kalender Kegiatan</h3>
                        </div>
                        <div class="content-calendar bg-calendar no-padding">
                            <div class="top-section">
                                <h6 class="heading-light"><?php echo date('F Y') ?></h6>
                                <span class="icon calendar-icon pull-right"></span>
                            </div>
                            <div class="list-view">
                                <?php foreach ($event AS $e) { ?>
                                    <div class="view-item">
                                        <div class="date-item">
                                            <span class="dates text-light"><?php echo date('D', strtotime($e->tgl_event)); ?></span>
                                            <span class="day text-bold color-theme"><?php echo date('d', strtotime($e->tgl_event)); ?></span>
                                            <span class="month text-light"><?php echo date('M', strtotime($e->tgl_event)); ?></span>
                                        </div>
                                        <div class="date-desc-wrapper">
                                            <div class="date-desc">
                                                <div class="date-title"><h6 class="heading-regular"><?php echo $e->judul ?></h6></div>
                                                <div class="date-excerpt">
                                                    <p>Organizer: IKASMANSSA</p>
                                                </div>
                                                <div class="place">
                                                    <span class="icon map-icon"></span>
                                                    <span class="text-place"><?php echo $e->alamat ?></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="view-all"><a href="<?php echo site_url('event') ?>">Lihat Seluruh Kegiatan</a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--end block links-->

    <!--begin alumni interview-->

    <?php $random = $wawancara_random; ?>
    <div class="alumni-interview"
         style="background: url(<?php echo base_url() . '/assets/upload/wawancara/' . $random->foto ?>);background-repeat: no-repeat;
         background-attachment: fixed;
         background-size: 100% 100%;">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-xs-12 pull-right">
                    <div class="interview-wrapper" style="margin-top: 100px">
                        <div class="interview-title">
                            <h4 class="heading-light text-capitalize">Alumni Interview</h4>
                            <h1 class="heading-light text-capitalize"><?php echo $random->judul ?></h1>
                        </div>
                        <div class="interview-desc text-left animated rollIn">
                            <p class="text-light"><?php echo (strlen($random->isi) > 300 ) ? (substr($random->isi, 0, 300) . '...') : $random->isi ?></p>
                        </div>
                        <div class="interview-see-story animated zoomInLeft">
                            <a class="see-story bnt text-uppercase" href="<?php echo site_url('alumni/detail/' . $random->slug) ?>">BACA CERITA <?php echo $random->judul ?></a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <!--end alumni interview-->

</div>
<!--End content wrapper-->

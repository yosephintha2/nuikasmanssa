
<!--Begin content wrapper-->
<div class="content-wrapper">
    <!--    <div class="latst-article">
            <div class="container">
                <div class="area-img">
                    <img src="images/img-article.jpg" alt="">
                </div>
                <div class="area-content">
                    <div class="category animated fadeIn">
                        <a href="#" class="bnt text-regular">Community</a>
                    </div>
                    <div class="article-title">
                        <h2 class="text-regular text-capitalize animated rollIn">Engaging With the IKASMANSSA Community</h2>
                    </div>
                    <div class="stats">
                        <span class="clock">
                            <span class="icon clock-icon-while"></span>
                            <span class="text-center text-white">16 May 2019</span>
                        </span>
                        <span class="comment">
                            <span class="icon comment-icon-while"></span>
                            <span class="text-center text-white">10 Comments</span>
                        </span>
                    </div>
                </div>
            </div>
        </div>-->
    <div class="blog-content" style="margin-top: 110px">
        <div class="container">
            <div class="row">
                <!--                <div class="col-main col-lg-8 col-md-7 col-xs-12">-->
                <div class="col-main col-lg-12 col-md-12 col-xs-12">
                    <div class="articles">
                        <?php foreach ($data AS $row) { ?>
                            <div class="article-item">
                                <div class="area-img">
                                    <img src="<?php echo base_url('assets/upload/berita') . '/' . $row->foto ?>" alt="">
                                </div>
                                <div class="area-content">
                                    <div class="article-left col-lg-2 col-md-3 col-sm-3 col-xs-12 pull-left">
                                        <div class="catetory-title">
                                            <h6 class="text-regular">Berita</h6>
                                        </div>
                                        <div class="stats">
                                            <span class="icon user-icon"></span>
                                            <span class="text-content text-light">IKASMANSSA Admin</span>
                                        </div>
                                    </div>
                                    <div class="article-right col-lg-10 col-md-9 col-sm-9 col-xs-12 pull-left">
                                        <h3><a href="<?php echo site_url('berita/detail/' . $row->slug) ?>"><?php echo $row->judul ?></a></h3>
                                        <p><?php echo (strlen($row->isi) > 250 ) ? (substr($row->isi, 0, 250) . '...') : $row->isi ?></p>
                                        <div class="stats">
                                            <span class="clock">
                                                <span class="icon clock-icon"></span>
                                                <span class="text-center text-light"><?php echo date('l, d/m/Y h:i:s', strtotime($row->tgl_post)); ?></span>
                                            </span>
                                            <span class="comment">
                                                <span class="icon comment-icon"></span>
                                                <span class="text-center text-light"><?php echo $this->mBerita->countKomentarByBerita($row->idberita);?> Comments</span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
<!--                       <div class="article-item">
                                <div class="area-img">
                                    <img src="<?php echo base_url('assets') ?>/images/article-img-1.jpg" alt="">
                                </div>
                                <div class="area-content">
                                    <div class="article-left col-lg-2 col-md-3 col-sm-3 col-xs-12 pull-left">
                                        <div class="catetory-title">
                                            <h6 class="text-regular">News</h6>
                                        </div>
                                        <div class="stats">
                                            <span class="icon user-icon"></span>
                                            <span class="text-content text-light">IKASMANSSA Admin</span>
                                        </div>
                                    </div>
                                    <div class="article-right col-lg-10 col-md-9 col-sm-9 col-xs-12 pull-left">
                                        <h3><a href="#">Bringing Computer Skills to Classrooms</a></h3>
                                        <p>Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis.</p>
                                        <div class="stats">
                                            <span class="clock">
                                                <span class="icon clock-icon"></span>
                                                <span class="text-center text-light">16 May 2019</span>
                                            </span>
                                            <span class="comment">
                                                <span class="icon comment-icon"></span>
                                                <span class="text-center text-light">10 Comments</span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>-->
                    </div>
                    <div class="pagination-wrapper text-center">
                        <!--Tampilkan pagination-->
                        <?php echo $pagination; ?>
                    </div>
                </div>
            </div>
            <!--                <div class="sidebar blog-right col-lg-4 col-md-5 hidden-sm hidden-xs">
                                <div class="block-sidebar">
                                    <div class="block-item popurlar-port">
                                        <div class="block-title text-center">
                                            <h5 class="text-regular text-uppercase">POPULAR POST</h5>
                                        </div>
                                        <div class="block-content">
                                            <ul>
                                                <li>
                                                    <div class="area-img">
                                                        <img src="images/post-right-1.jpg" alt="">
                                                    </div>
                                                    <div class="area-content">
                                                        <h6>Project Teach shows youngsters what�s possible</h6>
                                                        <div class="stats">
                                                            <span class="clock">
                                                                <span class="icon clock-icon"></span>
                                                                <span class="text-content text-light">16 May 2019</span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="area-img">
                                                        <img src="images/post-right-2.jpg" alt="">
                                                    </div>
                                                    <div class="area-content">
                                                        <h6>Claritas est etiam processus dynamicus, quisasma</h6>
                                                        <div class="stats">
                                                            <span class="clock">
                                                                <span class="icon clock-icon"></span>
                                                                <span class="text-content text-light">16 May 2019</span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="area-img">
                                                        <img src="images/post-right-3.jpg" alt="">
                                                    </div>
                                                    <div class="area-content">
                                                        <h6>Typi non habent claritatem insitam est usus legentis in</h6>
                                                        <div class="stats">
                                                            <span class="clock">
                                                                <span class="icon clock-icon"></span>
                                                                <span class="text-content text-light">16 May 2019</span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="area-img">
                                                        <img src="images/post-right-4.jpg" alt="">
                                                    </div>
                                                    <div class="area-content">
                                                        <h6>Dolore eu feugiat nulla facilisis at vero eros et accumsan et i</h6>
                                                        <div class="stats">
                                                            <span class="clock">
                                                                <span class="icon clock-icon"></span>
                                                                <span class="text-content text-light">16 May 2019</span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="area-img">
                                                        <img src="images/post-right-5.jpg" alt="">
                                                    </div>
                                                    <div class="area-content">
                                                        <h6>Dignissim qui blandit praesent luptatum zzril delenit augue dui</h6>
                                                        <div class="stats">
                                                            <span class="clock">
                                                                <span class="icon clock-icon"></span>
                                                                <span class="text-content text-light">16 May 2019</span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="block-item twitter">
                                        <div class="block-title text-center">
                                            <h5 class="text-regular text-uppercase">TWITTER</h5>
                                        </div>
                                        <div class="block-content">
                                            <div class="twitter-wrapper text-center">
                                                <div class="twitter-icon color-theme">
                                                    <i class="fa fa-twitter" aria-hidden="true"></i>
                                                </div>
                                                <div class="twitter-content">
                                                    <div class="twitter-desc">
                                                        <p class="text-light text-center">�Dignissim qui blandit praesent luptatum zril delenit augue duis dolore te feugait nulla facilisi. Nam liber tempor cum  <a href="#" class="color-theme">@#ikasmanssa</a>�</p>
                                                        <div class="twitter-user">
                                                            <span class="text-regular">@KathleenLittle</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="block-item tag">
                                        <div class="block-title text-center">
                                            <h5 class="text-regular text-uppercase">POPULAR TAGS</h5>
                                        </div>
                                        <div class="block-content">
                                            <ul class="list-inline">
                                                <li><a href="#">IKASMANSSA</a></li>
                                                <li><a href="#">community</a></li>
                                                <li><a href="#">news</a></li>
                                                <li><a href="#">alumni</a></li>
                                                <li><a href="#">profile</a></li>
                                                <li><a href="#">interview</a></li>
                                                <li><a href="#">Monashku</a></li>
                                                <li><a href="#">SMANSSA</a></li>
                                                <li><a href="#">psd</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>-->
        </div>
    </div>
</div>

</div>
<!--End content wrapper-->

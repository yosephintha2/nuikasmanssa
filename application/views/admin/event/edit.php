<script src="<?php echo base_url() ?>assets/tinymce/js/tinymce/tinymce.min.js"></script>
<link rel="stylesheet" href="<?= base_url('assets') ?>/bootstrap-datepicker/css/bootstrap-datepicker.min.css"/>

<script type="text/javascript">
    tinymce.init({
        file_browser_callback: function (field, url, type, win) {
            tinyMCE.activeEditor.windowManager.open({
                file: '<?php echo base_url() ?>assets/kcfinder/browse.php?opener=tinymce4&field=' + field + '&type=' + type,
                title: 'KCFinder',
                width: 700,
                height: 500,
                inline: true,
                close_previous: false
            }, {
                window: win,
                input: field
            });
            return false;
        },
        selector: "#isi",
        height: 250,
        plugins: [
            "advlist autolink lists link image charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste"
        ],
        toolbar: "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
    });
</script>
<script type="text/javascript">
    $(function () {
        $("#date").datepicker({
            changeMonth: true,
            changeYear: true,
            format: "yyyy-mm-dd"
//            onClose: function(selectedDate) {
//                $("#akhir").datepicker("option", "minDate", selectedDate);
//            }
        });
    });
</script>

<style>
    #imagePreview {
        margin-top: 7px;
        width: 569px;
        height: 331px;
        background-position: center center;
        background-size: cover;
        -webkit-box-shadow: 0 0 1px 1px rgba(0, 0, 0, .3);
        display: inline-block;
        background-image: url('<?php echo base_url('assets') . '/images/placeholder.png' ?>')
    }
</style>
<script type="text/javascript">
//    $(function () {
//        $("#file").on("change", function ()
//        {
//            var files = !!this.files ? this.files : [];
//            if (!files.length || !window.FileReader)
//                return; // no file selected, or no FileReader support
//
//            if (/^image/.test(files[0].type)) { // only image file
//                var reader = new FileReader(); // instance of the FileReader
//                reader.readAsDataURL(files[0]); // read the local file
//
//                reader.onloadend = function () { // set image data as background of div
//                    $("#imagePreview").css("background-image", "url(" + this.result + ")");
//                }
//            }
//        });
//    });

    function PreviewImage() {
        var oFReader = new FileReader();
        oFReader.readAsDataURL(document.getElementById("file").files[0]);
        oFReader.onload = function (oFREvent) {
            document.getElementById("img-preview").src = oFREvent.target.result;
        };
    }

</script>


<?php
// Session 
if ($this->session->flashdata('sukses')) {
    echo '<div class="alert alert-success">';
    echo $this->session->flashdata('sukses');
    echo '</div>';
}

// File upload error
if (isset($error)) {
    echo '<div class="alert alert-success">';
    echo $error;
    echo '</div>';
}

// Error
echo validation_errors('<div class="alert alert-success">', '</div>');
?>

<form action="<?php echo site_url('admin/event/edit/' . $event['idevent']) ?>" method="post" enctype="multipart/form-data">

    <div class="col-md-6">
        <div class="form-group input-group-lg">
            <label>Title</label>
            <input type="text" name="title" class="form-control" value="<?php echo $event['judul'] ?>" required placeholder="Title">
        </div> 
        <div class="form-group">
            <label>Date</label>
            <input type="text" name="date" class="datepicker form-control" id="date" autocomplete="off" data-date-format="yyyy-mm-dd"
                   value="<?php echo $event['tgl_event'] ?>" placeholder="Date">
        </div>
        <!--<button id="tambah" class="btn btn-primary">Tambah Survei</button>-->
        <div class="form-group">
            <label>Address</label>
            <textarea name="alamat" placeholder="Address" class="form-control" required=""><?php echo $event['alamat'] ?></textarea>
        </div>
        <!--        <div class="form-group">
                    <label>Latitude</label>
                    <input type="text" name="latitude" class="form-control" value="<?php echo set_value('latitude') ?>" placeholder="Latitude">
                </div>
                <div class="form-group">
                    <label>Longitude</label>
                    <input type="text" name="longitude" class="form-control" value="<?php echo set_value('longitude') ?>" placeholder="Longitude">
                </div>-->

        <div class="form-group">
            <label>Paste Google Maps Frame Here</label>
            <textarea name="gmap" placeholder="Google Maps Frame" class="form-control"><?php echo $event['google_map'] ?></textarea>
        </div>
        <div class="form-group">
            <label>Example:</label><br>
            <img src="<?php echo base_url('assets/upload/image/frame_maps.png/') ?>" class="img-responsive">
        </div>
        <div class="form-group">
            <label>Upload Image</label>
             <input type="file" name="image" class="form-control" id="file" accept="image/*" onchange="PreviewImage();">
            <div id="imagePreview"><img id="img-preview" style="width: 569px;height: 331px;"
                                        src="<?php echo base_url('assets/upload/event/' . $event['foto']) ?>"></div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group">
            <label>Description</label>
            <textarea name="desc" placeholder="Description Event" class="form-control" id="isi"><?php echo $event['isi'] ?></textarea>
        </div>

        <div class="form-group">
            <input type="submit" name="submit" value="Save" class="btn btn-primary">
            <input type="reset" name="reset" value="Reset" class="btn btn-default">
        </div>
    </div>

</form>
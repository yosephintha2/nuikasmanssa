<?php
// Session 
if ($this->session->flashdata('sukses')) {
    echo '<div class="alert alert-success">';
    echo $this->session->flashdata('sukses');
    echo '</div>';
}
// Error
echo validation_errors('<div class="alert alert-success">', '</div>');
?>

<!--  Modals-->
<div class="panel-body">
    <p>
        <a href="<?php echo site_url('admin/event/create') ?>" class="btn btn-primary"><i class="fa fa-plus"></i> Create New Event</a>
    </p>



    <table class="table table-striped table-bordered table-hover table-responsive" id="dataTables-example" width="100%">
        <thead>
            <tr>
                <th>#</th>
                <th width="30%">Title</th>
                <th>Date</th>
                <th width="30%">Address</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $i = 1;
            foreach ($event as $list) {
                ?>
                <tr class="odd gradeX">
                    <td><?php echo $i; ?></td>
                    <td>
                        <?php echo substr(strip_tags($list['judul']), 0, 20) ?><br> 
                        <a href="<?php echo site_url('event/detail/' . $list['slug']) ?>" target="blank">
                            <?php echo substr(strip_tags($list['slug']), 0, 20) ?><sup><i class="fa fa-link"></i></sup></a>
                    </td>        

                    <td><?php echo date('l, d/m/Y', strtotime($list['tgl_event'])); ?></td>           
                    <td><?php echo $list['alamat'] ?>
                        <!--                        <br>
                                                <a href="<?php echo site_url('admin/event/map/' . $list['idevent']) ?>" target="blank">
                                                    View Map<sup><i class="fa fa-link"></i></sup></a>-->
                    </td>           
                    <td class="center">
                        <a href="<?php echo site_url('admin/event/edit/' . $list['idevent']); ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                        <!-- View Biz -->
                        <!--  Modals-->
                        <button class="btn btn-success" data-toggle="modal" data-target="#View<?php echo $list['idevent']; ?>"><i class="fa fa-eye"></i></button>

                        <div class="modal fade" id="View<?php echo $list['idevent']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title" id="myModalLabel">View Post</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="col-md-12">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-striped table-bordered table-hover">
                                                <tr>
                                                <img src="<?php echo base_url('assets/upload/event/' . $list['foto']); ?>" width="539px">
                                                <td>Title</td>
                                                <td><?php echo $list['judul'] ?></td>
                                                </tr>

                                                <tr>
                                                    <td>Date</td>
                                                    <td><?php echo date('l, d/m/Y', strtotime($list['tgl_event'])) ?></td>
                                                </tr>   
                                                <tr>
                                                    <td>Address</td>
                                                    <td><?php echo $list['alamat']; ?></td>
                                                </tr>   
                                                <tr>
                                                    <td colspan="2">
                                                <center><?php echo $list['google_map']; ?></center></td>
                                                </tr>                   
                                                <tr>
                                                    <td colspan="2">Description : <?php echo $list['isi']; ?></td>
                                                </tr>                   
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td>
                                                        <a href="<?php echo site_url('admin/event/edit_event/' . $list['idevent']) ?>" class="btn btn-primary">Edit</a>
                                                        <a href="<?php echo site_url('admin/event/delete_event/' . $list['idevent']) ?>" class="btn btn-danger">Delete</a>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>

                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Modals-->        
                        <a href="<?php echo site_url('admin/event/delete_event/' . $list['idevent']); ?>" class="btn btn-danger" onClick="return confirm('Are you sure?')"><i class="fa fa-trash"></i></a>

                    </td>
                </tr>
                <?php
                $i++;
            }
            ?>
        </tbody>
    </table>
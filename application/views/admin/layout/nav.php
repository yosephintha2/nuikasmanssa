<!-- /. NAV TOP  -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<nav class="navbar-default navbar-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav" id="main-menu">

            <li><a  href="<?php echo site_url('admin/dashboard') ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <?php if ($this->session->userdata('roles') == 1) { ?>
                <li><a href="#"><i class="fa fa-users"></i> Users<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li><a href="<?php echo site_url('admin/users/admin') ?>">List Users</a></li>
                    </ul>
                </li>
                <li><a href="#"><i class="fa fa-pencil"></i> Blog<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li><a href="<?php echo site_url('admin/berita') ?>">List Blog</a></li>
                        <li><a href="<?php echo site_url('admin/berita/create') ?>">Create Blog</a></li>
                        <li><a href="<?php echo site_url('admin/berita/kategori') ?>">Categories</a></li>
                    </ul>
                </li>    
                <li><a href="#"><i class="fa fa-file-text"></i> Wawancara<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li><a href="<?php echo site_url('admin/wawancara') ?>">List Wawancara</a></li>
                        <li><a href="<?php echo site_url('admin/wawancara/create') ?>">Create Wawancara</a></li>
                    </ul>
                </li> 
                <li><a href="#"><i class="fa fa-calendar-check-o"></i> Event<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li><a href="<?php echo site_url('admin/event') ?>">List Event</a></li>
                        <li><a href="<?php echo site_url('admin/event/create') ?>">Create Event</a></li>
                    </ul>
                </li>
                <li><a href="#"><i class="fa fa-image"></i> Galleries<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="<?php echo site_url('admin/galeri') ?>">List Galleries</a></li>
                            <li><a href="<?php echo site_url('admin/galeri/create') ?>">Create Galleries</a></li>
                        </ul>
                    </li>          
            <?php } ?>

            <li><a href="#"><i class="fa fa-address-book"></i> Alumni<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li><a href="<?php echo site_url('admin/alumni') ?>">List Alumni</a></li>
                    <?php if ($this->session->userdata('roles') == 1 OR $this->session->userdata('roles') == 2) { ?>
                        <li><a href="<?php echo site_url('admin/alumni/create') ?>">Create Alumni</a></li>
                    <?php } ?>
                </ul>
            </li>   
            <?php if ($this->session->userdata('roles') == 1) { ?>
             <li><a  href="<?php echo site_url('admin/dashboard/config') ?>"><i class="fa fa-gear"></i> Setting</a></li>
            <?php } ?>

        </ul>
    </div>

</nav>  
<!-- /. NAV SIDE  -->
<div id="page-wrapper" >
    <div id="page-inner">


        <div class="row">
            <div class="col-md-12">
                <!-- Advanced Tables -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h2><?php echo $title ?></h2>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
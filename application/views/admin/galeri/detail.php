<?php
// Session 
if ($this->session->flashdata('sukses')) {
    echo '<div class="alert alert-success">';
    echo $this->session->flashdata('sukses');
    echo '</div>';
}
// Error
echo validation_errors('<div class="alert alert-success">', '</div>');
?>

<style>   
    .object-fit_fill { object-fit: fill }
    .object-fit_contain { object-fit: contain }
    .object-fit_cover { object-fit: cover }
    .object-fit_none { object-fit: none }
    .object-fit_scale-down { object-fit: scale-down }

    /*    html { 
            color: #eee; 
            padding: 30px;
            font-family: 'Source Code Pro', Monaco;
            background-color: #333;
        }
    
        p { font-weight: 200; font-size: 13px; margin-bottom: 10px; margin-top: 0;}*/

    .img { height: 200px; background-color: #444;}

    .img[class] { 
        width: 100%;
    }

    .original-image {
        margin-bottom: 50px;
    }

/*    .imagex {
        float: left;
        width: 40%;
        margin: 0 30px 20px 0;

        &:nth-child(2n) {
            clear: left;
        }

        &:nth-child(2n+1){
            margin-right: 0;
        }
    }*/
</style>

<!--  Modals-->
<div class="panel-body">
    <p><?php echo $galeri['deskripsi'] ?></p>
    <p><?php echo date('l, d/m/Y', strtotime($galeri['tgl_galeri'])); ?></p>
    <br>
    <div class="row">
        <?php
        foreach ($galeri_detail AS $gd) {
            $path = base_url() . 'assets/upload/galeri/' . $gd->idgaleri . '/';
            ?>
            <div class="col-md-3">
                <div class="thumbnail">
                    <a href="<?php echo $path . $gd->foto ?>" target="blank">
                        <div class="imagex">
                            <img class="object-fit_contain img" src="<?php echo $path . $gd->foto ?>" alt="Lights" style="width:100%">
                        </div>
                        <div class="caption">
                            <p><?php echo date('l, d/m/Y', strtotime($gd->tgl_post)) ?></p>
                            <p><?php echo (strlen($gd->keterangan) > 150 ) ? (substr($gd->keterangan, 0, 150) . '...') : $gd->keterangan ?></p>
                        </div>
                    </a>
                </div>
            </div>
        <?php } ?>
    </div>
</div>
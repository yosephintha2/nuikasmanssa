<?php
// Session 
if ($this->session->flashdata('sukses')) {
    echo '<div class="alert alert-success">';
    echo $this->session->flashdata('sukses');
    echo '</div>';
}
// Error
echo validation_errors('<div class="alert alert-success">', '</div>');
?>

<!--  Modals-->
<div class="panel-body">
    <p>
        <a href="<?php echo site_url('admin/galeri/create') ?>" class="btn btn-primary"><i class="fa fa-plus"></i> Create New Galeri</a>
    </p>



    <table class="table table-striped table-bordered table-hover table-responsive" id="dataTables-example" width="100%">
        <thead>
            <tr>
                <th>#</th>
                <th width="30%">Title</th>
                <th>Date</th>
                <th width="30%">Description</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $i = 1;
            foreach ($galeri as $list) {
                ?>
                <tr class="odd gradeX">
                    <td><?php echo $i; ?></td>
                    <td>
                        <?php echo substr(strip_tags($list['judul']), 0, 20) ?><br> 
                        <a href="<?php echo site_url('gallery/detail/' . $list['slug']) ?>" target="blank">
                            <?php echo substr(strip_tags($list['slug']), 0, 20) ?><sup><i class="fa fa-link"></i></sup></a>
                    </td>        

                    <td><?php echo date('l, d/m/Y', strtotime($list['tgl_galeri'])); ?></td>           
                    <td><?php echo $list['deskripsi'] ?></td>           
                    <td class="center">
                        <a href="<?php echo site_url('admin/galeri/edit/' . $list['idgaleri']); ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                        <a href="<?php echo site_url('admin/galeri/detail/' . $list['idgaleri']); ?>" class="btn btn-success" target="blank"><i class="fa fa-eye"></i></a>
<!--<button class="btn btn-success" data-toggle="modal" data-target="#View<?php echo $list['idgaleri']; ?>"><i class="fa fa-eye"></i></button>-->     
                        <a href="<?php echo site_url('admin/galeri/delete_galeri/' . $list['idgaleri']); ?>" class="btn btn-danger" onClick="return confirm('Are you sure?')"><i class="fa fa-trash"></i></a>

                    </td>
                </tr>
                <?php
                $i++;
            }
            ?>
        </tbody>
    </table>
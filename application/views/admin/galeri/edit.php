<script src="<?php echo base_url() ?>assets/tinymce/js/tinymce/tinymce.min.js"></script>
<link rel="stylesheet" href="<?= base_url('assets') ?>/bootstrap-datepicker/css/bootstrap-datepicker.min.css"/>


<script type="text/javascript">
    tinymce.init({
        file_browser_callback: function (field, url, type, win) {
            tinyMCE.activeEditor.windowManager.open({
                file: '<?php echo base_url() ?>assets/kcfinder/browse.php?opener=tinymce4&field=' + field + '&type=' + type,
                title: 'KCFinder',
                width: 700,
                height: 500,
                inline: true,
                close_previous: false
            }, {
                window: win,
                input: field
            });
            return false;
        },
        selector: "#isi",
        height: 250,
        plugins: [
            "advlist autolink lists link image charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste"
        ],
        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
    });
</script>

<script type="text/javascript">
    $(function () {
        $("#date").datepicker({
            changeMonth: true,
            changeYear: true,
            format: "yyyy-mm-dd"
//            onClose: function(selectedDate) {
//                $("#akhir").datepicker("option", "minDate", selectedDate);
//            }
        });
    });
</script>

<style>
    #imagePreview {
        margin-top: 7px;
        width: 569px;
        height: 331px;
        background-position: center center;
        background-size: cover;
        -webkit-box-shadow: 0 0 1px 1px rgba(0, 0, 0, .3);
        display: inline-block;
        background-image: url('<?php echo base_url('assets') . '/images/placeholder.png' ?>')
    }
</style>
<script type="text/javascript">
    $(document).ready(function () {

        var i = 0;
        $('#add').click(function () {
            i++;
            $('#dynamic_field').append('<tr id="row' + i + '">\n\
    <td><input type="file" name="image[]" \n\
    class="form-control image_list" accept="image/*" required id="input-photo' + i + '" multiple="multiple" \n\
required onchange="PreviewImage(' + i + ');"/></td>\n\
<td><center><div id="photo-preview' + i + '"><i id="no' + i + '">(No image)</i><img src="" id="img-preview' + i + '" style="width: 100px"/>\n\
</div></center></td>\n\
<td><textarea name="keterangan[]" placeholder="Description" class="form-control ket_list" multiple="multiple" \n\
placeholder="Description"></textarea></td>\n\
<td><center><button type="button" name="remove" id="' + i + '" class="btn btn-danger btn_remove"><i class="fa fa-times"></i></button>\n\
<center></td></tr>');

        });
        $(document).on('click', '.btn_remove', function () {
            var button_id = $(this).attr("id");
            $('#row' + button_id + '').remove();
        });
    });
</script>

<script>
    function PreviewImage(i)
    {
        //$('#img-preview'+$i ).show();
        var oFReader = new FileReader();
        oFReader.readAsDataURL(document.getElementById("input-photo" + i).files[0]);
        oFReader.onload = function (oFREvent) {
            document.getElementById("img-preview" + i).src = oFREvent.target.result;
            document.getElementById("no" + i).innerHTML = '';
        };
    }
</script>
<?php
// Session 
if ($this->session->flashdata('sukses')) {
    echo '<div class="alert alert-success">';
    echo $this->session->flashdata('sukses');
    echo '</div>';
}

// File upload error
if (isset($error)) {
    echo '<div class="alert alert-success">';
    echo $error;
    echo '</div>';
}

// Error
echo validation_errors('<div class="alert alert-success">', '</div>');
?>

<form action="<?php echo site_url('admin/galeri/edit/' . $galeri['idgaleri']) ?>" method="post" enctype="multipart/form-data">

    <div class="col-md-6">
        <div class="form-group input-group-lg">
            <label>Title</label>
            <input type="text" name="title" class="form-control" value="<?php echo $galeri['judul'] ?>" required placeholder="Title">
        </div> 
        <div class="form-group">
            <label>Description</label>
            <textarea name="desc" placeholder="Description" class="form-control" required=""><?php echo $galeri['deskripsi'] ?></textarea>
        </div>
    </div>
    <div class="col-md-10">
        <div class="form-group">
            <label>Upload Image</label>
            <table class="table table-bordered table-hover table-responsive">
                <tbody>
                    <tr>
                        <td width="30%"><center>Image</center></td>
                <td><center>Preview</center></td>
                <td><center>Description</center></td>
                <td><center>Action</center></td>
                </tr>
                <?php foreach ($galeri_detail AS $gd) { ?>
                    <tr>
                        <td><?php echo $gd->foto ?></td>
                        <td><center><img src="<?php echo base_url() . 'assets/upload/galeri/' . $gd->idgaleri . '/' . $gd->foto ?>" 
                                     style="width: 100px"/></center></td>
                    <td><?php echo $gd->keterangan ?></td>
                    <td>  <input type="checkbox" name="ck_lampiran[]" value="<?php echo $gd->idgaleri_detail ?>"><i style="font-size: 10pt"> Hapus</i></td>
                    </tr>
                <?php } ?>
<!--                <tr>
                <td>
                    <input type="file" name="image[]" class="form-control image_list" accept="image/*" required
                           id="input-photo1" multiple="multiple" required onchange="PreviewImage(1);"/>
                </td>
                <td><center>
                <div id="photo-preview1"><i id="no1">(No image)</i><img src="" id="img-preview1" style="width: 100px"/></div>
            </center>
            </td>
            <td>
                <textarea name="keterangan[]" placeholder="Description" class="form-control ket_list" multiple="multiple" placeholder="Description"></textarea>
                <input type="text" name="keterangan[]" class="form-control type_list" multiple="multiple" placeholder="Description"/>
            </td>
            <td><center><button type="button" name="add" id="add" class="btn btn-success"><i class="fa fa-plus"></i></button></center></td>
            </tr>-->
                <tr><td colspan="4"><button type="button" name="add" id="add" class="btn btn-success"><i class="fa fa-plus"></i> Add Image</button></td></tr>
                </tbody>
                <tbody id="dynamic_field"></tbody>
            </table>
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group">
            <input type="submit" name="submit" value="Save" class="btn btn-primary">
            <input type="reset" name="reset" value="Reset" class="btn btn-default">
        </div>
    </div>

</form>


<?php
// Session 
if ($this->session->flashdata('sukses')) {
    echo '<div class="alert alert-success">';
    echo $this->session->flashdata('sukses');
    echo '</div>';
}
// Error
echo validation_errors('<div class="alert alert-success">', '</div>');
?>

<form action="<?php echo site_url('admin/dashboard/config') ?>" method="post">

    <input type="hidden" name="config_id" value="<?php echo $site['config_id'] ?>">

    <div class="col-md-6">
        <h3>General Settings</h3><hr>
        <div class="form-group">
            <label>Website Name</label>
            <input type="text" name="nameweb" placeholder="Website Name" value="<?php echo $site['nameweb'] ?>" required class="form-control">
        </div>
         <div class="form-group">
            <label>About</label>
            <textarea name="about" placeholder="About" class="form-control" required=""><?php echo $site['about'] ?></textarea>
        </div> 
        <div class="form-group">
            <label>Email</label>
            <input type="email" name="email" placeholder="Email" value="<?php echo $site['email'] ?>" required class="form-control">
        </div>
        <div class="form-group">
            <label>Phone Number</label>
            <input type="text" name="phone_number" placeholder="Phone Number" value="<?php echo $site['phone_number'] ?>" required class="form-control">
        </div> 
        <div class="form-group">
            <label>Address</label>
            <textarea name="address" placeholder="Address" class="form-control"><?php echo $site['address'] ?></textarea>
        </div> 
        <div class="form-group">
            <label>City</label>
            <input type="text" name="city" placeholder="City" value="<?php echo $site['city'] ?>" class="form-control">
        </div>   
        <div class="form-group">
            <label>Zip Code</label>
            <input type="text" name="zip_code" placeholder="City" value="<?php echo $site['zip_code'] ?>" class="form-control">
        </div>
    </div>
    <div class="col-md-6">
        <h3>Social Media</h3><hr>
        <div class="form-group">
            <label>Facebook</label>
            <input type="text" name="facebook" placeholder="Facebook" value="<?php echo $site['facebook'] ?>" class="form-control">
        </div>   
        <div class="form-group">
            <label>Twitter</label>
            <input type="text" name="twitter" placeholder="Twitter" value="<?php echo $site['twitter'] ?>" class="form-control">
        </div>  
        <div class="form-group">
            <label>Instagram</label>
            <input type="text" name="instagram" placeholder="Instagram" value="<?php echo $site['instagram'] ?>" class="form-control">
        </div>   
    </div>

    <div class="col-md-12">
        <input type="submit" name="submit" value="Save" class="btn btn-primary">
        <input type="reset" name="reset" value="Reset" class="btn btn-default">
    </div>

</form>
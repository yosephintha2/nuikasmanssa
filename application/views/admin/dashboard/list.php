<div class="col-md-4 col-sm-12 col-xs-12">                       
    <div class="panel panel-primary text-center no-boder bg-color-gray">
        <div class="panel-body">
            <i class="fa fa-address-book fa-5x"></i>
            <h3><?php echo $alumni ?> Alumni </h3>
        </div>
        <div class="panel-footer back-footer-white putih">
            <a href="<?php echo site_url() ?>/admin/alumni">Management Alumni</a>
        </div></div></div>

<?php if ($this->session->userdata('roles') == 1) { ?>
    <div class="col-md-4 col-sm-12 col-xs-12">                       
        <div class="panel panel-primary text-center no-boder bg-color-gray">
            <div class="panel-body">
                <i class="fa fa-pencil fa-5x"></i>
                <h3><?php echo $berita ?> Blog </h3>
            </div>
            <div class="panel-footer back-footer-white putih">
                <a href="<?php echo site_url() ?>/admin/berita">Management Blog</a>
            </div></div></div>

    <div class="col-md-4 col-sm-12 col-xs-12">                       
        <div class="panel panel-primary text-center no-boder bg-color-gray">
            <div class="panel-body">
                <i class="fa fa-file-text fa-5x"></i>
                <h3><?php echo $wawancara ?> Wawancara </h3>
            </div>
            <div class="panel-footer back-footer-white putih">
                <a href="<?php echo site_url() ?>/admin/wawancara">Management Wawancara</a>
            </div></div></div>

    <div class="col-md-4 col-sm-12 col-xs-12">                       
        <div class="panel panel-primary text-center no-boder bg-color-gray">
            <div class="panel-body">
                <i class="fa fa-calendar-check-o fa-5x"></i>
                <h3><?php echo $event ?> Event </h3>
            </div>
            <div class="panel-footer back-footer-white putih">
                <a href="<?php echo site_url() ?>/admin/event">Management Event</a>
            </div></div></div>


    <div class="col-md-4 col-sm-12 col-xs-12">                       
    <div class="panel panel-primary text-center no-boder bg-color-gray">
        <div class="panel-body">
            <i class="fa fa-image fa-5x"></i>
            <h3><?php echo $galeri ?> Galleries </h3>
        </div>
        <div class="panel-footer back-footer-white putih">
            <a href="<?php echo site_url() ?>/admin/galeri">Management Galleries</a>
        </div></div></div>  


    <div class="col-md-4 col-sm-12 col-xs-12">                       
        <div class="panel panel-primary text-center no-boder bg-color-gray">
            <div class="panel-body">
                <i class="fa fa-user fa-5x"></i>
                <h3><?php echo $admins ?> Users </h3>
            </div>
            <div class="panel-footer back-footer-white putih">
                <a href="<?php echo site_url() ?>/admin/user_admin">Management Users</a>
            </div></div></div>      

<?php } ?>





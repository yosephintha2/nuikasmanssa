<?php
// Session 
if ($this->session->flashdata('sukses')) {
    echo '<div class="alert alert-success">';
    echo $this->session->flashdata('sukses');
    echo '</div>';
}
// Error
echo validation_errors('<div class="alert alert-success">', '</div>');
?>

<!--  Modals-->
<div class="panel-body">
    <p>
        <a href="<?php echo site_url('admin/berita/create') ?>" class="btn btn-primary"><i class="fa fa-plus"></i> Create New Blog</a>
        <a href="<?php echo site_url('admin/berita/kategori') ?>" class="btn btn-default"><i class="fa fa-plus"></i> Create New Category</a>
    </p>



    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
        <thead>
            <tr>
                <th>#</th>
                <th>Title</th>
                <th>Category</th>
                <th>Status</th>
                <th>Date Post</th>
                <th width="150px">Aksi</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $i = 1;
            foreach ($berita as $list) {
                ?>
                <tr class="odd gradeX">
                    <td><?php echo $i; ?></td>
                    <td>
                        <?php echo substr(strip_tags($list['judul']), 0, 20) ?> <br> 
                        <a href="<?php echo site_url('berita/detail/' . $list['slug']) ?>" target="blank">
                            <?php echo substr(strip_tags($list['slug']), 0, 20) ?><sup><i class="fa fa-link"></i></sup>
                        </a>
                    </td>        
                    <td>
                        <?php echo $list['nama'] ?> <br> 
                        <a href="<?php echo site_url('admin/berita/kategori/'); ?>">
                            View Categories <sup><i class="fa fa-eye"></i></sup>
                        </a>
                    </td>
                    <td><center><?php
                echo ($list['aktif'] == 1) ? '<h5><span class="label label-success">Aktif</span></h5>' :
                        '<h5><span class="label label-default">Non Aktif</span></h5>';
                ?></center></td>
            <td><?php echo date('l, d/m/Y', strtotime($list['tgl_post'])); ?></td>

            <td class="center">
                <a href="<?php echo site_url('admin/berita/edit/' . $list['idberita']); ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                <!-- View Biz -->
                <!--  Modals-->
                <button class="btn btn-success" data-toggle="modal" data-target="#View<?php echo $list['idberita']; ?>"><i class="fa fa-eye"></i></button>

                <div class="modal fade" id="View<?php echo $list['idberita']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title" id="myModalLabel">View Post</h4>
                            </div>
                            <div class="modal-body">
                                <div class="col-md-12">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-striped table-bordered table-hover">
                                        <tr>
                                        <img src="<?php echo base_url('assets/upload/berita/' . $list['foto']); ?>" width="539px">
                                        <td>Title</td>
                                        <td><?php echo $list['judul'] ?></td>
                                        </tr>
                                        <tr>
                                            <td>Category</td>
                                            <td><?php echo $list['nama']; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Status</td>
                                            <td><?php
                                                echo ($list['aktif'] == 1) ?
                                                        '<h5><span class="label label-success">Aktif</span></h5>' :
                                                        '<h5><span class="label label-default">Non Aktif</span></h5>';
                                                ?></td>
                                        </tr>          
                                        <tr>
                                            <td>Date Post</td>
                                            <td><?php echo $list['tgl_post']; ?></td>
                                        </tr>                      
                                        <tr>
                                            <td colspan="2">Description : <?php echo $list['isi']; ?></td>
                                        </tr>                   
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>
                                                <a href="<?php echo site_url('admin/berita/edit_berita/' . $list['idberita']) ?>" class="btn btn-primary">Edit</a>
                                                <?php if ($list['statis'] == 0) { ?>
                                                    <a href="<?php echo site_url('admin/berita/delete_berita/' . $list['idberita']) ?>" class="btn btn-danger">Delete</a>
                                                <?php } ?>
                                        </tr>
                                    </table>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Modals-->  
                <?php if ($list['statis'] == 0) { ?>
                    <a href="<?php echo site_url('admin/berita/delete_berita/' . $list['idberita']); ?>" class="btn btn-danger" onClick="return confirm('Are you sure?')"><i class="fa fa-trash"></i></a>
                    <?php } ?>
            </td>
            </tr>
            <?php
            $i++;
        }
        ?>
        </tbody>
    </table>
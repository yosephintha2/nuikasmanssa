
<script type="text/javascript">

    var save_method; //for save method string
    var table;
    var base_url = '<?php echo base_url(); ?>';

    $(document).ready(function () {
//        $('#View').modal('show');
        //datatables
        table = $('#tables').DataTable({
            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "order": [], //Initial no order.

            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": "<?php echo site_url('admin/alumni/ajax_list') ?>",
                "type": "POST"
            },
            //Set column definition initialisation properties.
            "columnDefs": [
                {
                    "targets": [-1], //last column
                    "orderable": false, //set not orderable
                },
                {
                    "targets": [0], //2 last column (photo)
                    "orderable": false, //set not orderable
                },
            ],
        });

    });

    function reload_table() {
        table.ajax.reload(null, false); //reload datatable ajax 
    }

    function verifikasi(id, con) {
        if (confirm('Anda yakin?')) {
            $.ajax({
                url: "<?php echo site_url('admin/alumni/ajax_verifikasi') ?>/" + id + '/' + con,
                type: "POST",
                dataType: "JSON",
                success: function (data) {
                    reload_table();
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert('Error get data from ajax');
                }
            });

        }
    }

    function detail(id) {
        //Ajax Load data from ajax
        $.ajax({
            url: "<?php echo site_url('admin/alumni/ajax_detail') ?>/" + id,
            type: "GET",
            dataType: "JSON",
            success: function (data) {
                $('#View').modal('show'); // show bootstrap modal when complete loaded
                $('#nama_lengkap').text(data.nama_lengkap); // label photo upload
                $('#nama_panggilan').text(data.nama_panggilan); // label photo upload
                $('#angkatan').text(data.angkatan); // label photo upload
                $('#jurusan').text(data.jurusan); // label photo upload
                $('#profesi').text(data.nama_profesi); // label photo upload
                $('#telepon').text(data.telepon); // label photo upload
                $('#email').text(data.email); // label photo upload
                $('#alamat').text(data.alamat); // label photo upload
                $('#kota').text(data.kota); // label photo upload
                $('#provinsi').text(data.provinsi); // label photo upload
                $('#kodepos').text(data.kodepos); // label photo upload
                $('#roles').text(data.roles); // label photo upload

                if (data.foto) {
                    $('#photo-preview').html('<img src="' + base_url + '/assets/upload/alumni/' + data.foto + '" class="img-responsive" id="img-preview">');
                } else {
                    $('#photo-preview').html('<img src="" class="img-responsive" id="img-preview">(No photo)'); // show photo
                }


            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert('Error get data from ajax');
            }
        });
    }
</script>
<style>
    th{
        text-align: center;
    }
</style>
<?php
// Session 
if ($this->session->flashdata('sukses')) {
    echo '<div class="alert alert-success">';
    echo $this->session->flashdata('sukses');
    echo '</div>';
}
// Error
echo validation_errors('<div class="alert alert-success">', '</div>');
?>

<!--  Modals-->
<div class="panel-body">
    <?php if ($this->session->userdata('roles') == 1 OR $this->session->userdata('roles') == 2) { ?>
        <p>
            <a href="<?php echo site_url('admin/alumni/create') ?>" class="btn btn-primary"><i class="fa fa-plus"></i> Create New Alumni</a>
        </p>
    <?php } ?>


    <table class="table table-striped table-bordered table-hover table-responsive" id="tables" width="100%">
        <thead>
            <tr>
                <th width="8%">#</th>
                <th>Nama Lengkap</th>
                <th>Angkatan</th>
                <th>Alamat</th>
                <th>Profesi</th>
                <th>Verifikasi</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody>

        </tbody>
    </table>

    <div class="modal fade" id="View" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">View Alumni</h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-striped table-bordered table-hover">
                            <tr>
                            <!--<img src="" width="539px">-->
                            <div id="photo-preview"></div>
                            <td>Nama Lengkap</td>
                            <td id="nama_lengkap"></td>
                            </tr>
                            <tr>
                                <td>Nama Panggilan</td>
                                <td id="nama_panggilan"></td>
                            </tr>
                            <tr>
                                <td>Angkatan</td>
                                <td id="angkatan"></td>
                            </tr>
                            <tr>
                                <td>Jurusan</td>
                                <td id="jurusan"></td>
                            </tr>
                            <tr>
                                <td>Profesi</td>
                                <td id="profesi"></td>
                            </tr><tr>
                                <td>Telepon</td>
                                <td id="telepon"></td>
                            </tr>
                            <tr>
                                <td>Email</td>
                                <td id="email"></td>
                            </tr>
                            <tr>
                                <td>Alamat</td>
                                <td id="alamat"></td>
                            </tr>
                            <tr>
                                <td>Provinsi</td>
                                <td id="provinsi"></td>
                            </tr>
                            <tr>
                                <td>Kota</td>
                                <td id="kota"></td>
                            </tr>
                            <tr>
                                <td>Kode Pos</td>
                                <td id="kodepos"></td>
                            </tr>
                            <tr>
                                <td>Role</td>
                                <td id="roles"></td>
                            </tr>
<!--                            <tr>
                                <td>&nbsp;</td>
                                <td></tr>-->
                        </table>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

<style>
    #imagePreview {
        margin-top: 7px;
        width: 100%;
        height: 370px;
        background-position: center center;
        background-size: cover;
        -webkit-box-shadow: 0 0 1px 1px rgba(0, 0, 0, .3);
        display: inline-block;
        background-image: url('<?php echo base_url('assets') . '/images/placeholder.png' ?>')
    }
</style>
<script type="text/javascript">
    $(document).ready(function () {

    });
    $(function () {
        $("#provinsi").on('change', function () {
            var id = $("#provinsi").val();
            var html = '<?= site_url("admin/alumni/list_kota") ?>/' + id + '/0';
            $("#kota").load(html);
        });

//        $("#file").on("change", function ()
//        {
//            var files = !!this.files ? this.files : [];
//            if (!files.length || !window.FileReader)
//                return; // no file selected, or no FileReader support
//
//            if (/^image/.test(files[0].type)) { // only image file
//                var reader = new FileReader(); // instance of the FileReader
//                reader.readAsDataURL(files[0]); // read the local file
//
//                reader.onloadend = function () { // set image data as background of div
//                    $("#imagePreview").css("background-image", "url(" + this.result + ")");
//                }
//            }
//        });
    });

    function PreviewImage() {
        var oFReader = new FileReader();
        oFReader.readAsDataURL(document.getElementById("file").files[0]);
        oFReader.onload = function (oFREvent) {
            document.getElementById("img-preview").src = oFREvent.target.result;
        };
    }


</script>


<?php
// Session 
if ($this->session->flashdata('sukses')) {
    echo '<div class="alert alert-success">';
    echo $this->session->flashdata('sukses');
    echo '</div>';
}

// File upload error
if (isset($error)) {
    echo '<div class="alert alert-success">';
    echo $error;
    echo '</div>';
}

// Error
echo validation_errors('<div class="alert alert-success">', '</div>');
?>

<form action="<?php echo site_url('admin/alumni/edit/' . $alumni['idalumni']) ?>" method="post" enctype="multipart/form-data">

    <div class="col-md-6">
        <div class="form-group">
            <label style="color: red">*) Wajib diisi</label>
        </div>
        <div class="form-group">
            <label>Nama Lengkap <i style="color: red">*</i></label>
            <input type="hidden" name="idalumni" value="<?php echo $alumni['idalumni'] ?>">
            <input type="text" name="nama_lengkap" class="form-control" 
                   value="<?php echo $alumni['nama_lengkap'] ?>" required placeholder="Nama Lengkap">
        </div>
        <div class="form-group">
            <label>Nama Panggilan</label>
            <input type="text" name="nama_panggilan" class="form-control" 
                   value="<?php echo $alumni['nama_panggilan'] ?>" placeholder="Nama Panggilan">
        </div>
        <?php if ($this->session->userdata('roles') == 2) { ?>
            <div class="form-group">
                <label>Angkatan <i style="color: red">*</i></label>
                <input type="number" name="angkatan" class="form-control" maxlength="4" min="1945"
                       value="<?php echo $alumni['angkatan'] ?>" readonly="" required placeholder="Angkatan">
            </div>
        <?php } else { ?>
            <div class="form-group">
                <label>Angkatan <i style="color: red">*</i></label>
                <input type="number" name="angkatan" class="form-control" maxlength="4" min="1945"
                       value="<?php echo $alumni['angkatan'] ?>" required placeholder="Angkatan">
            </div>
        <?php } ?>
        <div class="form-group">
            <label>Jurusan</label>
            <input type="text" name="jurusan" class="form-control"
                   value="<?php echo $alumni['jurusan'] ?>" placeholder="Jurusan">
        </div>
        <div class="form-group">
            <label>Profesi <i style="color: red">*</i></label>
            <?= form_dropdown('idprofesi', $this->form_option->profesi(), $alumni['idprofesi'], "id='profesi' class='form-control'required")
            ?>
        </div>
        <div class="form-group">
            <label>Telepon <i style="color: red">*</i></label>
            <input type="text" name="telepon" class="form-control" 
                   value="<?php echo $alumni['telepon'] ?>" required placeholder="Telepon">
        </div>
        <div class="form-group">
            <label>Email <i style="color: red">*</i></label>
            <input type="email" name="email" class="form-control" 
                   value="<?php echo $alumni['email'] ?>" required placeholder="Email">
        </div>
        <div class="form-group">
            <label>Alamat <i style="color: red">*</i></label>
            <textarea name="alamat" placeholder="Alamat" class="form-control" 
                      rows="5" required><?php echo $alumni['alamat'] ?></textarea>
        </div>
        <div class="form-group">
            <label>Provinsi </label>
            <?= form_dropdown('provinsi', $this->form_option->provinsi(), $alumni['idprovinsi'], "id='provinsi' class='form-control'")
            ?>
        </div>
        <div class="form-group">
            <label>Kabupaten/Kota </label>
            <?= form_dropdown('idkota', $this->form_option->kota_opsi($alumni['idprovinsi']), $alumni['idkota'], "id='kota' class='form-control'")
            ?>
        </div>
        <div class="form-group">
            <label>Kode Pos</label>
            <input type="number" name="kodepos" class="form-control" min="0"
                   value="<?php echo $alumni['kodepos'] ?>" placeholder="Kode Pos">
        </div>

    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label></label>
        </div>
        <div class="form-group">
            <label>Upload Image</label>
            <input type="file" name="image" class="form-control" id="file" accept="image/*">
            <div id="imagePreview"><img id="img-preview" style="width: 100%;height: 370px;"
                                        src="<?php echo base_url('assets/upload/alumni/' . $alumni['foto']) ?>"></div>
        </div>
        <div class="form-group">
            <label>Username <i style="color: red">*</i></label>
            <input type="text" name="username" class="form-control" 
                   value="<?php echo $alumni['username'] ?>" required placeholder="Username">
        </div>
        <div class="form-group">
            <label>Password <i style="color: red">(Kosongi jika tidak merubah password)</i></label>
            <input type="password" name="password" class="form-control" 
                   value="<?php echo set_value('password') ?>" placeholder="Password">
        </div>
        <div class="form-group">
            <label>Password Confirmation <i style="color: red">(Kosongi jika tidak merubah password)</i></label>
            <input type="password" name="passconf" class="form-control" 
                   value="<?php echo set_value('passconf') ?>" placeholder="Password Confirmation">
        </div>

        <?php if ($this->session->userdata('roles') == 3 AND $this->session->userdata('id_alumni') == $alumni['idalumni']) { ?>
            <input type="hidden" name="role" value="<?php echo $alumni['role'] ?>">
        <?php } else { ?>
            <div class="form-group">
                <label>Role <i style="color: red">*</i></label>
                <?= form_dropdown('role', $this->form_option->role(), $alumni['role'], "id='role' class='form-control' required")
                ?>
            </div>
        <?php } ?>
    </div>
    <div class="col-md-12">
        <div class="form-group">
            <br><br>
            <input type="submit" name="submit" value="Save" class="btn btn-primary">
            <input type="reset" name="reset" value="Reset" class="btn btn-default">
        </div>
    </div>

</form>